#include "test_bitstream_generator.h"

#define TESTNB 1

int test_create_elf_c_source(){
	options * test_opt = init_from_file("testbench_noseidel.c");
	generate_sog_data_no_dump(test_opt);
	generate_soag_data_no_dump(test_opt);

	create_elf_c_source(test_opt);
	int diff_out;
	diff_out = system("diff helloworld.c tests/test_codes/helloworld.c > diff_out");

	if (diff_out) {

		return 1;
	}

	remove("diff_out");
	remove("helloworld.c");
	return 0;
}

int test_bitstream_generator() {
	int error = 0;

	Testsuite bitstream_generator_tests[TESTNB];

	bitstream_generator_tests[0].function = test_create_elf_c_source;
	bitstream_generator_tests[0].name = "create_elf_c_source";

	error = run_testsuite(bitstream_generator_tests, TESTNB);
	return error;
}