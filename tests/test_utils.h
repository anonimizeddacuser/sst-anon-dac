#ifndef TEST_UTILS_H
#define TEST_UTILS_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <clan/clan.h>
#include <osl/scop.h>

#include <osl/scop.h>
#include <osl/macros.h>
#include <osl/util.h>
#include <osl/extensions/dependence.h>
#include <osl/extensions/arrays.h>
#include <candl/macros.h>
#include <candl/dependence.h>
#include <candl/violation.h>
#include <candl/options.h>
#include <candl/scop.h>
#include <candl/util.h>
#include <candl/piplib.h>

#include "../src/scoputils.h"
#include "../src/streaming_graph.h"
#include "../src/barvinok_interface.h"
#include "../src/parser.h"
#include "../src/evaluate_scop.h"
#include "../src/hls_generator.h"
#include "../src/soag.h"
#include "../src/bitstream_generator.h"


struct testsuite{
int (*function)();
char *name;
};

typedef struct testsuite Testsuite;

int run_testsuite(Testsuite* tests, int testnb);

options* init_from_file(char* file_path);

void failed();

void succeded();
#endif