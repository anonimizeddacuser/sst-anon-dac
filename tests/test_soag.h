#ifndef TEST_SOAG_H
#define TEST_SOAG_H

#include "test_utils.h"


int test_stancil_array_ID();

int test_select_access_to_stancil_array();

int test_get_stancil_array_DIM();

int test_check_dimension_coherency();

int test_compute_stancil_array_dimensions();

int test_compute_stancil_array_dimensions_parse_res();

int test_generate_soag_data();

int test_create_channels();

int test_compute_filter_lexmin();

int test_cmp_GT_filters();

int test_compute_fifo_size();

int test_create_channel_links();

int test_create_kernels();

int test_get_channel_port();

int test_make_channels_kernels_links();

int test_create_demux();

int test_dump_soag_dot();

int test_soag();


#endif
