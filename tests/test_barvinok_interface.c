#include "test_barvinok_interface.h"

#define TESTNB 1

int test_get_loops_from_domain(){
	options * test_opt = init_from_file("jacobi_1d_noparm_rightiterator_scop.c");
	generate_sog_data_no_dump(test_opt);
	generate_soag_data_no_dump(test_opt);


	struct soag_SST* tmp_SST = test_opt->contexts->SST;
	struct soag_kernel *kernels = tmp_SST -> kernels;

	struct soag_channel *channels_it = tmp_SST->channels;

	while (channels_it != NULL){
		if( channels_it->ID == tmp_SST->stancil_array_id ){
			break;
		}
		channels_it =channels_it ->next;
	}

	struct soag_filter *filters_it = channels_it->filters;

	while(filters_it != NULL){
		if(filters_it->center)
			break;
		filters_it = filters_it->next;
	}

	osl_relation_p domain = filters_it -> access -> statement-> domain;
	char* iscc_result;
	iscc_result = get_loops_from_domain(domain);
	//printf("iscc_result:%s\n", iscc_result);
	if(strcmp(iscc_result,"for (register int i0 = 1; i0 <= 48; i0 += 1)")){
		return 1;
	}

	test_opt = init_from_file("jacobi_2d_noparm_scop.c");
	generate_sog_data_no_dump(test_opt);
	generate_soag_data_no_dump(test_opt);

	tmp_SST = test_opt->contexts->SST;
	channels_it = tmp_SST->channels;

	while (channels_it != NULL){
		if( channels_it->ID == tmp_SST->stancil_array_id ){
			break;
		}
		channels_it =channels_it ->next;
	}

	filters_it = channels_it->filters;

	while(filters_it != NULL){
		if(filters_it->center)
			break;
		filters_it = filters_it->next;
	}

	domain = filters_it -> access -> statement-> domain;
	iscc_result = get_loops_from_domain(domain);
	//printf("iscc_result:%s\n", iscc_result);
  if(strcmp(iscc_result,"for (register int i0 = 1; i0 <= 48; i0 += 1)\n  for (register int i1 = 1; i1 <= 48; i1 += 1)")){
		return 1;
	}

	return 0;
}

int test_barvinok_interface(){
	int error = 0;

	Testsuite barvinok_interface_tests[TESTNB];

	barvinok_interface_tests[0].function = test_get_loops_from_domain;
	barvinok_interface_tests[0].name = "get_loops_from_domain";
	
	error = run_testsuite(barvinok_interface_tests, TESTNB);
	return error;
}