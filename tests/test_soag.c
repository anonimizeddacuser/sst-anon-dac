#include "test_soag.h"

#define TESTNB 17

int test_get_stancil_array_ID() {

	//test 1
	int test_nb = 1;
	options *test_opt = init_from_file("jacobi_1d_scop.c");
	int expected = 6;
	int actual = get_stancil_array_ID(test_opt->contexts);

	if (actual != expected) {
		return 1;
	}

	//test 2
	test_nb++;
	test_opt = init_from_file("jacobi_2d_scop.c");
	expected = 7;
	actual = get_stancil_array_ID(test_opt->contexts);

	if (actual != expected) {
		return 1;
	}

	return 0;
}

int test_select_access_to_stancil_array() {
	//test 1
	int test_nb = 1;
	options *test_opt = init_from_file("jacobi_1d_scop.c");
	osl_relation_list_p res = select_access_to_stancil_array(test_opt->contexts);
	int stancil_ID = get_stancil_array_ID(test_opt->contexts);
	int error = 0;
	int current = 0;
	int count = 0;

	while (res != NULL) {
		current = get_array_id_from_osl_relation(res->elt, test_opt->contexts->scop);
		if (current != stancil_ID)
		{
			error = 1;
			return error;
		}
		count ++;
		res = res->next;
	}

	if (count != 4) {   error = 1;}

	return error;
}

int test_get_stancil_array_DIM() {
	options *test_opt = init_from_file("jacobi_1d_scop.c");
	int res = get_stancil_array_DIM(test_opt->contexts);
	if (res != 1) {  return 1;}

	test_opt = init_from_file("jacobi_2d_scop.c");
	res = get_stancil_array_DIM(test_opt->contexts);
	if (res != 2) {  return 1;}


	return 0;
}

int test_check_dimension_coherency() {
	//test 1
	options *test_opt = init_from_file("jacobi_1d_scop.c");
	int res = check_dimension_coherency(test_opt->contexts);
	if (res) {  return 1;}

	//test 2
	test_opt = init_from_file("jacobi_2d_scop.c");
	res = check_dimension_coherency(test_opt->contexts);
	if (res) {  return 1;}

	//test 3
	test_opt = init_from_file("fail_check_dimension_coherency.c");
	res = check_dimension_coherency(test_opt->contexts);
	if (!res) {  return 1;}

	//test 4
	test_opt = init_from_file("fail_check_dimension_coherency_2.c");
	res = check_dimension_coherency(test_opt->contexts);
	if (!res) {  return 1;}

	return 0;
}

int test_compute_stancil_array_dimensions() {
	options * test_opt = init_from_file("jacobi_2d_noparm_scop.c");
	int *res = compute_stancil_array_dimensions(test_opt->contexts);

	if (res[0] != 49 || res[1] != 49)
	{
		return 1;
	}

	test_opt = init_from_file("jacobi_1d_noparm_scop.c");
	res = compute_stancil_array_dimensions(test_opt->contexts);

	if (res[0] != 49)
	{

		return 1;
	}

	test_opt = init_from_file("jacobi_2d_no_parm_test_dimension_scop.c");
	res = compute_stancil_array_dimensions(test_opt->contexts);

	if (res[0] != 1024 || res[1] != 750)
	{
		printf("%d %d\n", res[0], res[1] );
		return 1;
	}

	test_opt = init_from_file("jacobi_2d_no_parm_test_dimension_scop_harder.c");
	res = compute_stancil_array_dimensions(test_opt->contexts);

	if (res[0] != 1024 || res[1] != 750)
	{
		printf("%d %d\n", res[0], res[1] );
		return 1;
	}

	return 0;

}

int test_compute_stancil_array_dimensions_parse_res() {
	char *input1 = " { [49, 48] }";
	int *res1 = compute_stancil_array_dimensions_parse_res(input1);

	if (res1[0] != 49 || res1[1] != 48) {

		return 1;
	}


	char *input2 = " { [1, 480, 50] }";
	int *res2 = compute_stancil_array_dimensions_parse_res(input2);

	if (res2[0] != 1 || res2[1] != 480 || res2[2] != 50) {

		return 1;
	}

	return 0;
}

int test_generate_soag_data() {
	options * test_opt = init_from_file("jacobi_1d_noparm_rightiterator_scop.c");
	generate_sog_data_no_dump(test_opt);
	//FILE *output;
	//output = fopen("test_create_channels.dot", "w");
	//generate_dot_from_sog_graph(output, test_opt->contexts->graph);
	//fclose(output);
	generate_soag_data_no_dump(test_opt);

	struct soag_SST* sst = test_opt->contexts->SST;

	if (strcmp(sst->ID, "SST_0") || sst->stancil_array_id != 4 || sst->dim != 1 || sst->size_dim[0] != 49) {

		printf("ID:%s, stancil_id: %d, dim: %d, size_dim: %d\n", sst->ID , sst->stancil_array_id, sst->dim, sst->size_dim[0]);
		return 1;
	}

	return 0;
}

int test_create_channels() {
	options * test_opt = init_from_file("jacobi_1d_noparm_rightiterator_scop.c");
	generate_sog_data_no_dump(test_opt);
	//FILE *output;
	//output = fopen("test_create_channels.dot","w");
	//generate_dot_from_sog_graph(output, test_opt->contexts->graph);
	//fclose(output);
	generate_soag_data_no_dump(test_opt);
	create_channels (test_opt->contexts);

	struct soag_SST* sst = test_opt->contexts->SST;

	struct soag_channel *channels = sst->channels;

	int count = 0;
	while (channels != NULL) {
		count ++;
		channels = channels->next;
	}
	channels = sst->channels;

	if (count != 1 || channels->SST != sst || channels->ID != 4) {

		return 1;
	}

	struct soag_filter* filters = channels->filters;
	count = 0;
	while (filters != NULL) {
		count ++;
		filters = filters->next;
	}
	if (count != 3) {

		printf("filter count 1 %d\n", count);
		return 1;
	}

	filters = channels->filters;
	int *dim = filters->access_lexmin;
	int center = filters->center;
	if (dim[0] != 0 || center) {

		printf("dim[0] 1 %d\n", dim[0] );
		return 1;
	}
	filters = filters->next;
	dim = filters->access_lexmin;
	center = filters->center;
	if (dim[0] != 1 || !center) {

		printf("dim[0] 2 %d\n", dim[0] );
		return 1;
	}

	filters = filters->next;
	dim = filters->access_lexmin;
	center = filters->center;
	if (dim[0] != 2 || center) {

		printf("dim[0] 3 %d\n", dim[0] );
		return 1;
	}


	test_opt = init_from_file("jacobi_2d_noparm_scop.c");
	generate_sog_data_no_dump(test_opt);
	//FILE *output;
	//output = fopen("test_create_channels.dot","w");
	//generate_dot_from_sog_graph(output, test_opt->contexts->graph);
	//fclose(output);
	generate_soag_data_no_dump(test_opt);
	create_channels (test_opt->contexts);

	sst = test_opt->contexts->SST;

	channels = sst->channels;

	count = 0;
	while (channels != NULL) {
		count ++;
		channels = channels->next;
	}

	channels = sst->channels;

	if (count != 1 || channels->SST != sst || channels->ID != 5) {

		return 1;
	}

	filters = channels->filters;
	count = 0;
	while (filters != NULL) {
		count ++;
		filters = filters->next;
	}

	if (count != 5) {

		printf("filter count 2 %d\n", count);
		return 1;
	}

	filters = channels->filters;

	dim = filters->access_lexmin;
	center = filters->center;
	if (dim[0] != 1 || dim[1] != 1 || !center) {

		printf("3 dim[0]:%d dim[1] %d\n", dim[0], dim[1]);
		return 1;
	}

	filters = filters->next;

	dim = filters->access_lexmin;
	center = filters->center;
	if (dim[0] != 1 || dim[1] != 0 || center) {

		printf("4 dim[0]:%d dim[1] %d\n", dim[0], dim[1]);
		return 1;
	}


	filters = filters->next;

	dim = filters->access_lexmin;
	center = filters->center;
	if (dim[0] != 1 || dim[1] != 2 || center) {

		printf("5 dim[0]:%d dim[1] %d\n", dim[0], dim[1]);
		return 1;
	}


	filters = filters->next;
	dim = filters->access_lexmin;
	center = filters->center;
	if (dim[0] != 2 || dim[1] != 1 || center) {

		printf("6 dim[0]:%d dim[1] %d\n", dim[0], dim[1]);
		return 1;
	}

	filters = filters->next;
	dim = filters->access_lexmin;
	center = filters->center;
	if (dim[0] != 0 || dim[1] != 1 || center) {

		printf("7 dim[0]:%d dim[1] %d\n", dim[0], dim[1]);
		return 1;
	}

	test_opt = init_from_file("jacobi_1d_noparm_2channels.c");
	generate_sog_data_no_dump(test_opt);
	//FILE *output;
	//output = fopen("test_create_channels.dot", "w");
	//generate_dot_from_sog_graph(output, test_opt->contexts->graph);
	//fclose(output);
	generate_soag_data_no_dump(test_opt);
	create_channels (test_opt->contexts);

	sst = test_opt->contexts->SST;

	channels = sst->channels;

	count = 0;
	while (channels != NULL) {
		count ++;
		channels = channels->next;
	}

	channels = sst->channels;
	struct soag_channel *channel2 = channels->next;
	if (count != 2 || channels->SST != sst || channels->ID != 4 || channel2->ID != 5) {
		printf("channel id: %d\n channel id: %d\n", channels->ID, channel2->ID);

		return 1;
	}

	filters = channels->filters;
	count = 0;
	while (filters != NULL) {
		count++;
		filters = filters->next;
	}
	filters = channels->filters;
	if (count != 1) {

		printf("filter count 3 %d \n", count);
		return 1;
	}
	dim = filters->access_lexmin;
	if (dim[0] != 1) {

		printf("8 dim[0] %d \n", dim[0]);
		return 1;
	}

	filters = channel2->filters;
	count = 0;
	while (filters != NULL) {
		count++;
		filters = filters->next;
	}
	filters = channel2->filters;
	if (count != 3) {

		printf("filter count 4 %d \n", count);
		return 1;
	}

	dim = filters->access_lexmin;
	if (dim[0] != 0) {

		printf("9 dim[0]:%d\n", dim[0]);
		return 1;
	}

	filters = filters->next;
	dim = filters->access_lexmin;
	if (dim[0] != 1) {

		printf("10 dim[0]:%d\n", dim[0]);
		return 1;
	}

	filters = filters->next;
	dim = filters->access_lexmin;
	if (dim[0] != 2) {

		printf("11 dim[0]:%d\n", dim[0]);
		return 1;
	}

	return 0;
}

int test_compute_filter_lexmin() {
	options * test_opt = init_from_file("jacobi_1d_noparm_rightiterator_scop.c");
	generate_sog_data_no_dump(test_opt);
	scop_context *tmp_context = test_opt->contexts;

	access_lst* read_access = get_read_access_from_sog_graph(tmp_context->graph);

	//access_lst* it=read_access;
	//while(it!= NULL){
	//	printf("Access: %s\n",  osl_relation_sprint(it->access->access_relation));
	//	it=it->next;
	//}

	int *res = compute_filter_lexmin(tmp_context, read_access->access);

	if (res[0] != 0) {
		printf("1res: %d\n", res[0]);

		return 1;
	}

	read_access = read_access -> next;

	res =  compute_filter_lexmin(tmp_context, read_access->access);

	if (res[0] != 1) {
		printf("2res: %d\n", res[0]);

		return 1;
	}

	read_access = read_access -> next;

	res =  compute_filter_lexmin(tmp_context, read_access->access);

	if (res[0] != 2) {
		printf("3res: %d\n", res[0]);

		return 1;
	}





	test_opt = init_from_file("jacobi_2d_noparm_scop.c");
	generate_sog_data_no_dump(test_opt);
	tmp_context = test_opt->contexts;

	read_access = get_read_access_from_sog_graph(tmp_context->graph);

	//access_lst* it = read_access;
	//while (it != NULL) {
	//	printf("Access: %s\n",  osl_relation_sprint(it->access->access_relation));
	//	it = it->next;
	//}


	res =  compute_filter_lexmin(tmp_context, read_access->access);

	if (res[0] != 1 || res[1] != 1 ) {
		printf("4res: %d\n", res[0]);

		return 1;
	}

	read_access = read_access -> next;

	res =  compute_filter_lexmin(tmp_context, read_access->access);

	if (res[0] != 1 || res[1] != 0 ) {
		printf("5res: %d\n", res[0]);

		return 1;
	}

	read_access = read_access -> next;

	res =  compute_filter_lexmin(tmp_context, read_access->access);

	if (res[0] != 1 || res[1] != 2 ) {
		printf("6res: %d\n", res[0]);

		return 1;
	}

	read_access = read_access -> next;

	res =  compute_filter_lexmin(tmp_context, read_access->access);

	if (res[0] != 2 || res[1] != 1 ) {
		printf("7res: %d\n", res[0]);

		return 1;
	}

	read_access = read_access -> next;

	res =  compute_filter_lexmin(tmp_context, read_access->access);

	if (res[0] != 0 || res[1] != 1 ) {
		printf("8res: %d\n", res[0]);

		return 1;
	}

	return 0;

}

int test_cmp_GT_filters() {
	options * test_opt = init_from_file("jacobi_1d_noparm_rightiterator_scop.c");
	generate_sog_data_no_dump(test_opt);
	//FILE *output;
	//output = fopen("test_create_channels.dot","w");
	//generate_dot_from_sog_graph(output, test_opt->contexts->graph);
	//fclose(output);
	generate_soag_data_no_dump(test_opt);
	create_channels (test_opt->contexts);

	struct soag_SST* sst = test_opt->contexts->SST;
	struct soag_channel *channels = sst->channels;

	struct soag_filter* filtersA = channels->filters;
	struct soag_filter* filtersB = filtersA->next;

	if (cmp_GT_filters(filtersA, filtersB)) {

		printf("1\n");
		return 1;
	}

	if (!cmp_GT_filters(filtersB, filtersA)) {

		printf("2\n");
		return 1;
	}

	test_opt = init_from_file("jacobi_2d_noparm_scop.c");
	generate_sog_data_no_dump(test_opt);
	//FILE *output;
	//output = fopen("test_create_channels.dot","w");
	//generate_dot_from_sog_graph(output, test_opt->contexts->graph);
	//fclose(output);
	generate_soag_data_no_dump(test_opt);
	create_channels (test_opt->contexts);

	sst = test_opt->contexts->SST;

	channels = sst->channels;


	filtersA = channels->filters;
	filtersB = filtersA->next;


	if (!cmp_GT_filters(filtersA, filtersB)) {

		printf("3\n");
		return 1;
	}

	if (cmp_GT_filters(filtersB, filtersA)) {

		printf("3\n");
		return 1;
	}


	return 0;

}


int test_compute_fifo_size() {
	options * test_opt = init_from_file("jacobi_1d_noparm_rightiterator_scop.c");
	generate_sog_data_no_dump(test_opt);
	//FILE *output;
	//output = fopen("test_create_channels.dot", "w");
	//generate_dot_from_sog_graph(output, test_opt->contexts->graph);
	//fclose(output);
	generate_soag_data_no_dump(test_opt);
	create_channels (test_opt->contexts);

	struct soag_SST* sst = test_opt->contexts->SST;
	struct soag_channel *channels = sst->channels;

	struct soag_filter* filtersA = channels->filters;
	struct soag_filter* filtersB = filtersA->next;

	if (compute_fifo_size(filtersB, filtersA) != 1) {

		printf("1 fifo size: %d\n", compute_fifo_size(filtersB, filtersA));

		return 1;


	}

	test_opt = init_from_file("jacobi_2d_noparm_scop.c");
	generate_sog_data_no_dump(test_opt);
	//FILE *output;
	//output = fopen("test_create_channels.dot", "w");
	//generate_dot_from_sog_graph(output, test_opt->contexts->graph);
	//fclose(output);
	generate_soag_data_no_dump(test_opt);
	create_channels (test_opt->contexts);

	sst = test_opt->contexts->SST;

	channels = sst->channels;


	filtersA = channels->filters;
	filtersB = filtersA->next;


	if (compute_fifo_size(filtersA, filtersB) != 1) {
		printf("2 fifo size: %d\n", compute_fifo_size(filtersB, filtersA));

		return 1;
	}

	filtersA = filtersB;
	filtersB = filtersB -> next -> next-> next;

	if ( compute_fifo_size(filtersA, filtersB) != 48) {

		printf("3 fifo size: %d\n", compute_fifo_size(filtersA, filtersB));

		return 1;
	}

	return 0;
}

int test_create_channel_links() {
	options * test_opt = init_from_file("jacobi_1d_noparm_rightiterator_scop.c");
	generate_sog_data_no_dump(test_opt);
	//FILE *output;
	//output = fopen("test_create_channels.dot", "w");
	//generate_dot_from_sog_graph(output, test_opt->contexts->graph);
	//fclose(output);
	generate_soag_data_no_dump(test_opt);
	create_channels (test_opt->contexts);


	struct soag_SST* sst = test_opt->contexts->SST;
	struct soag_channel *channels = sst->channels;

	create_channel_links(channels);


	struct soag_channel_link *link = channels->links;

	int count = 0;

	while (link != NULL) {
		if (!cmp_GT_filters(link->from, link->to)) {
			printf("1\n");

			return 1;
		}
		count++;
		link = link->next;
	}

	if (count != 2) {
		printf("2:%d ", count);

		return 1;
	}


	test_opt = init_from_file("jacobi_2d_noparm_scop.c");
	generate_sog_data_no_dump(test_opt);
	//FILE *output;
	//output = fopen("test_create_channels.dot", "w");
	//generate_dot_from_sog_graph(output, test_opt->contexts->graph);
	//fclose(output);
	generate_soag_data_no_dump(test_opt);
	create_channels (test_opt->contexts);
	sst = test_opt->contexts->SST;
	channels = sst->channels;

	create_channel_links(channels);
	count = 0;
	link = channels->links;
	while (link != NULL) {
		if (!cmp_GT_filters(link->from, link->to)) {
			printf("3\n");

			return 1;
		}
		count++;
		link = link->next;
	}

	if (count != 4) {
		printf("4:%d ", count);

		return 1;
	}


	return 0;
}

int test_get_channel_port() {
	options * test_opt = init_from_file("jacobi_1d_noparm_rightiterator_scop.c");
	generate_sog_data_no_dump(test_opt);
	//FILE *output;
	//output = fopen("test_create_channels.dot", "w");
	//generate_dot_from_sog_graph(output, test_opt->contexts->graph);
	//fclose(output);
	generate_soag_data_no_dump(test_opt);
	//create_channels (test_opt->contexts);

	struct soag_SST* sst = test_opt->contexts->SST;
	struct soag_channel *channels = sst->channels;
	struct soag_channel_port *port = NULL;
	struct soag_channel_port *port_it =  channels->ports;

	int port_type;
	struct sog_access *access;

	while ( port_it != NULL) {

		port_type  = port_it -> port_type;
		access = port_it-> filter -> access;
		port = get_channel_port(port_type, access, channels);

		if ( port != port_it || port == NULL) {


			return 1;
		}

		port_it = port_it->next;
	}


	return 0;
}

int test_create_kernels() {
	options * test_opt = init_from_file("jacobi_1d_noparm_rightiterator_scop.c");
	generate_sog_data_no_dump(test_opt);
	//FILE *output;
	//output = fopen("test_create_channels.dot", "w");
	//generate_dot_from_sog_graph(output, test_opt->contexts->graph);
	//fclose(output);
	generate_soag_data_no_dump(test_opt);
	//create_channels (test_opt->contexts);

	struct soag_SST* sst = test_opt->contexts->SST;
	struct soag_kernel *kernels = sst->kernels;
	struct soag_kernel *kernels_it = kernels;

	int count = 0;
	while (kernels_it != NULL) {
		count++;
		kernels_it = kernels_it->next;
	}

	if (count != 1) {

		return 1;
	}


	if (strcmp(kernels->body, "A[i] = ( 0.33333 * (A[i-1] + A[i] + A[i + 1]));")) {

		return 1;
	}

	struct soag_kernel_port *port_it = kernels->ports;

	count = 0;
	int count_filter_in = 0;
	int count_kernel_out = 0;

	while (port_it != NULL) {
		count ++ ;

		if (port_it->port_type == FILTER_IN)
			count_filter_in++;
		if (port_it->port_type == KERNEL_OUT)
			count_kernel_out++;

		if (port_it->kernel != kernels) {

			return 1;
		}
		port_it = port_it->next;
	}

	if (count != 4 || count_filter_in != 3 || count_kernel_out != 1) {
		printf("count :%d, count_filter_in:%d,count_kernel_out:%d", count, count_filter_in, count_kernel_out);

		return 1;
	}




	return 0;
}



//void make_channels_kernels_links(struct soag_SST* SST)

int test_make_channels_kernels_links() {
	options * test_opt = init_from_file("jacobi_1d_noparm_rightiterator_scop.c");
	generate_sog_data_no_dump(test_opt);
	//FILE *output;
	//output = fopen("test_create_channels.dot", "w");
	//generate_dot_from_sog_graph(output, test_opt->contexts->graph);
	//fclose(output);
	generate_soag_data_no_dump(test_opt);
	//create_channels (test_opt->contexts);

	struct soag_SST* sst = test_opt->contexts->SST;
	struct soag_kernel *kernels = sst->kernels;
	struct soag_kernel *kernels_it = kernels;

	struct soag_channel_kernel_link *links_channel_kernel = sst->links_channel_kernel;

	struct soag_channel_kernel_link *links_channel_kernel_it = links_channel_kernel;

	int count = 0;
	int count_filters = 0;
	int count_border = 0;

	while (links_channel_kernel_it != NULL) {
		count ++;

		if (links_channel_kernel_it->from == NULL) {
			printf("links_channel_kernel->from = NULL");

			return 1;
		}

		if (links_channel_kernel_it->from->port_type == FILTER_OUT)
			count_filters++;

		links_channel_kernel_it = links_channel_kernel_it->next;
	}

	if (count != 3 || count_filters != 3 ) {
		printf("count :%d, count_filters: %d, count_border: %d", count, count_filters, count_border);

		return 1;
	}



	return 0;
}

int test_create_demux() {
	options * test_opt = init_from_file("jacobi_1d_noparm_rightiterator_scop.c");
	generate_sog_data_no_dump(test_opt);
	generate_soag_data_no_dump(test_opt);

	struct soag_SST* sst = test_opt->contexts->SST;
	struct soag_demux *demux = sst->demux;
	struct soag_demux_port *ports_it = demux->ports;

	int count = 0;

	while (ports_it != NULL) {
		count++;
		ports_it = ports_it ->next;
	}

	if (count != 3) {

		return 1;
	}

	ports_it = demux->ports;

	if (ports_it->ID != 0 || ports_it->port_type != KERNEL_IN) {

		return 1;
	}

	ports_it = ports_it->next;
	if (ports_it->ID != 1 || ports_it->port_type != BORDER_IN) {

		return 1;
	}

	ports_it = ports_it->next;
	if (ports_it->ID != 2 || ports_it->port_type != DEMUX_OUT) {

		return 1;
	}



	struct soag_kernel_demux_link *links_kernel_demux_it = sst->links_kernel_demux;
	count = 0;
	while (links_kernel_demux_it != NULL) {
		count++;
		links_kernel_demux_it = links_kernel_demux_it->next;
	}
	if ( count != 1) {
		printf("1 count %d\n", count);


		return 1;
	}

	links_kernel_demux_it = sst->links_kernel_demux;

	if (links_kernel_demux_it->from->port_type != KERNEL_OUT ||
	        links_kernel_demux_it->to->port_type != KERNEL_IN) {
		printf("2\n");

		return 1;
	}

	struct soag_filter_demux_link *links_channel_demux_it = sst->links_channel_demux;
	count = 0;
	while (links_channel_demux_it) {
		count++;
		links_channel_demux_it = links_channel_demux_it->next;
	}
	if ( count != 1) {
		printf("3\n");

		return 1;
	}

	links_channel_demux_it = sst->links_channel_demux;

	if (links_channel_demux_it->from->port_type != BORDER_OUT ||
	        links_channel_demux_it->to->port_type != BORDER_IN) {
		printf("4\n");

		return 1;
	}


	return 0;
}

int test_dump_soag_dot() {
	options * test_opt = init_from_file("jacobi_1d_noparm_rightiterator_scop.c");
	generate_sog_data_no_dump(test_opt);
	generate_soag_data_no_dump(test_opt);


	FILE* output;
	output = fopen("soag.dot", "w");
	dump_soag_dot(output, test_opt->contexts->SST );
	fclose (output);

	int diff_out;
	diff_out = system("diff tests/test_codes/jacobi_1d_noparm_rightiterator_soag.dot  soag.dot > /dev/null");

	if (diff_out) {

		return 1;
	}


	remove("soag.dot");

	
	test_opt = init_from_file("jacobi_2d_noparm_scop.c");
	generate_sog_data_no_dump(test_opt);
	generate_soag_data_no_dump(test_opt);


	output = fopen("soag1.dot", "w");
	dump_soag_dot(output, test_opt->contexts->SST );
	fclose (output);

	diff_out = system("diff tests/test_codes/jacobi_2d_noparm_soag.dot  soag1.dot > /dev/null");

	if (diff_out) {


		return 1;
	}
	remove("soag1.dot");

	return 0;
}

int test_soag() {
	int error = 0;

	Testsuite soag_tests[TESTNB];

	soag_tests[0].function = test_get_stancil_array_ID;
	soag_tests[0].name = "get_stancil_array_ID";

	soag_tests[1].function = test_select_access_to_stancil_array;
	soag_tests[1].name = "select_access_to_stancil_array";

	soag_tests[2].function = test_get_stancil_array_DIM;
	soag_tests[2].name = "get_stancil_array_DIM";

	soag_tests[3].function = test_check_dimension_coherency;
	soag_tests[3].name = "check_dimension_coherency";

	soag_tests[4].function = test_compute_stancil_array_dimensions_parse_res;
	soag_tests[4].name = "compute_stancil_array_dimensions_parse_res";

	soag_tests[5].function = test_compute_stancil_array_dimensions;
	soag_tests[5].name = "compute_stancil_array_dimensions";

	soag_tests[6].function = test_generate_soag_data;
	soag_tests[6].name = "generate_soag_data";

	soag_tests[7].function = test_create_channels;
	soag_tests[7].name = "create_channels";

	soag_tests[8].function = test_compute_filter_lexmin;
	soag_tests[8].name = "compute_filter_lexmin";

	soag_tests[9].function = test_cmp_GT_filters;
	soag_tests[9].name = "cmp_GT_filters";

	soag_tests[10].function = test_compute_fifo_size;
	soag_tests[10].name = "compute_fifo_size";

	soag_tests[11].function = test_create_channel_links;
	soag_tests[11].name = "create_channel_links";

	soag_tests[12].function = test_get_channel_port;
	soag_tests[12].name = "get_channel_port";

	soag_tests[13].function = test_create_kernels;
	soag_tests[13].name = "create_kernels";

	soag_tests[14].function = test_make_channels_kernels_links;
	soag_tests[14].name = "make_channels_kernels_links";

	soag_tests[15].function = test_create_demux;
	soag_tests[15].name = "create_demux";

	soag_tests[16].function = test_dump_soag_dot;
	soag_tests[16].name = "dump_soag_dot";

	error = run_testsuite(soag_tests, TESTNB);


	return error;
}