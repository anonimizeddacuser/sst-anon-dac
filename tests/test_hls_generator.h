#ifndef TEST_HLS_GENERATOR_H
#define TEST_HLS_GENERATOR_H

#include "test_utils.h"

int test_hls_generator();

int test_create_kernel();

int test_create_hls_demux();

int test_create_filter_from_soag_filter();
#endif