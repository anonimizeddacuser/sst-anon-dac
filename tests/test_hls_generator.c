#include "test_hls_generator.h"

#define TESTNB 3

//void create_filter_from_soag_filter(struct soag_filter* filter)
int test_create_hls_filter_from_soag_filter() {

	options * test_opt = init_from_file("jacobi_1d_noparm_rightiterator_scop.c");
	generate_sog_data_no_dump(test_opt);
	generate_soag_data_no_dump(test_opt);

	struct soag_SST* sst = test_opt->contexts->SST;

	struct soag_filter *filters;

	mkdir("filters", 0755);
	chdir("filters");
	struct soag_filter *filters_it = sst->channels->filters;
	while (filters_it != NULL) {
		create_hls_filter_from_soag_filter(filters_it);
		filters_it = filters_it->next;
	}

	chdir("..");
	int diff_out;
	diff_out = system("diff filters/filter_0_4_0.cpp tests/test_codes/jacobi_1d_noparm_rightiterator_filter_4_0.cpp > diff_out");

	if (diff_out) {

		return 1;
	}

	remove("diff_out");
	diff_out = system("diff filters/filter_0_4_1.cpp tests/test_codes/jacobi_1d_noparm_rightiterator_filter_4_1.cpp > diff_out");

	if (diff_out) {

		return 1;
	}

	remove("diff_out");
	diff_out = system("diff filters/filter_0_4_2.cpp tests/test_codes/jacobi_1d_noparm_rightiterator_filter_4_2.cpp > diff_out");

	if (diff_out) {

		return 1;
	}
	remove("diff_out");

	remove("filters/filter_0_4_0.cpp");
	remove("filters/filter_0_4_1.cpp");
	remove("filters/filter_0_4_2.cpp");
	rmdir("filters");

	return 0;
}

int test_create_kernel() {
	options * test_opt = init_from_file("jacobi_1d_noparm_rightiterator_scop.c");
	generate_sog_data_no_dump(test_opt);
	generate_soag_data_no_dump(test_opt);

	//mkdir("Kernel", 0755);
	//chdir("Kernel");

	struct soag_SST* tmp_SST = test_opt->contexts->SST;

	struct soag_kernel *kernels = tmp_SST -> kernels;
	//create_kernel(kernels->ID, tmp_SST->dim, tmp_SST->size_dim, kernels->body);
	create_kernel_from_soag_kernel(kernels);
	//chdir("..");

	int diff_out = 0;
	diff_out = system("diff kernel_0_0.cpp tests/test_codes/jacobi_1d_noparm_rightiterator_kernel_0.cpp  > /dev/null");

	if (diff_out) {
		printf("diff %d", diff_out);
		return 1;
	}

	remove("kernel_0_0.cpp");

	test_opt = init_from_file("jacobi_2d_noparm_scop.c");
	generate_sog_data_no_dump(test_opt);
	generate_soag_data_no_dump(test_opt);

	//mkdir("Kernel", 0755);
	//chdir("Kernel");

	tmp_SST = test_opt->contexts->SST;

	kernels = tmp_SST -> kernels;
	//create_kernel(kernels->ID, tmp_SST->dim, tmp_SST->size_dim, kernels->body);
	create_kernel_from_soag_kernel(kernels);
	//chdir("..");
	diff_out = system("diff kernel_0_0.cpp tests/test_codes/jacobi_2d_noparm_kernel_0.cpp  > /dev/null");

	if (diff_out) {
		printf("diff %d", diff_out);
		return 1;
	}

	remove("kernel_0_0.cpp");
	return 0;
}

//void create_hls_demux(struct soag_demux* demux)
int test_create_hls_demux() {
	options * test_opt = init_from_file("jacobi_1d_noparm_rightiterator_scop.c");
	generate_sog_data_no_dump(test_opt);
	generate_soag_data_no_dump(test_opt);

	struct soag_SST* tmp_SST = test_opt->contexts->SST;

	create_hls_demux(tmp_SST->demux );

	int diff_out = 0;
	diff_out = system("diff demux_0.cpp tests/test_codes/jacobi_1d_noparm_rightiterator_demux.cpp  > diff_out");

	if (diff_out) {
		printf("diff %d", diff_out);
		return 1;
	}

	remove("demux_0.cpp");
	remove("diff_out");
	
	return 0;
}

int test_hls_generator() {
	int error = 0;

	Testsuite hls_generator_tests[TESTNB];

	hls_generator_tests[0].function = test_create_hls_filter_from_soag_filter;
	hls_generator_tests[0].name = "create_hls_filter_from_soag_filter";

	hls_generator_tests[1].function = test_create_kernel;
	hls_generator_tests[1].name = "create_kernel";

	hls_generator_tests[2].function = test_create_hls_demux;
	hls_generator_tests[2].name = "create_hls_demux";

	error = run_testsuite(hls_generator_tests, TESTNB);
	return error;
}