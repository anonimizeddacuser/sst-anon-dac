#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <clan/clan.h>
#include <osl/scop.h>

#include <osl/scop.h>
#include <osl/macros.h>
#include <osl/util.h>
#include <osl/extensions/dependence.h>
#include <osl/extensions/arrays.h>
#include <candl/macros.h>
#include <candl/dependence.h>
#include <candl/violation.h>
#include <candl/options.h>
#include <candl/scop.h>
#include <candl/util.h>
#include <candl/piplib.h>

#include "../src/scoputils.h"
#include "../src/streaming_graph.h"
#include "../src/barvinok_interface.h"
#include "../src/parser.h"
#include "../src/evaluate_scop.h"
#include "../src/hls_generator.h"



int run_all_tests(){
	int error = 0;
	printf("**Testing soag module (test_soag)**\n\n");
	if(test_soag()) error = 1;

	printf("\n**Testing hls generator module (test_hls_generator)**\n\n" );
	if(test_hls_generator()) error = 1;

	printf("\n**Testing barvinok interface (test_barvinok_interface)**\n\n" );
	if(test_barvinok_interface()) error = 1;

	printf("\n**Testing bitstream generator module (test_bitstream_generator)**\n\n" );
	if(test_bitstream_generator()) error = 1;

	printf("\n**Testing parser module (test_parser)**\n\n");
	if(test_parser()) error = 1;
	return error;
}


int main(){
	printf("Starting test suite...\n\n");
	
	return run_all_tests();
}