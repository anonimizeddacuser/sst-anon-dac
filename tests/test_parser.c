#include "test_parser.h"

#define TESTNB 1

int test_parse_args() {
	char* args[3];
	args[0] = "program_name";
	args[1] = "-q";
	args[2] = "1,2,3";
	options * test_opt = parse_args (3, args);
	struct queuing* queuing = test_opt->queuing;
	if (queuing->arg_nb != 3 )
		return 1;

	if (queuing->args[0] != 1 )
		return 1;
	if (queuing->args[1] != 2 )
		return 1;	
	if (queuing->args[2] != 3 )
		return 1;

	return 0;
}

int test_parser() {
	int error = 0;

	Testsuite parser_tests[TESTNB];

	parser_tests[0].function = test_parse_args;
	parser_tests[0].name = "parse_args";

	error = run_testsuite(parser_tests, TESTNB);
	return error;
}