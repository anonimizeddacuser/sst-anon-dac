#include "test_utils.h"

options* init_from_file(char* file_path) {
	options *opt = NULL;
	int argc = 2;
	char *argv[2];
	argv[0] = "test";
	char *complete_file_path = (char*)malloc(sizeof(char)*(strlen(file_path)+20));
	complete_file_path[0]='\0';
	strcat(complete_file_path,"tests/test_codes/");
	strcat(complete_file_path,file_path);
	argv[1] = complete_file_path;
	opt = parse_args(argc, argv);
	parse_input_no_dump(opt);
	return opt;
}

int run_testsuite(Testsuite* tests, int testnb){
 int i =0;
 int error = 0;
 int align = -82;

 for ( i = 0 ; i < testnb; i++){
	printf("%s %*s", " Testing",align,tests[i].name);
	if ((*(tests[i].function))()){
		failed();
		error = 1;
	}
	else
		succeded();
 }

 return error;
}

void failed(){
	printf("[ \x1B[31mFailed\e[0m ]\n");
}
void succeded(){
	printf("[ \x1B[32mSucceded\e[0m ]\n");
}