static
void kernel_jacobi_2d_imper(int tsteps,
			    int n,
			    DATA_TYPE POLYBENCH_2D(A,N,N,n,n),
			    DATA_TYPE POLYBENCH_2D(B,N,N,n,n))
{
  int t, i, j;

#pragma scop
  for (t = 0; t < 10; t++)
    {
      for (i = 1; i < 49; i++)
	for (j = 1; j < 49; j++)
	  B[i][j] = 0.2 * (A[i][j] + A[i][j-1] + A[i][1+j] + A[1+i][j] + A[i-1][j]);
      for (i = 1; i < 49; i++)
	for (j = 1; j <49; j++)
	  A[i][j] = B[i][j];
    }
#pragma endscop

}

