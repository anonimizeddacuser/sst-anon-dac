static
void kernel_jacobi_2d_imper(int tsteps,
			    int n,
			    DATA_TYPE POLYBENCH_2D(A,N,N,n,n),
			    DATA_TYPE POLYBENCH_2D(B,N,N,n,n))
{
  int t, i, j;

#pragma scop
  for (t = 0; t < 10; t++)
    {
      for (i = 2; i < 1023; i++)
	for (j = 2; j < 749; j++)
	  B[i][j] = 0.2 * (A[i][j] + A[i][j-2] + A[i][2+j] + A[2+i][j] + A[i-2][j]);
      for (i = 2; i < 1023; i++)
	for (j = 2; j <749; j++)
	  A[i][j] = B[i][j];
    }
#pragma endscop

}