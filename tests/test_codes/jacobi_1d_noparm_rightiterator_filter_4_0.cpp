#include <ap_int.h>
#include <hls_stream.h>

#define N 49

#define data_type float
template<int D> struct ap_axis{
  ap_int<D> data;
  ap_uint<1> last;
};

typedef ap_axis<sizeof(data_type)*8> data;
typedef hls::stream<data> data_stream;

void filter_0_4_0(data_stream &fifo_4_1, data_stream &filter_0){
#pragma HLS INTERFACE axis port=fifo_4_1
#pragma HLS INTERFACE axis port=filter_0

#pragma HLS INTERFACE ap_ctrl_none port=return

  data in;

  for(register int i0 = 0; i0 <= N; i0++)
  {
  in = fifo_4_1.read();

  if( i0 <= 47 && i0 >= 0 ){
    filter_0.write(in);
  } 
}
}