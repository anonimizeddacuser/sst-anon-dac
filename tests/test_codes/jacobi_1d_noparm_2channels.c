static
void kernel_jacobi_1d_imper(int tsteps,
			    int n,
			    DATA_TYPE POLYBENCH_1D(A,N,n),
			    DATA_TYPE POLYBENCH_1D(B,N,n))
{
  int t, i, j;

#pragma scop
  for (t = 0; t < 10; t++)
    {
      for (i = 1; i < 50 - 1; i++)
	B[i] = C[i] * (A[i-1] + A[i] + A[i + 1]);
      for (i = 1; i < 50 - 1; i++)
	A[i] = B[i];
    }
#pragma endscop

}
