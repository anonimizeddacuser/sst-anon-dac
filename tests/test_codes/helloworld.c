#include <stdio.h>
#include <stdlib.h>
#include "platform.h"
#include "xaxidma.h"
int init_dma(XAxiDma *axiDmaPtr, int deviceId)
{
	XAxiDma_Config *CfgPtr;
	XStatus status;
	CfgPtr = XAxiDma_LookupConfig(deviceId);
	if(!CfgPtr){
		printf("Error looking for AXI DMA config\n\r");
		return XST_FAILURE;
	}

	status = XAxiDma_CfgInitialize(axiDmaPtr,CfgPtr);
	if(status != XST_SUCCESS){
		printf("Error initializing DMA. Error code: %d\n\r",status);
		return XST_FAILURE;
	}
	XAxiDma_IntrDisable(axiDmaPtr, XAXIDMA_IRQ_ALL_MASK,XAXIDMA_DEVICE_TO_DMA);
	XAxiDma_IntrDisable(axiDmaPtr, XAXIDMA_IRQ_ALL_MASK,XAXIDMA_DMA_TO_DEVICE);

	return XST_SUCCESS;
}

#define D_TYPE float

int main()
{
    init_platform();
		srand(100);
	XAxiDma AxiDma0;
	XAxiDma AxiDma1;
	XAxiDma AxiDma2;

	XStatus Status;

	Xil_DCacheDisable();
	Xil_ICacheDisable();

register int i0, i1, i2, index;
	Status = init_dma(&AxiDma0,XPAR_AXI_DMA_0_DEVICE_ID);
	Status = init_dma(&AxiDma1,XPAR_AXI_DMA_1_DEVICE_ID);
	Status = init_dma(&AxiDma2,XPAR_AXI_DMA_2_DEVICE_ID);
	D_TYPE *p = (D_TYPE*)malloc(50*sizeof(D_TYPE));
	if(p == NULL){
		printf("\r\n--- 'p' malloc failed! --- \r\n");
	}
	for (i0 = 0; i0 < 50; i0++)
	{
			index = (i0);
			p[index] = rand() % 100;
		}

		Status = XAxiDma_SimpleTransfer(&AxiDma0,(u32) p, 50*sizeof(D_TYPE), XAXIDMA_DMA_TO_DEVICE);
		if (Status != XST_SUCCESS) {
			return XST_FAILURE;
		}
printf("\r\nstarting computation\r\n");
		Status = XAxiDma_SimpleTransfer(&AxiDma0,(u32) p, 50*sizeof(D_TYPE), XAXIDMA_DEVICE_TO_DMA);
		if (Status != XST_SUCCESS) {
			return XST_FAILURE;
		}
		while ((XAxiDma_Busy(&AxiDma0,XAXIDMA_DEVICE_TO_DMA))||(XAxiDma_Busy(&AxiDma0,XAXIDMA_DMA_TO_DEVICE)));
	for (i0 = 0; i0 < 50; i0++)
	{
			index = (i0);
				printf("PL:A[%d] = %f\n\r",i0,p[index]);
		}

printf("Finish test on SST_0\n\r");
	free(p);
	p = (D_TYPE*)malloc(50*50*sizeof(D_TYPE));
	if(p == NULL){
		printf("\r\n--- 'p' malloc failed! --- \r\n");
	}
	for (i1 = 0; i1 < 50; i1++)
	for (i0 = 0; i0 < 50; i0++)
	{
			index = (i0)+(i1)*50;
			p[index] = rand() % 100;
		}

		Status = XAxiDma_SimpleTransfer(&AxiDma1,(u32) p, 50*50*sizeof(D_TYPE), XAXIDMA_DMA_TO_DEVICE);
		if (Status != XST_SUCCESS) {
			return XST_FAILURE;
		}
printf("\r\nstarting computation\r\n");
		Status = XAxiDma_SimpleTransfer(&AxiDma1,(u32) p, 50*50*sizeof(D_TYPE), XAXIDMA_DEVICE_TO_DMA);
		if (Status != XST_SUCCESS) {
			return XST_FAILURE;
		}
		while ((XAxiDma_Busy(&AxiDma1,XAXIDMA_DEVICE_TO_DMA))||(XAxiDma_Busy(&AxiDma1,XAXIDMA_DMA_TO_DEVICE)));
	for (i1 = 0; i1 < 50; i1++)
	for (i0 = 0; i0 < 50; i0++)
	{
			index = (i0)+(i1)*50;
				printf("PL:A[%d][%d] = %f\n\r",i1,i0,p[index]);
		}

printf("Finish test on SST_1\n\r");
	free(p);
	p = (D_TYPE*)malloc(50*50*50*sizeof(D_TYPE));
	if(p == NULL){
		printf("\r\n--- 'p' malloc failed! --- \r\n");
	}
	for (i2 = 0; i2 < 50; i2++)
	for (i1 = 0; i1 < 50; i1++)
	for (i0 = 0; i0 < 50; i0++)
	{
			index = (i0)+(i1)*50+(i2)*50*50;
			p[index] = rand() % 100;
		}

		Status = XAxiDma_SimpleTransfer(&AxiDma2,(u32) p, 50*50*50*sizeof(D_TYPE), XAXIDMA_DMA_TO_DEVICE);
		if (Status != XST_SUCCESS) {
			return XST_FAILURE;
		}
printf("\r\nstarting computation\r\n");
		Status = XAxiDma_SimpleTransfer(&AxiDma2,(u32) p, 50*50*50*sizeof(D_TYPE), XAXIDMA_DEVICE_TO_DMA);
		if (Status != XST_SUCCESS) {
			return XST_FAILURE;
		}
		while ((XAxiDma_Busy(&AxiDma2,XAXIDMA_DEVICE_TO_DMA))||(XAxiDma_Busy(&AxiDma2,XAXIDMA_DMA_TO_DEVICE)));
	for (i2 = 0; i2 < 50; i2++)
	for (i1 = 0; i1 < 50; i1++)
	for (i0 = 0; i0 < 50; i0++)
	{
			index = (i0)+(i1)*50+(i2)*50*50;
				printf("PL:A[%d][%d][%d] = %f\n\r",i2,i1,i0,p[index]);
		}

printf("Finish test on SST_2\n\r");
	free(p);
    return 0;
}
