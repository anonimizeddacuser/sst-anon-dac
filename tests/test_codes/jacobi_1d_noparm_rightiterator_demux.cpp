#include <ap_int.h>
#include <hls_stream.h>

#define N 49

#define data_type float
template<int D> struct ap_axis{
  ap_int<D> data;
  ap_uint<1> last;
};

typedef ap_axis<sizeof(data_type)*8> data;
typedef hls::stream<data> data_stream;

void demux_0( data_stream &port_0,data_stream &port_1,data_stream &m_axis_port_2){ 
#pragma HLS INTERFACE axis port=port_0
#pragma HLS INTERFACE axis port=port_1
#pragma HLS INTERFACE axis port=m_axis_port_2
#pragma HLS INTERFACE ap_ctrl_none port=return

data in;
for (register int i0 = 0; i0 <= N; i0++) 
{
if(  i0 <= 48 && i0 >= 1 ){
in = port_0.read();
}
else{
in = port_1.read();
}
if( i0 == 49 ){
in.last = 1;
m_axis_port_2.write(in);
}
else {
in.last = 0;
m_axis_port_2.write(in);
}
}
}
