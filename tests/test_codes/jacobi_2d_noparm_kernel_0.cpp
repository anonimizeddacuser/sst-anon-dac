#include <ap_int.h>
#include <hls_stream.h>

#define N 49
#define O 49

#define data_type float
template<int D> struct ap_axis{
  ap_int<D> data;
  ap_uint<1> last;
};

typedef ap_axis<sizeof(data_type)*8> data;
typedef hls::stream<data> data_stream;

inline data_type pop(data const &e){
  union
  {
    int ival;
    data_type oval;
  } converter;
  converter.ival = e.data;
  data_type ret = converter.oval;
  return ret;
}
inline data push(data_type const &v, ap_uint<1> last = 0){
  data e;

  union
  {
    int oval;
    data_type ival;
  } converter;
  converter.ival = v;
  e.data = converter.oval;
  e.last = last ? 1 : 0;
  return e;
}

void kernel_0_0(data_stream &port_0,data_stream &port_1,data_stream &port_2,data_stream &port_3,data_stream &port_4,data_stream &demux){
#pragma HLS INTERFACE axis port=port_0
#pragma HLS INTERFACE axis port=port_1
#pragma HLS INTERFACE axis port=port_2
#pragma HLS INTERFACE axis port=port_3
#pragma HLS INTERFACE axis port=port_4
#pragma HLS INTERFACE axis port=demux
#pragma HLS INTERFACE ap_ctrl_none port=return

data_type acc_0, acc_1, acc_2, acc_3, acc_4, out;
data out_struct;
for (register int i0 = 1; i0 <= 48; i0 += 1)
  for (register int i1 = 1; i1 <= 48; i1 += 1)
{
 #pragma HLS LOOP_FLATTEN off
#pragma HLS PIPELINE II=1
acc_0 = pop(port_0.read());
 acc_1 = pop(port_1.read());
 acc_2 = pop(port_2.read());
 acc_3 = pop(port_3.read());
 acc_4 = pop(port_4.read());
 out = (0.2*(acc_0+acc_1+acc_2+acc_3+acc_4));
out_struct = push(out);
demux.write(out_struct);
}
}
