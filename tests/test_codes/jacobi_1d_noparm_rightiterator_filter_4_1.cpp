#include <ap_int.h>
#include <hls_stream.h>

#define N 49

#define data_type float
template<int D> struct ap_axis{
  ap_int<D> data;
  ap_uint<1> last;
};

typedef ap_axis<sizeof(data_type)*8> data;
typedef hls::stream<data> data_stream;

void filter_0_4_1(data_stream &fifo_4_2, data_stream &filter_1,data_stream &border,data_stream &fifo_4_1){
#pragma HLS INTERFACE axis port=fifo_4_2
#pragma HLS INTERFACE axis port=fifo_4_1
#pragma HLS INTERFACE axis port=border
#pragma HLS INTERFACE axis port=filter_1

#pragma HLS INTERFACE ap_ctrl_none port=return

  data in;

  for(register int i0 = 0; i0 <= N; i0++)
  {
  in = fifo_4_2.read();

  if( i0 <= 48 && i0 >= 1 ){
    filter_1.write(in);
    fifo_4_1.write(in);
  }  else{
    fifo_4_1.write(in);
border.write(in);
}
}
}