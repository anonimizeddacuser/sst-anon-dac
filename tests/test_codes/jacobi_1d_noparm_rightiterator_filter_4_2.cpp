#include <ap_int.h>
#include <hls_stream.h>

#define N 49

#define data_type float
template<int D> struct ap_axis{
  ap_int<D> data;
  ap_uint<1> last;
};

typedef ap_axis<sizeof(data_type)*8> data;
typedef hls::stream<data> data_stream;

void filter_0_4_2(data_stream &s_axis_dma_4_in, data_stream &filter_2,data_stream &fifo_4_2){
#pragma HLS INTERFACE axis port=s_axis_dma_4_in
#pragma HLS INTERFACE axis port=fifo_4_2
#pragma HLS INTERFACE axis port=filter_2

#pragma HLS INTERFACE ap_ctrl_none port=return

  data in;

  for(register int i0 = 0; i0 <= N; i0++)
  {
  in = s_axis_dma_4_in.read();

  if( i0 <= 49 && i0 >= 2 ){
    filter_2.write(in);
    fifo_4_2.write(in);
  }  else
    fifo_4_2.write(in);
}
}