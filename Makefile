PATH_CLAN_INCLUDE=/opt/polyhedral_tools/clan-install/include
PATH_SCOPLIB_INCLUDE=/opt/pocc-1.1/ir/install-scoplib/include/
PATH_CANDL_INCLUDE=/opt/polyhedral_tools/candl-install/include
PATH_PIPLIB_INCLUDE=/opt/pocc-1.1/math/install-piplib-hybrid/include/

PATH_CLAN_LIB=/opt/polyhedral_tools/clan-install/lib
PATH_SCOPLIB_LIB=/opt/pocc-1.1/ir/install-scoplib/lib/
PATH_CANDL_LIB=/opt/polyhedral_tools/candl-install/lib
PATH_PIPLIB_LIB=/opt/polyhedral_tools/candl/piplib/.libs

INCLUDES=-I$(PATH_CLAN_INCLUDE) -I$(PATH_CANDL_INCLUDE)

LIBRARIES=-L$(PATH_CLAN_LIB) -L$(PATH_CANDL_LIB)

LIB=-lcandl -lclan -losl -lpiplibMP 

LD_FLAG=-Wl,-rpath=$(PATH_CLAN_LIB),-rpath=$(PATH_CANDL_LIB),-rpath=$(PATH_PIPLIB_LIB)

SRCDIR=src
OBJDIR=obj


SOURCES = $(wildcard $(SRCDIR)/*.c)

OBJ = $(SOURCES:$(SRCDIR)/%.c=$(OBJDIR)/%.o)



TESTSRCDIR=tests
TESTOBJDIR=obj_test

OBJTOTEST = $(filter-out obj/clan.o,$(OBJ))
TESTSOURCES = $(wildcard $(TESTSRCDIR)/*.c)
TESTOBJ  = $(TESTSOURCES:$(TESTSRCDIR)/%.c=$(TESTOBJDIR)/%.o)

polyfpga: $(OBJ) 
	gcc \
	$(INCLUDES) \
	$(OBJ) \
	$(LIBRARIES) \
	$(LIB) \
	$(LD_FLAG) -g -O0 -o polyfpga

$(OBJDIR)/%.o: $(SRCDIR)/%.c
	gcc \
	$(INCLUDES) \
	-g -c $< -o $@ 

test: $(TESTOBJ)
	@gcc \
	$(INCLUDES) \
	$(TESTOBJ) \
	$(OBJTOTEST) \
	$(LIBRARIES) \
	$(LIB) \
	$(LD_FLAG) -g  -o test
	@./test
	@-rm -f test

debug: $(TESTOBJ)
	@gcc \
	$(INCLUDES) \
	$(TESTOBJ) \
	$(OBJTOTEST) \
	$(LIBRARIES) \
	$(LIB) \
	$(LD_FLAG) -g  -o test

$(TESTOBJDIR)/%.o: $(TESTSRCDIR)/%.c
	@gcc \
	$(INCLUDES) \
	-g -c $< -o $@ 

clean: 
	-rm -f polyfpga tmp.* $(OBJDIR)/*.o bitstream_SST_out vivado_bitstream.tcl vivado.jou vivado.log bitstream_vivado_output webtalk.*
	-rm -rf scop_* complete_hw
	-rm -rf splitter*
	-rm -f $(TESTOBJDIR)/*.o
