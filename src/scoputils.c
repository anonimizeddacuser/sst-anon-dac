/*
 *   This file is part of PolyFPGA.
 *
 *   PolyFPGA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *   Copyright (C) 2015 Giulio Stramondo
 *
 */
 
 #include "scoputils.h"

/*get_cell_from_osl_relation (osl_relation_p access_relation, osl_scop_p scop, int row, int col)
    input: a pointer to an osl_relation ( which is supposed to be an access to an array)
    intput: a pointer to a scop ( where the relaation is taken from )
    input : the row of the matrix we want to have returned
    input : the col of the matrix we want to have returned
    return : the element in the access_relation matrix at position row col ( as int)
*/
int get_cell_from_osl_relation (osl_relation_p access_relation, osl_scop_p scop, int row, int col)
{
  osl_names_p names = NULL;
  if (scop == NULL)
    names = NULL;
  else
    names = osl_scop_names(scop);

  char *access_tmp_rel;
  char *selected_row;
  char *selected_item;
  int item;
  int i = 0;

  if (row > ( access_relation->nb_rows - 1 ) || col > ( access_relation->nb_columns - 1) ) {
    printf("[Error] Tring to print value outside the relation matrix (get_cell_from_osl_relation)\n");
    return 0;
  }

  access_tmp_rel = osl_relation_spprint_polylib(access_relation, names);

  strtok (access_tmp_rel, "\n");
  strtok (NULL, "\n");

  for (i = 0; i <= row; i++)
    selected_row = strtok (NULL, "\n");

  selected_item = strtok (selected_row, " ");
  for (i = 0; i < col; i++)
    selected_item = strtok (NULL, " ");

  item = atoi (selected_item);

  return item;

}

/*get_array_id_from_osl_relation
  input : access_relation
  input : scop
  return : returns the id of the array involved in the relation
*/
int get_array_id_from_osl_relation( osl_relation_p access_relation, osl_scop_p scop) {
  int nb_columns = access_relation->nb_columns;
  int array_id = get_cell_from_osl_relation(access_relation, scop, 0, nb_columns - 1);
  return array_id;
}

void test_get_cell_from_osl_relation (osl_scop_p scop) {
  //TESTING GET ITEM FROM OSL RELATION
  printf("\n\033[33mTESTING get_cell_from_osl_relation:\033[00m\n");
  int tmp;
  int j, k;
  char *test_buffer;
  osl_names_p names = osl_scop_names(scop);
  test_buffer = osl_relation_spprint_polylib(scop->statement->access->elt, names);
  printf("%s\n", test_buffer);
  printf("nb_rows: %d, nb_columns: %d,nb_output_dims: %d,nb_input_dims: %d,nb_local_dims: %d, nb_parameters: %d\n",
         scop->statement->access->elt->nb_rows,
         scop->statement->access->elt->nb_columns,
         scop->statement->access->elt->nb_output_dims,
         scop->statement->access->elt->nb_input_dims,
         scop->statement->access->elt->nb_local_dims,
         scop->statement->access->elt->nb_parameters);


  for (j = 0; j < 3; ++j)
  {
    for (k = 0; k < 9; ++k)
    {
      tmp = get_cell_from_osl_relation(scop->statement->access->elt, scop, j, k );
      printf("cell %d %d -> %d\n", j, k, tmp);
    }
  }

  tmp = get_cell_from_osl_relation(scop->statement->access->elt, scop, 1, 0 );
  printf("cell %d %d -> %d\n", 1, 0, tmp);
  return;
}

char* pprint_access_from_relation( osl_relation_p access_relation, osl_scop_p scop,  osl_statement_p statements) {
  char* access = (char*) malloc(sizeof(char) * 10);
  osl_arrays_p arrays;
  osl_body_p body = NULL;
  body = (osl_body_p)osl_generic_lookup(statements->extension, OSL_URI_BODY);
  int tmp_id =  get_array_id_from_osl_relation( access_relation, scop);
  tmp_id --;
  arrays = osl_generic_lookup(scop->extension, OSL_URI_ARRAYS);
  sprintf(access, "%s [%s ][%s  ]", arrays->names[tmp_id], body->iterators->string[0], body->iterators->string[1]);
  return access;
}

void my_print_statements_access(FILE* output, osl_scop_p scop) {

  osl_statement_p statements = scop->statement;
  osl_body_p body = NULL;
  osl_arrays_p arrays;
  arrays = osl_generic_lookup(scop->extension, OSL_URI_ARRAYS);
  int i = 0;
  while (statements != NULL) {
    fprintf(output, "S%d:\n", i);
    body = (osl_body_p)osl_generic_lookup(statements->extension, OSL_URI_BODY);
    //fprintf(output,"original:  ");
    osl_strings_print(output, body->expression);

    get_access_str_from_statement(body->expression->string[0]);

    int access_relations = osl_relation_list_count(statements->access);
    fprintf(output, "1 write, %d reads\n",   access_relations - 1 );
    int columns = statements->access->elt->nb_columns;
    char *access_tmp_rel;
    //access_tmp_rel=osl_relation_spprint_polylib(statements->access->elt,names);
    // fprintf(output,"%s\n", access_tmp_rel);
    get_array_id_from_osl_relation( statements->access->elt, scop);
    osl_relation_list_p   access = statements->access;
    int tmp_id =  get_array_id_from_osl_relation( access->elt, scop);
    tmp_id --;
    fprintf(output, "W %s\n", arrays->names[tmp_id]);
    access = access->next;
    while (access != NULL) {
      tmp_id =  get_array_id_from_osl_relation( access->elt, scop);
      tmp_id --;
      fprintf(output, "R %s\n", arrays->names[tmp_id]);

      printf("PRINT ACCESS FROM RELATION\n");
      fprintf(output, "%s\n", pprint_access_from_relation (access->elt, scop, statements));
      access = access->next;

    }
    fprintf(output, "\n");
    statements = statements->next;
    i++;

  }
  // test_get_cell_from_osl_relation ( scop);

  return;
}


void my_print_statements_access_from_statement_body(FILE* output, osl_scop_p scop) {

  osl_statement_p statements = scop->statement;
  osl_body_p body = NULL;
  osl_arrays_p arrays;
  arrays = osl_generic_lookup(scop->extension, OSL_URI_ARRAYS);
  int i = 0, j = 0;
  while (statements != NULL) {
    fprintf(output, "S%d:\n", i);
    body = (osl_body_p)osl_generic_lookup(statements->extension, OSL_URI_BODY);
    //fprintf(output,"original:  ");
    osl_strings_print(output, body->expression);

    char ** access = get_access_str_from_statement(body->expression->string[0]);

    int access_relations = osl_relation_list_count(statements->access);
    fprintf(output, "1 write, %d reads\n",   access_relations - 1 );

    fprintf(output, "W %s\n", access[0]);

    for (j = 1; j < access_relations; j++) {

      fprintf(output, "R %s\n", access[j]);

    }
    fprintf(output, "\n");
    statements = statements->next;
    i++;

  }
  // test_get_cell_from_osl_relation ( scop);

  return;
}

void my_print_scop_infos(FILE* output, osl_scop_p scop) {
  int i = 0;

  fprintf(output, "There were %d statement in the scop\n", osl_statement_number(scop->statement));
  if (scop->parameters != NULL)
    osl_strings_print(output, scop->parameters->data);

  osl_arrays_p arrays;
  arrays = osl_generic_lookup(scop->extension, OSL_URI_ARRAYS);
  osl_names_p names = osl_scop_names(scop);

  osl_strings_print(output, osl_arrays_to_strings(arrays));

  fprintf(output, "\n\n\033[33mDUMP ARRAYS:\033[00m\n");
  for (i = 0; i < arrays->nb_names; ++i)
  {
    fprintf(output, "iteration:%d\tnumber:%d\tname:%s\n", i, arrays->id[i], arrays->names[i]);
  }
  fprintf(output, "\n\n\033[33mSTATEMENTS ACCESS:\033[00m\n");
  my_print_statements_access_from_statement_body(output, scop);

}



int count_spaces_in_statement(const char* statement) {
  int count = 0;
  while (*statement != '\0') {
    if (*statement == ' ')count ++;
    statement++;
  }
  return count;
}

int count_accesses_in_statement(char* statement) {
  int count = 0;
  while (*statement != '\0') {
    if (*statement == ']' && *(statement + 1) != '[')count ++;
    statement++;
  }
  return count;
}

char* remove_spaces_from_statement ( char* statement) {

  int spaces = count_spaces_in_statement(statement);

  char *result;
  result = strdup(statement);
  if (spaces == 0) return result;
  int size_result = strlen(statement) - spaces + 1;



  result = (char *) malloc (sizeof(char) * size_result);
  char* tmp_ptr_orig = statement;
  char* tmp_ptr_res = result;

  while (*tmp_ptr_orig != '\0') {
    if (*tmp_ptr_orig != ' ') {
      *tmp_ptr_res = *tmp_ptr_orig;
      tmp_ptr_res++;
    }
    tmp_ptr_orig++;
  }
  *tmp_ptr_res = '\0';
  return result;
}


char** get_access_str_from_statement(char * statement) {

  char *local_stmt = remove_spaces_from_statement(statement);
  int access = count_accesses_in_statement(statement);
  //char *result[access];
  char **result = (char **) malloc(sizeof(char*)*access);


  char *tmp_head_ptr = local_stmt;
  int index = 0, string_over = 0;

  while (*local_stmt != ';' && !string_over) {
    //printf("%c\n",*local_stmt);
    //local_stmt++;
    if ( (*local_stmt > 64 && *local_stmt < 91) || //Uppercase Letter
         (*local_stmt > 96 && *local_stmt < 123)) //lowercase Letter
    {
      local_stmt++;
      while ((*local_stmt > 64 && *local_stmt < 91) || //Uppercase Letter
             (*local_stmt > 96 && *local_stmt < 123) || //lowercase Letter
             (*local_stmt > 47 && *local_stmt < 58)) //number
      {
        local_stmt++;
      }
      if (*local_stmt == '[') {
        while (*local_stmt == '[') {
          local_stmt++;
          while (*local_stmt != ']') {
            local_stmt++;
          }
          local_stmt++;
        }
        if (*local_stmt == ';')
          string_over = 1;
        *local_stmt = '\0';
        result[index] = strdup(tmp_head_ptr);
        index++;

        local_stmt++;
        tmp_head_ptr = local_stmt;
        continue;
      }
      else {
        local_stmt++;
        tmp_head_ptr = local_stmt;
        continue;
      }
    }

    local_stmt++;
    tmp_head_ptr = local_stmt;
  }
  return result;
}