/*
 *   This file is part of PolyFPGA.
 *
 *   PolyFPGA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * 	 Copyright (C) 2015 Giulio Stramondo
 *
 */
 
#ifndef IP_GENERATOR_H
#define IP_GENERATOR_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "struct.h"
#include "error.h"

#ifdef THIS_MOD
#undef THIS_MOD
#endif
#define THIS_MOD IP_GEN_MOD

//Number of board we are currently handlying
#define BOARD_NB 2

//Associate each board to an ID
#define ZYBO 0
#define VIRTEX7 1

char *get_board_part_name(int BOARD_ID);

char *get_hls_command(int BOARD_ID);

void generate_IPs(options* opt, int BOARD_ID);

void generate_vivado_tcl(options* opt, int BOARD_ID);

void package_SST_IP(options* opt);

int get_closest_FIFO_size(int desidered_size);

#endif