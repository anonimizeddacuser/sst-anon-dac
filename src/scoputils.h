/*
 *   This file is part of PolyFPGA.
 *
 *   PolyFPGA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *   Copyright (C) 2015 Giulio Stramondo
 *
 */
 
#ifndef SCOPUTILS_H
#define SCOPUTILS_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <clan/clan.h>
#include <osl/scop.h>

#include <osl/scop.h>
#include <osl/macros.h>
#include <osl/util.h>
#include <osl/extensions/dependence.h>
#include <osl/extensions/arrays.h>
#include <candl/macros.h>
#include <candl/dependence.h>
#include <candl/violation.h>
#include <candl/options.h>
#include <candl/scop.h>
#include <candl/util.h>
#include <candl/piplib.h>


#include "struct.h"


/*get_cell_from_osl_relation (osl_relation_p access_relation, osl_scop_p scop, int row, int col)
    input: a pointer to an osl_relation ( which is supposed to be an access to an array)
    input: a pointer to a scop ( where the relation is taken from )
    input : the row of the matrix we want to have returned
    input : the col of the matrix we want to have returned
    return : the element in the access_relation matrix at position row col ( as int)
*/
int get_cell_from_osl_relation (osl_relation_p access_relation, osl_scop_p scop, int row, int col);

/*get_array_id_from_osl_relation
  input : access_relation
  input : scop
  return : returns the id of the array involved in the relation
*/
int get_array_id_from_osl_relation( osl_relation_p access_relation, osl_scop_p scop);

char* pprint_access_from_relation( osl_relation_p access_relation, osl_scop_p scop,  osl_statement_p statements);

void test_get_cell_from_osl_relation (osl_scop_p scop);

void my_print_statements_access(FILE* output, osl_scop_p scop);

void my_print_scop_infos(FILE* output, osl_scop_p scop);

/*
    get_access_str_from_statement
    input: the body of a statement (string )
    returns: an array containing the access in order

*/
char** get_access_str_from_statement(char * statement);

char* remove_spaces_from_statement ( char* statement);

#endif
