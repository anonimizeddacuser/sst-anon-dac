/*
 *   This file is part of PolyFPGA.
 *
 *   PolyFPGA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * 	 Copyright (C) 2015 Giulio Stramondo
 *
 */
 

#ifndef PARSER_H
#define PARSER_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <clan/clan.h>
#include <osl/scop.h>

#include <osl/scop.h>
#include <osl/macros.h>
#include <osl/util.h>
#include <osl/extensions/dependence.h>
#include <osl/extensions/arrays.h>
#include <candl/macros.h>
#include <candl/dependence.h>
#include <candl/violation.h>
#include <candl/options.h>
#include <candl/scop.h>
#include <candl/util.h>
#include <candl/piplib.h>
#include "scoputils.h"

#ifdef THIS_MOD
#undef THIS_MOD
#endif
#define THIS_MOD PARSER_MOD



/**
 * @brief Parses the program's input arguments
 * The return type is a pointer to an option data structure defined in struct.h
 * @param argc Number of argument passed to the program.
 * @param argv[] Array of strings passed to the program.
 * @returns options Option structure where the informations about the program root directory,
 *					input source program, and number of steps ( of the toolchain ) to execute are filled in. 
*/
options* parse_args(int argc ,char* argv[]);



/**
 * @brief Parses the source given as input to the program.
 * Creates the folder structure that will hold the scop related informations, one folder for each scop that
 * appears in the input source code. The folders are named scop_id where id is an identificative number for the scop,
 * that follows the order of appearence in the input source code.
 * Inside each scop folder the following files are created:
 * @p tmp.access.dot that stores the memory accesses performed by the scop,
 * @p tmp.clan.scop that stores the output of the analysis of clan,
 * @p tmp.candl.dot that stores the output of candl containing the scop's data-dependency analysis in .dot format.
 *
 * For each scop, a data of type scop_context is allocated, where the following fields are filled:
 * @p scop, with the output of clan (with osl_scop_p type),
 * @p path, with the path to the folder containing the scop data ( as a string ),
 * @p dep, with the output of candl ( with osl_dependence_p type ),
 * @p id, number identifying the scop, following the order of appearence in the input source code (as int).
 *
 * All the scop_context created are linked to each other in a single-linked list, the head of the list is stored
 * into the @p context field of the option typed variable given as input.
 * @param opt the struct that is going to be updated with the scop informations.
 * @see options
 * @see scop_context
 * @see http://icps.u-strasbg.fr/~bastoul/development/openscop/docs/openscop.pdf
 */
int parse_input(options* opt);

/**
 * @brief Parses the source given as input to the program.
 * Does the same thing as parse_input, but it doesn't create any file or folder.
 *
 * For each scop, a data of type scop_context is allocated, where the following fields are filled:
 * @p scop, with the output of clan (with osl_scop_p type),
 * @p path, with the path to the folder containing the scop data ( as a string ),
 * @p dep, with the output of candl ( with osl_dependence_p type ),
 * @p id, number identifying the scop, following the order of appearence in the input source code (as int).
 *
 * All the scop_context created are linked to each other in a single-linked list, the head of the list is stored
 * into the @p context field of the option typed variable given as input.
 * @param opt the struct that is going to be updated with the scop informations.
 * @see options
 * @see scop_context
 * @see parse_input
 * @see http://icps.u-strasbg.fr/~bastoul/development/openscop/docs/openscop.pdf
 */
void parse_input_no_dump(options* opt);

/**
 * @brief Prints into stdout a summary of the information contained into the given scop_context
 * @param cont, the scop_context variable that will be summaryzed in stdout
 * @see scop_context
 */

void dump_context (scop_context* cont);

// TODO
//move the following to scoputils ( and include scoputils)


/**
 * @brief Dumps the output of clan into a file
 *        the input given to clan is the osl_scop_p variable given as input.
 *
 * @param scop, the (osl_scop_p) data structure holding the informations about the scop.
 * @param filename, the name of the file that will be created where the informations about the scop are going to be
 *         printed. 
 */
void dump_scop_to_file(osl_scop_p scop, char *filename);

osl_dependence_p get_dependency_from_scop_file(char *filename);

void dump_deps_to_file(osl_dependence_p dep, char *filename);

void dump_access_to_file(osl_scop_p scop,char *filename);
//copies just the current scop, not the following
osl_scop_p copy_current_scop(osl_scop_p input_scop);

void print_usage();
#endif