/*
 *   This file is part of PolyFPGA.
 *
 *   PolyFPGA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * 	 Copyright (C) 2015 Giulio Stramondo
 *
 */
 
#ifndef SOAG_H
#define SOAG_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <clan/clan.h>
#include <osl/scop.h>

#include <osl/scop.h>
#include <osl/macros.h>
#include <osl/util.h>
#include <osl/extensions/dependence.h>
#include <osl/extensions/arrays.h>
#include <candl/macros.h>
#include <candl/dependence.h>
#include <candl/violation.h>
#include <candl/options.h>
#include <candl/scop.h>
#include <candl/util.h>
#include <candl/piplib.h>

#include "struct.h"
#include "barvinok_interface.h"
#include "error.h"
#include "scoputils.h"

#ifdef THIS_MOD
#undef THIS_MOD
#endif
#define THIS_MOD SOAG_MOD


#define READ 0
#define WRITE 1

//opt has to have the informations about the dimension and the size
//of the stencil array
void generate_soag_data(options *opt);

void generate_soag_data_no_dump(options *opt);

int get_stancil_array_ID(scop_context *tmp_context);

osl_relation_list_p select_access_to_stancil_array(scop_context *tmp_context);

int get_stancil_array_DIM(scop_context *tmp_context);

int check_dimension_coherency(scop_context *tmp_context);

void dump_SST (struct soag_SST* SST);

int *compute_stancil_array_dimensions(scop_context *tmp_context);

void create_channels(scop_context *tmp_context);

int cmp_GT_filters(struct soag_filter *A, struct soag_filter *B);

struct soag_filter *create_filter(int filter_ID, struct sog_access* access,
                                  scop_context *tmp_context);

int *compute_filter_lexmin(scop_context *tmp_context, struct sog_access* access);

int *compute_stancil_array_dimensions_parse_res(char* iscc_res);

void create_channel_links(struct soag_channel* channel);

struct soag_kernel *create_kernels(scop_context *tmp_context);

struct soag_kernel *create_kernels_helper(int kernel_ID, struct sog_access* access, scop_context *tmp_context);

void create_channel_ports(struct soag_channel *channels);

struct soag_channel_port *get_channel_port(int port_type, struct sog_access *access, struct soag_channel *channels);

struct soag_filter *get_filter(struct sog_access *access,struct soag_channel *channels);

void make_channels_kernels_links(struct soag_SST* SST);

int compute_fifo_size(struct soag_filter *higher, struct soag_filter *lower);

void dump_soag_dot(FILE *fp, struct soag_SST* SST);

void create_demux(scop_context *tmp_context);

#endif