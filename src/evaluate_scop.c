/*
 *   This file is part of PolyFPGA.
 *
 *   PolyFPGA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * 	 Copyright (C) 2015 Giulio Stramondo
 *
 */

#include "evaluate_scop.h"

int test_scops(options *opt) {
	int global_error = 0 ;
	//here call all the test to be performed
	test_scops_for_parameters(opt);

	global_error = test_queuing_args(opt);

	if(global_error){
		return global_error;
	}

	return 0;
}

int test_queuing_args(options *opt) {
	scop_context *tmp_context = NULL;
	tmp_context = opt->contexts;
	osl_scop_p tmp_scop = NULL;
	struct queuing* queuing = opt->queuing;
	if(queuing == NULL)
		return 0;
	
	int scop_nb = 0;
	while (tmp_context != NULL) {
		scop_nb++;
		tmp_context = tmp_context->next;
	}

	if (scop_nb != queuing->arg_nb) {
		return WRONG_PARSING_ARG_NUMBER;
	}

	return 0;
}

void test_scops_for_parameters(options *opt) {
	scop_context *tmp_context = NULL;
	tmp_context = opt->contexts;
	osl_scop_p tmp_scop = NULL;

	while (tmp_context != NULL) {
		tmp_scop = tmp_context->scop;
		if (tmp_scop->parameters != NULL) {
			set_error(tmp_context, PARAMETERS, THIS_MOD);
		}
		tmp_context = tmp_context->next;
	}
	return;
}