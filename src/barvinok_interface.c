/*
 *   This file is part of PolyFPGA.
 *
 *   PolyFPGA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * 	 Copyright (C) 2015 Giulio Stramondo
 *
 */

#include "barvinok_interface.h"

char* query_barvinok(char* query) {
	FILE *result_fp = NULL;
	char complete_query[2100];
	char *result = (char *) malloc (sizeof(char) * 500);
	complete_query[0] = '\0';
	strcat (complete_query, "echo '");
	strcat (complete_query, query);
	strcat (complete_query, "'");
	strcat(complete_query, "| ");
	strcat(complete_query, ISCC);
	strcat(complete_query, "> iscc.res");
	system(complete_query);
	result_fp = fopen("iscc.res", "r");
	//fscanf(result_fp, "%s\n", result);
	fgets(result, 500, result_fp);
	fclose(result_fp);
	system("rm iscc.res");
	return result;
}

char* query_barvinok_full(char* query) {
	FILE *result_fp = NULL;
	char complete_query[2100];
	char buff[50];

	char *result = (char *) malloc (sizeof(char) * 500);
	result[0] = '\0';
	complete_query[0] = '\0';
	strcat (complete_query, "echo '");
	strcat (complete_query, query);
	strcat (complete_query, "'");
	strcat(complete_query, "| ");
	strcat(complete_query, ISCC);
	strcat(complete_query, "> iscc.res");
	system(complete_query);
	result_fp = fopen("iscc.res", "r");
	//fscanf(result_fp, "%s\n", result);

	while (fgets(buff, 500, result_fp) != NULL) {
		strcat(result, buff);
	}

	fclose(result_fp);
	system("rm iscc.res");
	return result;
}


int same_data_domain(osl_relation_p D1, osl_relation_p A1, osl_relation_p D2, osl_relation_p A2) {
	char query [2000];
	query[0] = '\0';
	char *dom1;
	char *dom2;
	char *acc1;
	char *acc2;

	dom1 = get_domain_barvinok(D1);
	dom2 = get_domain_barvinok(D2);
	acc1 = get_access_map_barvinok(D1, A1);
	acc2 = get_access_map_barvinok(D2, A2);

	strcat(query, "D1 :=");
	strcat(query, dom1);
	strcat(query, ";\n");

	strcat(query, "D2 :=");
	strcat(query, dom2);
	strcat(query, ";\n");

	strcat(query, "A1 :=");
	strcat(query, acc1);
	strcat(query, ";\n");

	strcat(query, "A2 :=");
	strcat(query, acc2);
	strcat(query, ";\n");

	strcat(query, "A1(D1)=A2(D2);");
	//printf("Query: %s\n", query);
	char *result = NULL;
	result = query_barvinok(query);
	//printf("result: %s\n",result );

	int bool_ans = -1;

	if (strcmp(result, "True\n") == 0)
		bool_ans = 1;
	if (strcmp(result, "False\n") == 0)
		bool_ans = 0;
	//printf("returned : %d\n",bool_ans);
	free(result);
	return bool_ans;
}

char* get_domain_barvinok(osl_relation_p D1) {
	char* domain = osl_relation_spprint_polylib(D1, NULL);
	int i = 0;
	int state = 0;

	for ( i = 0; domain[i] != '\n'; i++)
		switch (state) {
		case (0):
			if (domain[i] != ' ')
				state++;
			break;
		case (1):
			if (domain[i] == ' ')
				state++;
			break;
		case (2):
			if (domain[i] != ' ')
				state++;
			break;
		case (3):
			if (domain[i] == ' ')
				state++;
			break;
		case (4):
			if (domain[i] != ' ')
				domain[i] = ' ';
			break;
		}
	return domain;
}

char* get_access_map_barvinok(osl_relation_p domain, osl_relation_p access) {
	char * res = (char*) malloc (sizeof(char) * 100);
	int i = 0;

	res[0] = '\0';

	strcat(res, "{ [ ");


	for (i = 0; i < (domain->nb_output_dims); i++) {
		char* tmp = (char*) malloc(sizeof(char) * 6);
		if (i == (domain->nb_output_dims) - 1)
			sprintf(tmp, "i%d ", i);
		else
			sprintf(tmp, "i%d, ", i);

		strcat(res, tmp);
	}
	strcat(res, "] -> [ ");

	strcat(res, get_access_barvinok(access));

	strcat(res, "] }");
	return res;
}

char* map_to_project_away_time_dim(osl_relation_p domain) {
	char * res = (char*) malloc (sizeof(char) * 100);
	int i = 0;

	res[0] = '\0';

	strcat(res, "{ [ ");

	for (i = 0; i < (domain->nb_output_dims); i++) {
		char* tmp = (char*) malloc(sizeof(char) * 6);
		if (i == (domain->nb_output_dims) - 1)
			sprintf(tmp, "i%d ", i);
		else
			sprintf(tmp, "i%d, ", i);

		strcat(res, tmp);
	}
	strcat(res, "] -> [ ");

	for (i = 1; i < (domain->nb_output_dims); i++) {
		char* tmp = (char*) malloc(sizeof(char) * 6);
		if (i == (domain->nb_output_dims) - 1)
			sprintf(tmp, "i%d ", i);
		else
			sprintf(tmp, "i%d, ", i);

		strcat(res, tmp);
	}
	strcat(res, "] }");
	return res;
}
char* get_access_barvinok(osl_relation_p access) {

	int i = 0;
	int j = 0;
	char * res = (char *) malloc (sizeof(char) * 50);
	char *tmp = (char *) malloc (sizeof(char) * 10);

	for (i = 0; i < 50; ++i)
	{
		res[i] = '\0';
	}
	res[49] = '\0';
	int nb_rows = access -> nb_rows;
	int nb_columns = access -> nb_columns;
	int dims = access -> nb_input_dims;

	int tmp_access = 0;
	int has_iterators = 0;

	for (i = 1 ; i < nb_rows; i ++) {
		if (i > 1) {
			strcat(res, ",");
			has_iterators = 0;
		}

		for (j = nb_rows + 1 ; j < nb_rows + 1 + dims; j++) {
			tmp_access = get_cell_from_osl_relation(access, NULL, i, j);
			if (tmp_access != 0) {
				if ((tmp_access * tmp_access) != 1) {
					if (!has_iterators) {
						has_iterators = 1;
						sprintf(tmp, "%d * i%d", tmp_access, j - (nb_rows + 1));
					}
					else {
						if (tmp_access > 0)
							sprintf(tmp, " + %d * i%d", tmp_access, j - (nb_rows + 1));
						else
							sprintf(tmp, "%d * i%d", tmp_access, j - (nb_rows + 1));
					}
				} else {
					if (tmp_access == 1) {
						if (has_iterators) {
							sprintf(tmp, " + i%d", j - (nb_rows + 1));
						}
						else {
							sprintf(tmp, " i%d", j - (nb_rows + 1));
							has_iterators = 1;
						}
					} else {
						sprintf(tmp, "-i%d", j - (nb_rows + 1));
					}
				}

				strcat(res, tmp);
			}

		}
		tmp_access = get_cell_from_osl_relation(access, NULL, i, nb_columns - 1);
		if (tmp_access != 0) {
			if (!has_iterators) {
				sprintf(tmp, "%d", tmp_access);
			} else {
				if (tmp_access > 0)
					sprintf(tmp, " + %d", tmp_access);
				else
					sprintf(tmp, " %d", tmp_access);
			}
			strcat(res, tmp);
		}
	}
	free(tmp);
	return res;
}

void clean_loops_output(char* input) {
	char *nl_pointer;
	char *string_it;
	for (string_it = input; * (string_it + 2) != '\0' ; string_it++) {
		if (*string_it == '\n')
			nl_pointer = string_it;
	}

	*nl_pointer = '\0';
	return;
}

void convert_loop_iterators(char* input) {
	char *string_it;
	for (string_it = input; *string_it != '\0' ; string_it++) {
		if ( *string_it == 'c') {
			*string_it = 'i';
		}
	}
	return;
}

char* add_register_int_to_loop(char* input) {
	char *string_it;
	char *output = (char *) malloc (sizeof(char)*500);
	output[0] = '\0';

	int i = 0;

	for (string_it = input; *string_it != '\0'; string_it++) {
		if (*string_it == 'f') {
			output[i] = *string_it ;
			i++;
			string_it++;
			if (*string_it == 'o') {
				output[i] = *string_it ;
				i++;
				string_it++;
				if (*string_it == 'r') {
					output[i] = *string_it ;
					i++;
					string_it++;
					if (*string_it == ' ') {
						output[i] = *string_it ;
						i++;
						string_it++;
						if (*string_it == '(') {
							output[i] = *string_it ;
							i++;
							string_it++;

							output[i] = '\0';
							strcat(output, "register ");
							i +=9;
						}
					}
				}
			}

		}

		output[i] = *string_it ;
		i++;
		
	}
	output[i] = '\0';

	return output;
}

char* get_loops_from_domain(osl_relation_p D1) {
	char query [2000];
	query[0] = '\0';

	char *dom1;
	char *map_no_time_dim;
	char *result;

	dom1 = get_domain_barvinok(D1);
	map_no_time_dim = map_to_project_away_time_dim(D1);

	strcat(query, "D1 :=");
	strcat(query, dom1);
	strcat(query, ";\n");

	strcat(query, "T :=");
	strcat(query, map_no_time_dim);
	strcat(query, ";\n");

	strcat(query, "D2:=T(D1);");

	strcat(query, "codegen D2;");
	result = query_barvinok_full(query);
	clean_loops_output(result);
	convert_loop_iterators(result);
	result=add_register_int_to_loop(result);
	return result;
}

char* get_filter_equation(osl_relation_p D1, osl_relation_p A1) {
	char query [2000];
	query[0] = '\0';

	char *dom1;
	char *map_no_time_dim;
	char* acc1;
	dom1 = get_domain_barvinok(D1);

	map_no_time_dim = map_to_project_away_time_dim(D1);

	acc1 = get_access_map_barvinok(D1, A1);


	strcat(query, "D1 :=");
	strcat(query, dom1);
	strcat(query, ";\n");

	strcat(query, "T :=");
	strcat(query, map_no_time_dim);
	strcat(query, ";\n");

	strcat(query, "A1 :=");
	strcat(query, acc1);
	strcat(query, ";\n");

	strcat(query, "D2:=T(D1);");
	//strcat(query, "A1(D1)*D2;");
	strcat(query, "A1(D1);");

	char *result = NULL;
	//printf("QUERY:\n%s\n", query );
	result = query_barvinok(query);

	//printf("result:%s\n", result);
	char *substring = NULL;

	substring = strtok(result, ":}");
	//printf("substring:%s\n", substring);
	substring = strtok(NULL, ":}");
	//printf("substring:%s\n", substring);
	substring = str_replace(substring, "and", "&&");

	substring = strdup(substring);
	free(result);
	//printf("final condition:%s\n", substring);
	return substring;
}

char* test_get_filter_equation(options* opt) {

	scop_context *tmp_context = NULL;
	tmp_context = opt->contexts;
	FILE* sog_output;

	while (tmp_context != NULL) {
		//checks if this scop has encoutered an error before
		//if so skips to the next scop
		if (encountered_error(tmp_context)) {
			tmp_context = tmp_context->next;
			continue;
		}

		sog_statement *sog_stmts = opt->contexts->sog_stmts;

		sog_access *test_access;
		test_access = get_sog_access(sog_stmts, 0, 1);

		char * condition = NULL;
		condition = get_filter_equation(test_access->statement->domain , test_access->access_relation);
		printf("(test_get_filter_equation) condition:%s\n", condition);

		tmp_context = tmp_context->next;
	}

}

