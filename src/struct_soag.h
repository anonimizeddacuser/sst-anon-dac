/*
 *   This file is part of PolyFPGA.
 *
 *   PolyFPGA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *   Copyright (C) 2015 Giulio Stramondo
 *
 */
 
#ifndef STRUCT_SOAG_H
#define STRUCT_SOAG_H


#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <clan/clan.h>
#include <osl/scop.h>

#include <osl/scop.h>
#include <osl/macros.h>
#include <osl/util.h>
#include <osl/extensions/dependence.h>
#include <osl/extensions/arrays.h>
#include <candl/macros.h>
#include <candl/dependence.h>
#include <candl/violation.h>
#include <candl/options.h>
#include <candl/scop.h>
#include <candl/util.h>
#include <candl/piplib.h>

#include "struct_sog.h"

//---------------------------------
// TODO 

// probably the kernel needs to order it's input ports in order of 
// channels, but kernel_port structure should allow to abstract variables and ports

//define port type
#define DMA_IN 0
#define FILTER_OUT 1
#define BORDER_OUT 2

#define FILTER_IN 3
#define KERNEL_OUT 4

#define BORDER_IN 5
#define KERNEL_IN 6
#define DEMUX_OUT 7

struct soag_channel_port{
  int port_type;
  int is_connected;
  int ID;
  struct soag_filter *filter; //filter owning the port
  struct soag_channel_port *next;
  struct soag_channel *channel; //channel owning the port
};

struct soag_channel_link
{
  struct soag_filter *from;
  struct soag_filter *to;
  int weight; // from this will depend the depth of the fifo linking the two filters
  struct soag_channel_link *next; 
};

//This struct will take in account links going from a channel's port to a kernel's port.
struct soag_channel_kernel_link
{
  struct soag_channel_port *from;
  struct soag_kernel_port *to; 
  struct soag_channel_kernel_link *next;
  //here the size is always 1 so we can omit the weight of the link
};

 struct soag_kernel_demux_link
 {
 	struct soag_kernel_port *from;
 	struct soag_demux_port *to;
 	struct soag_kernel_demux_link *next;
 };

struct soag_filter_demux_link
 {
  struct soag_channel_port *from;
  struct soag_demux_port *to;
  struct soag_filter_demux_link *next;
 };

struct soag_channel 
{
  int ID; // ID that identifies the channel, it's going to be channel_NUM where NUM is an int taken from 
            // the access relation's ID of the Array the channels refers to
  struct soag_filter *filters;
  struct soag_filter *attached_to_DMA; //filter that will be attached to the DMA
  struct soag_channel_port *ports;
  struct soag_channel_link *links;
  struct soag_SST *SST; //sst containing the channel
  struct soag_channel *next;
};

struct soag_kernel_port 
{
   int port_type;
   int ID;
  struct soag_kernel *kernel; // kernel owning the port
   struct sog_access *access; // READ access connected to this port
  struct soag_kernel_port *next;
};

struct soag_filter
{
	int ID; // ID that identifies the filter
	struct sog_access* access; // Access the filter is derived from
	struct soag_channel *channel; // channel containing the filter
  int array_id; // id of the array that the filter is filtering
  int *access_lexmin; //lexmin of the datadomain of the filter
                    // used for the ordering inside the channel
  int center; //true if the filter is the central access to the array
  struct soag_filter* next;
};

struct soag_kernel
{
  int ID; // ID that identifies the kernel
  char *body; // ID that identifies the kernel functions
  struct soag_kernel_port  *ports; // binds the ports to the variable
  struct sog_access *access; //sog_access of the kernel
  struct soag_SST *SST; //sst containing the kernel
  struct soag_kernel *next;
};

struct soag_demux_port
{
  int ID; // ID that identifies the port
  struct soag_demux *demux; // demux owning the port
  int port_type;
  char *condition;//condition under which the output
                    // coming from this port is
                  //outputted as well by the demux
  struct soag_demux_port *next;
};

struct soag_demux
{
  int ID;
  struct soag_demux_port *ports;
  struct soag_SST *SST;
  char* border_condition;
};


struct soag_SST_ports{
	char *ID; //ID that identifies the SST port
	struct soag_SST *SST; //SST owner of the port
	struct soag_SST_ports *next;
};

struct soag_SST
{
  char* ID; //ID that identifies the SST
  int num_ID;
  int stancil_array_id;
  int dim;// Number of dimentions of the stencil array
  int *size_dim;// Array containing the size of each dimension of the stencil array
  struct soag_channel *channels; //list of channels inside the SST
  struct soag_kernel *kernels; // list of kernels inside the SST
  struct soag_demux *demux; // list of demuxes indise the SST
  struct soag_channel_kernel_link *links_channel_kernel;
  struct soag_kernel_demux_link *links_kernel_demux;
  struct soag_filter_demux_link *links_channel_demux; //To bring the border to demux
  struct soag_SST_ports *ports;

};

#endif
