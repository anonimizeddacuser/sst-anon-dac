/*
 *   This file is part of PolyFPGA.
 *
 *   PolyFPGA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * 	 Copyright (C) 2015 Giulio Stramondo
 *
 */
 
#ifndef HLS_GENERATOR_H
#define HLS_GENERATOR_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "scoputils.h"
#include "streaming_graph.h"


void generate_hls(options *opt);

void create_hls_filter_from_soag_filter(struct soag_filter* filter);

void create_hls_filter(int sst_id, int last, int center ,int input_fifo_ID,int channel_ID, int attached_to_dma,int filter_ID, int ndim, int size_dim[ndim], char *filtercondition);

//void create_kernel(int kernel_ID, int ndim, int size_dim[ndim],char *body);

void create_kernel(int sst_ID, int kernel_ID, int ndim, int size_dim[ndim], char *body, char *loops);

void create_kernel_from_soag_kernel(struct soag_kernel *kernel);

void create_hls_demux(struct soag_demux* demux);

char *generate_kernel_exp(char *original_body);//here for testing only

#endif