#!/bin/bash
#The first parameters is the folder containing the hls sources
#the second parameter is the part code ( ie. xc7z010clg400-1 )
#the third parameter is the clock's period (ns)

source /opt/Xilinx/Vivado/2014.4/settings64.sh

export XILINXD_LICENSE_FILE=/home/exafpga/Documents/Xilinx.lic
export LM_LICENSE_FILE=""
mkdir IP
cd IP

for file in ../$1/*
do
	filename=$(basename "$file")
	top_funct=${filename%.*}
	echo 	"open_project $1_$top_funct
			set_top $top_funct
			add_files ../$1/$filename
			open_solution \"solution1\"
			set_part {$2}
			create_clock -period $3 -name default
			csynth_design
			export_design -format ip_catalog
			exit" >> $top_funct.tcl
done


for file in *.tcl
do
	vivado_hls -f $file &
done


wait