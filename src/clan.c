/*
 *   This file is part of PolyFPGA.
 *
 *   PolyFPGA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *   Copyright (C) 2015 Giulio Stramondo
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>


#include <clan/clan.h>
#include <osl/scop.h>

#include <osl/scop.h>
#include <osl/macros.h>
#include <osl/util.h>
#include <osl/extensions/dependence.h>
#include <osl/extensions/arrays.h>
#include <candl/macros.h>
#include <candl/dependence.h>
#include <candl/violation.h>
#include <candl/options.h>
#include <candl/scop.h>
#include <candl/util.h>
#include <candl/piplib.h>

#include "scoputils.h"
#include "streaming_graph.h"
#include "barvinok_interface.h"
#include "parser.h"
#include "evaluate_scop.h"
#include "error.h"
#include "hls_generator.h"
#include "ip_generator.h"
#include "bitstream_generator.h"



int main(int argc, char* argv[]) {
  options* opt = NULL;
  int err_state = 0;

  //PARSE MODULE

  opt = parse_args(argc, argv);

  //if printed usage return
  if (opt == NULL) return 0;

  if (opt->input == NULL) {
    print_usage();
    return 0;
  }

  err_state = parse_input(opt);
  if(err_state)
    return NO_SCOPS_FOUND;

  dump_context(opt->contexts);

  //EVALUATE SCOP MODULE
  err_state = test_scops(opt);
  if (err_state) {
    if (err_state == WRONG_PARSING_ARG_NUMBER) {
      printf("Wrong number of queuing arguments\n");
      print_usage();
      return 0;
    }
  }

  //STREAMING_GRAPH
  generate_sog_data(opt);


  generate_soag_data(opt);

  generate_hls(opt);



  print_error_messages(opt);

  dump_error_messages(opt, "errors");

  if ( opt->step == 0 ) {
    printf("The code analysis is completed\ndo you want to procede with the IP generation? (y/n) \n");

    int c;
    /* use system call to make terminal send all keystrokes directly to stdin */
    system ("/bin/stty raw");
    if ((c = getchar()) != 'y') {
      system ("/bin/stty cooked");
      printf("\n");
      return 0;
    }
    /* use system call to set terminal behaviour to more normal behaviour */
    system ("/bin/stty cooked");
  }

  printf("\n");
  printf("Launching HLS...\n");
  generate_IPs(opt, opt->board);
  printf("HLS done.\n");

  printf("Packaging SST...\n");
  package_SST_IP(opt);
  printf("SST packed...\n");

  printf("Creating Bitstream...\n");
  err_state = generate_Bitstream(opt);

  if (err_state) {
    printf("Bitstream creation failed\n");
    return 1;
  }
  printf("Bitstream created\n");



  print_error_messages(opt);

  dump_error_messages(opt, "errors");
  return 0;
}
