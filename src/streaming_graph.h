/*
 *   This file is part of PolyFPGA.
 *
 *   PolyFPGA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * 	 Copyright (C) 2015 Giulio Stramondo
 *
 */
 
#ifndef STREAMING_GRAPH_H
#define STREAMING_GRAPH_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <clan/clan.h>
#include <osl/scop.h>

#include <osl/scop.h>
#include <osl/macros.h>
#include <osl/util.h>
#include <osl/extensions/dependence.h>
#include <osl/extensions/arrays.h>
#include <candl/macros.h>
#include <candl/dependence.h>
#include <candl/violation.h>
#include <candl/options.h>
#include <candl/scop.h>
#include <candl/util.h>
#include <candl/piplib.h>

#include "struct.h"
#include "barvinok_interface.h"
#include "error.h"

#ifdef THIS_MOD
#undef THIS_MOD
#endif
#define THIS_MOD SOG_MOD

//definition used to set the type of the access
#define READ 0
#define WRITE 1

//TODO
// Merge read access nodes,with same array's references,
// that belong to the same loop nest





void generate_sog_data(options *opt);

sog_statement* create_sog_statement();

sog_statement* allocate_sog_statements(osl_scop_p scop);

/*DDG Edge Removal Conditions.
Let us consider a DDG G = (n, e), whit only RAW dependencies,
in which each node n is marked with a growing number given by the execution order of each statement,
that, by the way, in test_get_cell_from_osl_relation (osl_scop_p scophe case of an rSCoP corresponds also to the syntactic order.
For the process of the streaming-oriented graph construction, and edge e must be removed if it represents a dependence
 carried along the time dimension (depth 1), and:
• e is a self-loop or
• e isdirectedfromni tonj andi>j.*/

osl_dependence_p edge_removal(osl_dependence_p dep );

char** get_access_str_from_statement(char * statement);

void dump_sog_statement(sog_statement *try_stmt);

void generate_dot_from_sog_statement(FILE* output, sog_statement *try_stmt, osl_dependence_p dep);

sog_graph * generate_sog_graph_from_sog_statement(sog_statement *try_stmt, osl_dependence_p dep);

void generate_dot_from_sog_graph(FILE* output, sog_graph* graph);

sog_access *get_sog_access(sog_statement *try_stmt, int stmt_ref, int acc_ref);

sog_graph_edges_list *get_edges_arriving_to_sog_access(sog_graph *graph, sog_access* to);

access_lst *get_read_access_from_sog_graph(sog_graph *graph);

sog_graph_edges_list *get_edges_starting_from_sog_access(sog_graph *graph, sog_access* from);

void dump_sog_graph_edges_list(FILE* output, sog_graph_edges_list* edge_list);

access_lst *get_write_access(sog_statement *try_stmt);

access_lst *get_write_access_from_sog_graph(sog_graph *graph);

void dump_sog_access_lst(FILE* output, access_lst * acc_lst );

int count_access_lst(access_lst * acc_lst);

int count_sog_graph_edges_list(sog_graph_edges_list* edges_lst);

int access_lst_contain_sog_access(access_lst *lst, sog_access* iscontained);

sog_graph_edges_list* get_copy_edges(sog_graph* graph);

int remove_copy_edge(sog_graph* graph, sog_graph_edges_list* edges_lst);

char *wrap_in_parenthesis(char *original);

char *str_replace(char *orig, char *rep, char *with);

char *select_assignment_expression(char *body);

void generate_sog_data_no_dump(options *opt);

#endif
