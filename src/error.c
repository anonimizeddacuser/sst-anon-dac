/*
 *   This file is part of PolyFPGA.
 *
 *   PolyFPGA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * 	 Copyright (C) 2015 Giulio Stramondo
 *
 */
 
#include "error.h"

char* get_error(int err_code) {
	switch (err_code) {
	case NO_ERROR:
		return "no error";

	case PARAMETERS:
		return "The scop seems to have parameters, currently the parameters are not handeled.";

	case INCOHERENT_SUB_FUNC:
		return "Two nodes should be merged, but their subscript function is incoherent.";

	case INCOHERENT_DOMAIN_ARRAY:
		return "The dimentions of the loop should be equal to the dimentions of the stancil array plus one.";

	case FILE_NOT_FOUND_VIVADO_TCL:
		return "Can't proceed with the packing of the SST because the vivado tcl is missing.";

	case TOO_LARGE_FIFO:
		return "Required a FIFO too large ( max size 32768 )";

	default:
		return "Unknown Error";
	}
}

char* get_module(int mod) {
	switch (mod) {
	case PARSER_MOD:
		return "(Parser Module)";

	case EVALUATE_SCOP_MOD:
		return "(Evaluate Scop Module)";

	case SOG_MOD:
		return "(SOG Module)";

	case SOAG_MOD:
		return "(SOAG Module)";

	case IP_GEN_MOD:
		return "(IP Generator Module)";

	case BIT_GEN_MOD:
		return "(Bitstream Generator Module)";
		
	default:
		return "";
	}
}

char* get_error_message(int err_code, int mod) {
	char *err_msg;
	char *module;

	err_msg = get_error(err_code);
	module = get_module(mod);

	char *message = (char *) malloc(sizeof(char) * strlen(err_msg) + sizeof(char) * strlen(module) + sizeof(char) * 3);
	message[0] = '\0';
	strcat(message, module);
	strcat(message, ": ");
	strcat(message, err_msg);
	return message;
}

int encountered_error(scop_context *scop_cntx) {
	if (scop_cntx->err_id == NO_ERROR)
		return 0;
	else
		return 1;
}

void set_error(scop_context *scop_cntx, int err_code, int mod) {
	if (!encountered_error(scop_cntx)) {
		scop_cntx->err_id = err_code;
		scop_cntx->module_id = mod;
	}
}

void print_error_messages(options* opt) {
	scop_context *tmp_context = NULL;
	tmp_context = opt->contexts;


	while (tmp_context != NULL) {
		if (tmp_context->err_id) {
			printf("Error Scop %d\n", tmp_context->id);
			printf("%s\n", get_error_message(tmp_context->err_id, tmp_context->module_id));
		}
		tmp_context = tmp_context->next;
	}
	return;
}


void dump_error_messages(options* opt, char *filename){
	scop_context *tmp_context = NULL;
	tmp_context = opt->contexts;

	while (tmp_context != NULL) {
		chdir(opt->root_directory);
		chdir(tmp_context->path);

		FILE *out = fopen(filename, "w");
		if (tmp_context->err_id) {
			fprintf(out, "Error Scop %d\n", tmp_context->id);
			fprintf(out, "%s\n", get_error_message(tmp_context->err_id, tmp_context->module_id));
		}
		else {
			fprintf(out, "Scop %d OK\n", tmp_context->id);
		}

		tmp_context = tmp_context->next;
	}
	chdir(opt->root_directory);
	return;
}