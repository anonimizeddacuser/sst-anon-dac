/*
 *   This file is part of PolyFPGA.
 *
 *   PolyFPGA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * 	 Copyright (C) 2015 Giulio Stramondo
 *
 */
 
#ifndef BARVINOK_INTERFACE_H
#define BARVINOK_INTERFACE_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <clan/clan.h>
#include <osl/scop.h>

#include <osl/scop.h>
#include <osl/macros.h>
#include <osl/util.h>
#include <osl/extensions/dependence.h>
#include <osl/extensions/arrays.h>
#include <candl/macros.h>
#include <candl/dependence.h>
#include <candl/violation.h>
#include <candl/options.h>
#include <candl/scop.h>
#include <candl/util.h>
#include <candl/piplib.h>

#include "struct.h"
#include "streaming_graph.h" //needed for the test

#define ISCC "/opt/polyhedral_tools/barvinok-install/bin/iscc" 

char* query_barvinok(char* query);

char* query_barvinok_full(char* query);

char* get_access_map_barvinok(osl_relation_p domain, osl_relation_p access);

char* get_access_barvinok(osl_relation_p access);

char* get_domain_barvinok(osl_relation_p D1);

char* map_to_project_away_time_dim(osl_relation_p D1);

//Queries
int same_data_domain(osl_relation_p D1, osl_relation_p A1,osl_relation_p D2, osl_relation_p A2);

char* get_filter_equation(osl_relation_p D1, osl_relation_p A1);

//Tests
char* test_get_filter_equation(options* opt);

char* get_loops_from_domain(osl_relation_p D1);
#endif