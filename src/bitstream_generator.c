/*
 *   This file is part of PolyFPGA.
 *
 *   PolyFPGA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * 	 Copyright (C) 2015 Giulio Stramondo
 *
 */
#include "bitstream_generator.h"

char *get_base_design_path(int BOARD_ID) {
	switch (BOARD_ID) {
	case ZYBO: return "/opt/clantool/base_prj/ZYNQ_Polytool_base";
	default: return "";
	}
}

char *get_project_path(options* opt) {
	char *buffer = (char*)malloc(500 * sizeof(char));
	buffer[0] = '\0';
	strcat(buffer, opt->root_directory);
	strcat(buffer, "/complete_hw");
	return buffer;
}

char *get_project_creation_command(options* opt) {
	char *buffer = (char*)malloc(500 * sizeof(char));
	buffer[0] = '\0';
	int BOARD_ID = opt->board;
	switch (BOARD_ID) {
	case ZYBO:
		strcat(buffer, "mkdir -p ");
		strcat(buffer, get_project_path(opt));
		strcat(buffer, " && cp -r /opt/clantool/base_prj/ZYNQ_Polytool_base/project_1/* ");
		strcat(buffer, get_project_path(opt));
		return buffer;
	case VIRTEX7:
		strcat(buffer, "mkdir -p ");
		strcat(buffer, get_project_path(opt));
		strcat(buffer, " && cp -r /opt/clantool/base_prj/V7_Polytool_base/project_1/* ");
		strcat(buffer, get_project_path(opt));
		return buffer;

	default: return "";
	}
}
void create_VIRTEX7_Bitstream_tcl(options* opt) {
	FILE *fp;
	chdir(opt->root_directory);
	fp = fopen ("vivado_bitstream.tcl", "w");
	fprintf(fp, "open_project %s/project_1.xpr\n", get_project_path(opt));
	fprintf(fp, "open_bd_design {%s/project_1.srcs/sources_1/bd/design_1/design_1.bd}\n", get_project_path(opt));

	fprintf(fp, "set_property ip_repo_paths  {");
	struct scop_context *tmp_context = opt->contexts;
	struct queuing* queuing = opt -> queuing;
	int register_slice_ID = 0;
	int queuing_index = 1;


	while ( tmp_context != NULL) {
		if (encountered_error(tmp_context)) {
			tmp_context = tmp_context->next;
			continue;
		}
		fprintf(fp, "%s/%s ", opt->root_directory, tmp_context->path);
		tmp_context = tmp_context->next;
	}

	fprintf(fp, "} [current_project]\n");
	fprintf(fp, "update_ip_catalog\n");
	tmp_context = opt->contexts;

	fprintf(fp, "startgroup\n");

	fprintf(fp, "create_bd_cell -type ip -vlnv Blablabla:user:SST_0_design_wrapper:1.0 SST_0_design_wrapper_0\n");

	fprintf(fp, "endgroup\n");

	fprintf(fp, "connect_bd_intf_net [get_bd_intf_pins axi_dma_0/M_AXIS_MM2S] [get_bd_intf_pins SST_0_design_wrapper_0/s_axis_dma_%d_in]\n", tmp_context->SST->stancil_array_id);

	fprintf(fp, "connect_bd_net -net [get_bd_nets microblaze_0_Clk] [get_bd_pins SST_0_design_wrapper_0/ap_clk] [get_bd_pins mig_7series_0/ui_clk]\n");

	fprintf(fp, "connect_bd_net -net [get_bd_nets rst_mig_7series_0_200M_peripheral_aresetn] [get_bd_pins SST_0_design_wrapper_0/ap_rst_n] [get_bd_pins rst_mig_7series_0_200M/peripheral_aresetn]\n");

	struct soag_demux_port *ports_it = tmp_context->SST->demux->ports;
	while (ports_it != NULL) {
		if (ports_it->port_type == DEMUX_OUT)
			break;
		ports_it = ports_it->next;
	}


	if (queuing != NULL && queuing->args[tmp_context->id] != 1) {
		for (queuing_index = 1; queuing_index < queuing->args[tmp_context->id];  queuing_index++) {
			fprintf(fp, "create_bd_cell -type ip -vlnv xilinx.com:ip:axis_register_slice:1.1 axis_register_slice_%d\n", register_slice_ID);
			fprintf(fp, "set_property -dict [list CONFIG.TDATA_NUM_BYTES.VALUE_SRC USER CONFIG.HAS_TLAST.VALUE_SRC USER] [get_bd_cells axis_register_slice_%d]\n", register_slice_ID);
			fprintf(fp, "set_property -dict [list CONFIG.TDATA_NUM_BYTES {4} CONFIG.HAS_TLAST {1}] [get_bd_cells axis_register_slice_%d]\n", register_slice_ID);
			fprintf(fp, "connect_bd_net -net [get_bd_nets microblaze_0_Clk] [get_bd_pins axis_register_slice_%d/aclk] [get_bd_pins mig_7series_0/ui_clk]\nconnect_bd_net -net [get_bd_nets rst_mig_7series_0_200M_peripheral_aresetn] [get_bd_pins axis_register_slice_%d/aresetn] [get_bd_pins rst_mig_7series_0_200M/peripheral_aresetn]\n", register_slice_ID, register_slice_ID);
			fprintf(fp, "connect_bd_intf_net [get_bd_intf_pins SST_%d_design_wrapper_%d/m_axis_port_%d] [get_bd_intf_pins axis_register_slice_%d/S_AXIS]\n", tmp_context->id, queuing_index - 1, ports_it->ID , register_slice_ID);

			fprintf(fp, "create_bd_cell -type ip -vlnv Blablabla:user:SST_%d_design_wrapper:1.0 SST_%d_design_wrapper_%d\n", tmp_context->id, tmp_context->id, queuing_index);

			fprintf(fp, "connect_bd_intf_net [get_bd_intf_pins axis_register_slice_%d/M_AXIS] [get_bd_intf_pins SST_%d_design_wrapper_%d/s_axis_dma_%d_in]\n", register_slice_ID, tmp_context->id, queuing_index, tmp_context->SST->stancil_array_id);

			fprintf(fp, "connect_bd_net -net [get_bd_nets microblaze_0_Clk] [get_bd_pins SST_%d_design_wrapper_%d/ap_clk] [get_bd_pins mig_7series_0/ui_clk]\n", tmp_context->id, queuing_index);

			fprintf(fp, "connect_bd_net -net [get_bd_nets rst_mig_7series_0_200M_peripheral_aresetn] [get_bd_pins SST_%d_design_wrapper_%d/ap_rst_n] [get_bd_pins rst_mig_7series_0_200M/peripheral_aresetn]\n", tmp_context->id, queuing_index);
			register_slice_ID++;
		}
		fprintf(fp, "connect_bd_intf_net [get_bd_intf_pins SST_%d_design_wrapper_%d/m_axis_port_%d] [get_bd_intf_pins axi_dma_0/S_AXIS_S2MM]\n", tmp_context->id, queuing_index - 1, ports_it->ID);

	} else {
		fprintf(fp, "connect_bd_intf_net [get_bd_intf_pins SST_0_design_wrapper_0/m_axis_port_%d] [get_bd_intf_pins axi_dma_0/S_AXIS_S2MM]\n", ports_it->ID);
	}


	tmp_context = tmp_context->next;
	int flag = 0;
	while ( tmp_context != NULL) {
		if (encountered_error(tmp_context)) {
			tmp_context = tmp_context->next;
			continue;
		}

		flag = 1;
		fprintf(fp, "startgroup\n");
		fprintf(fp, "create_bd_cell -type ip -vlnv xilinx.com:ip:axi_dma:7.1 axi_dma_%d\n", tmp_context->id);

		fprintf(fp, "set_property -dict [list CONFIG.c_include_sg {0} CONFIG.c_sg_length_width {23} CONFIG.c_sg_include_stscntrl_strm {0}] [get_bd_cells axi_dma_%d]\n", tmp_context->id);


		fprintf(fp, "apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config {Master \"/microblaze_0 (Periph)\" Clk \"Auto\" }  [get_bd_intf_pins axi_dma_%d/S_AXI_LITE]\n", tmp_context->id);

		fprintf(fp, "apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config {Slave \"/mig_7series_0/S_AXI\" Clk \"Auto\" }  [get_bd_intf_pins axi_dma_%d/M_AXI_MM2S]\n", tmp_context->id);

		fprintf(fp, "apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config {Slave \"/mig_7series_0/S_AXI\" Clk \"Auto\" }  [get_bd_intf_pins axi_dma_%d/M_AXI_S2MM]\n", tmp_context->id);

		fprintf(fp, "endgroup\n");

		fprintf(fp, "startgroup\n");
		fprintf(fp, "create_bd_cell -type ip -vlnv Blablabla:user:SST_%d_design_wrapper:1.0 SST_%d_design_wrapper_0\n", tmp_context->id, tmp_context->id);
		fprintf(fp, "endgroup\n");
		fprintf(fp, "connect_bd_intf_net [get_bd_intf_pins axi_dma_%d/M_AXIS_MM2S] [get_bd_intf_pins SST_%d_design_wrapper_0/s_axis_dma_%d_in]\n", tmp_context->id, tmp_context->id, tmp_context->SST->stancil_array_id);

		fprintf(fp, "connect_bd_net -net [get_bd_nets microblaze_0_Clk] [get_bd_pins SST_%d_design_wrapper_0/ap_clk] [get_bd_pins mig_7series_0/ui_clk]\n", tmp_context->id);


		fprintf(fp, "connect_bd_net -net [get_bd_nets rst_mig_7series_0_200M_peripheral_aresetn] [get_bd_pins SST_%d_design_wrapper_0/ap_rst_n] [get_bd_pins rst_mig_7series_0_200M/peripheral_aresetn]\n", tmp_context->id);

		ports_it = tmp_context->SST->demux->ports;
		while (ports_it != NULL) {
			if (ports_it->port_type == DEMUX_OUT)
				break;
			ports_it = ports_it->next;
		}
		if (queuing != NULL && queuing->args[tmp_context->id] != 1) {
			for (queuing_index = 1; queuing_index < queuing->args[tmp_context->id];  queuing_index++) {
				fprintf(fp, "create_bd_cell -type ip -vlnv xilinx.com:ip:axis_register_slice:1.1 axis_register_slice_%d\n", register_slice_ID);
				fprintf(fp, "set_property -dict [list CONFIG.TDATA_NUM_BYTES.VALUE_SRC USER CONFIG.HAS_TLAST.VALUE_SRC USER] [get_bd_cells axis_register_slice_%d]\n", register_slice_ID);
				fprintf(fp, "set_property -dict [list CONFIG.TDATA_NUM_BYTES {4} CONFIG.HAS_TLAST {1}] [get_bd_cells axis_register_slice_%d]\n", register_slice_ID);
				fprintf(fp, "connect_bd_net -net [get_bd_nets microblaze_0_Clk] [get_bd_pins axis_register_slice_%d/aclk] [get_bd_pins mig_7series_0/ui_clk]\nconnect_bd_net -net [get_bd_nets rst_mig_7series_0_200M_peripheral_aresetn] [get_bd_pins axis_register_slice_%d/aresetn] [get_bd_pins rst_mig_7series_0_200M/peripheral_aresetn]\n", register_slice_ID, register_slice_ID);
				fprintf(fp, "connect_bd_intf_net [get_bd_intf_pins SST_%d_design_wrapper_%d/m_axis_port_%d] [get_bd_intf_pins axis_register_slice_%d/S_AXIS]\n", tmp_context->id, queuing_index - 1, ports_it->ID , register_slice_ID);

				fprintf(fp, "create_bd_cell -type ip -vlnv Blablabla:user:SST_%d_design_wrapper:1.0 SST_%d_design_wrapper_%d\n", tmp_context->id, tmp_context->id, queuing_index);

				fprintf(fp, "connect_bd_intf_net [get_bd_intf_pins axis_register_slice_%d/M_AXIS] [get_bd_intf_pins SST_%d_design_wrapper_%d/s_axis_dma_%d_in]\n", register_slice_ID, tmp_context->id, queuing_index, tmp_context->SST->stancil_array_id);

				fprintf(fp, "connect_bd_net -net [get_bd_nets microblaze_0_Clk] [get_bd_pins SST_%d_design_wrapper_%d/ap_clk] [get_bd_pins mig_7series_0/ui_clk]\n", tmp_context->id, queuing_index);

				fprintf(fp, "connect_bd_net -net [get_bd_nets rst_mig_7series_0_200M_peripheral_aresetn] [get_bd_pins SST_%d_design_wrapper_%d/ap_rst_n] [get_bd_pins rst_mig_7series_0_200M/peripheral_aresetn]\n", tmp_context->id, queuing_index);
				register_slice_ID++;
			}
			fprintf(fp, "connect_bd_intf_net [get_bd_intf_pins SST_%d_design_wrapper_%d/m_axis_port_%d] [get_bd_intf_pins axi_dma_%d/S_AXIS_S2MM]\n", tmp_context->id, queuing_index - 1, ports_it->ID, tmp_context->id);

		} else {
			fprintf(fp, "connect_bd_intf_net [get_bd_intf_pins SST_%d_design_wrapper_0/m_axis_port_%d] [get_bd_intf_pins axi_dma_%d/S_AXIS_S2MM]\n", tmp_context->id, ports_it->ID, tmp_context->id);

		}

		tmp_context = tmp_context ->next;
	}
	fprintf(fp, "save_bd_design\n");
	fprintf(fp, "reset_run synth_1\n");
	fprintf(fp, "launch_runs impl_1 -to_step write_bitstream\n");
	fprintf(fp, "wait_on_run impl_1\n");
	fprintf(fp, "wait_on_run synth_1\n");
	//fprintf(fp, "open_run impl_1\n");
	fprintf(fp, "if {[file exists %s/project_1.runs/impl_1/design_1_wrapper.sysdef]} {\n", get_project_path(opt));
	fprintf(fp, "file mkdir %s/project_1.sdk\n", get_project_path(opt));
	fprintf(fp, "file copy -force %s/project_1.runs/impl_1/design_1_wrapper.sysdef %s/project_1.sdk/design_1_wrapper.hdf\n", get_project_path(opt), get_project_path(opt));
	fprintf(fp, "} else {\nputs \"Error during bitstream synthesis\"\n}\n");
	fprintf(fp, "exit\n");//print exit here

	fclose(fp);
	return;
}

void create_ZYBO_Bitstream_tcl(options * opt) {
	FILE *fp;
	chdir(opt->root_directory);
	fp = fopen ("vivado_bitstream.tcl", "w");
	fprintf(fp, "open_project %s/project_1.xpr\n", get_project_path(opt));
	fprintf(fp, "open_bd_design {%s/project_1.srcs/sources_1/bd/design_1/design_1.bd}\n", get_project_path(opt));

	fprintf(fp, "set_property ip_repo_paths  {");
	struct scop_context *tmp_context = opt->contexts;
	struct queuing* queuing = opt -> queuing;
	int register_slice_ID = 0;
	int queuing_index = 1;

	while ( tmp_context != NULL) {
		if (encountered_error(tmp_context)) {
			tmp_context = tmp_context->next;
			continue;
		}
		fprintf(fp, "%s/%s ", opt->root_directory, tmp_context->path);
		tmp_context = tmp_context->next;
	}

	fprintf(fp, "} [current_project]\n");
	fprintf(fp, "update_ip_catalog\n");
	tmp_context = opt->contexts;

	fprintf(fp, "startgroup\n");

	fprintf(fp, "create_bd_cell -type ip -vlnv Blablabla:user:SST_0_design_wrapper:1.0 SST_0_design_wrapper_0\n");

	fprintf(fp, "endgroup\n");

	fprintf(fp, "connect_bd_intf_net [get_bd_intf_pins axi_dma_0/M_AXIS_MM2S] [get_bd_intf_pins SST_0_design_wrapper_0/s_axis_dma_%d_in]\n", tmp_context->SST->stancil_array_id);

	fprintf(fp, "connect_bd_net -net [get_bd_nets processing_system7_0_FCLK_CLK0] [get_bd_pins SST_0_design_wrapper_0/ap_clk] [get_bd_pins processing_system7_0/FCLK_CLK0]\n");
	fprintf(fp, "connect_bd_net -net [get_bd_nets rst_processing_system7_0_100M_peripheral_aresetn] [get_bd_pins SST_0_design_wrapper_0/ap_rst_n] [get_bd_pins rst_processing_system7_0_100M/peripheral_aresetn]\n");

	struct soag_demux_port *ports_it = tmp_context->SST->demux->ports;
	while (ports_it != NULL) {
		if (ports_it->port_type == DEMUX_OUT)
			break;
		ports_it = ports_it->next;
	}

	if (queuing != NULL && queuing->args[tmp_context->id] != 1) {
		for (queuing_index = 1; queuing_index < queuing->args[tmp_context->id];  queuing_index++) {
			fprintf(fp, "create_bd_cell -type ip -vlnv xilinx.com:ip:axis_register_slice:1.1 axis_register_slice_%d\n", register_slice_ID);
			fprintf(fp, "set_property -dict [list CONFIG.TDATA_NUM_BYTES.VALUE_SRC USER CONFIG.HAS_TLAST.VALUE_SRC USER] [get_bd_cells axis_register_slice_%d]\n", register_slice_ID);
			fprintf(fp, "set_property -dict [list CONFIG.TDATA_NUM_BYTES {4} CONFIG.HAS_TLAST {1}] [get_bd_cells axis_register_slice_%d]\n", register_slice_ID);
			fprintf(fp, "connect_bd_net -net [get_bd_nets processing_system7_0_FCLK_CLK0] [get_bd_pins axis_register_slice_%d/aclk] [get_bd_pins processing_system7_0/FCLK_CLK0]\nconnect_bd_net -net [get_bd_nets rst_processing_system7_0_100M_peripheral_aresetn] [get_bd_pins axis_register_slice_%d/aresetn] [get_bd_pins rst_processing_system7_0_100M/peripheral_aresetn]\n", register_slice_ID, register_slice_ID);
			fprintf(fp, "connect_bd_intf_net [get_bd_intf_pins SST_%d_design_wrapper_%d/m_axis_port_%d] [get_bd_intf_pins axis_register_slice_%d/S_AXIS]\n", tmp_context->id, queuing_index - 1, ports_it->ID , register_slice_ID);

			fprintf(fp, "create_bd_cell -type ip -vlnv Blablabla:user:SST_%d_design_wrapper:1.0 SST_%d_design_wrapper_%d\n", tmp_context->id, tmp_context->id, queuing_index);

			fprintf(fp, "connect_bd_intf_net [get_bd_intf_pins axis_register_slice_%d/M_AXIS] [get_bd_intf_pins SST_%d_design_wrapper_%d/s_axis_dma_%d_in]\n", register_slice_ID, tmp_context->id, queuing_index, tmp_context->SST->stancil_array_id);

			fprintf(fp, "connect_bd_net -net [get_bd_nets processing_system7_0_FCLK_CLK0] [get_bd_pins SST_%d_design_wrapper_%d/ap_clk] [get_bd_pins processing_system7_0/FCLK_CLK0]\n", tmp_context->id, queuing_index);

			fprintf(fp, "connect_bd_net -net [get_bd_nets rst_processing_system7_0_100M_peripheral_aresetn] [get_bd_pins SST_%d_design_wrapper_%d/ap_rst_n] [get_bd_pins rst_processing_system7_0_100M/peripheral_aresetn]\n", tmp_context->id, queuing_index);
			register_slice_ID++;
		}
		fprintf(fp, "connect_bd_intf_net [get_bd_intf_pins SST_%d_design_wrapper_%d/m_axis_port_%d] [get_bd_intf_pins axi_dma_0/S_AXIS_S2MM]\n", tmp_context->id, queuing_index - 1, ports_it->ID);

	} else {
		fprintf(fp, "connect_bd_intf_net [get_bd_intf_pins SST_0_design_wrapper_0/m_axis_port_%d] [get_bd_intf_pins axi_dma_0/S_AXIS_S2MM]\n", ports_it->ID);
	}

	tmp_context = tmp_context->next;
	int flag = 0;
	while ( tmp_context != NULL) {
		if (encountered_error(tmp_context)) {
			tmp_context = tmp_context->next;
			continue;
		}
		flag = 1;
		fprintf(fp, "startgroup\n");
		fprintf(fp, "create_bd_cell -type ip -vlnv xilinx.com:ip:axi_dma:7.1 axi_dma_%d\n", tmp_context->id);
		fprintf(fp, "apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config {Master \"/processing_system7_0/M_AXI_GP0\" Clk \"Auto\" }  [get_bd_intf_pins axi_dma_%d/S_AXI_LITE]\n", tmp_context->id);
		fprintf(fp, "apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config {Slave \"/processing_system7_0/S_AXI_HP0\" Clk \"Auto\" }  [get_bd_intf_pins axi_dma_%d/M_AXI_SG]\n", tmp_context->id);
		fprintf(fp, "apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config {Slave \"/processing_system7_0/S_AXI_HP0\" Clk \"Auto\" }  [get_bd_intf_pins axi_dma_%d/M_AXI_MM2S]\n", tmp_context->id);
		fprintf(fp, "apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config {Slave \"/processing_system7_0/S_AXI_HP0\" Clk \"Auto\" }  [get_bd_intf_pins axi_dma_%d/M_AXI_S2MM]\n", tmp_context->id);

		fprintf(fp, "set_property -dict [list CONFIG.c_include_sg {0} CONFIG.c_sg_length_width {23} CONFIG.c_sg_include_stscntrl_strm {0}] [get_bd_cells axi_dma_%d]\n", tmp_context->id);
		fprintf(fp, "endgroup\n");

		fprintf(fp, "startgroup\n");
		fprintf(fp, "create_bd_cell -type ip -vlnv Blablabla:user:SST_%d_design_wrapper:1.0 SST_%d_design_wrapper_0\n", tmp_context->id, tmp_context->id);
		fprintf(fp, "endgroup\n");
		fprintf(fp, "connect_bd_intf_net [get_bd_intf_pins axi_dma_%d/M_AXIS_MM2S] [get_bd_intf_pins SST_%d_design_wrapper_0/s_axis_dma_%d_in]\n", tmp_context->id, tmp_context->id, tmp_context->SST->stancil_array_id);
		fprintf(fp, "connect_bd_net -net [get_bd_nets processing_system7_0_FCLK_CLK0] [get_bd_pins SST_%d_design_wrapper_0/ap_clk] [get_bd_pins processing_system7_0/FCLK_CLK0]\n", tmp_context->id);
		fprintf(fp, "connect_bd_net -net [get_bd_nets rst_processing_system7_0_100M_peripheral_aresetn] [get_bd_pins SST_%d_design_wrapper_0/ap_rst_n] [get_bd_pins rst_processing_system7_0_100M/peripheral_aresetn]\n", tmp_context->id);
		ports_it = tmp_context->SST->demux->ports;
		while (ports_it != NULL) {
			if (ports_it->port_type == DEMUX_OUT)
				break;
			ports_it = ports_it->next;
		}

		if (queuing != NULL && queuing->args[tmp_context->id] != 1) {
			for (queuing_index = 1; queuing_index < queuing->args[tmp_context->id];  queuing_index++) {
				fprintf(fp, "create_bd_cell -type ip -vlnv xilinx.com:ip:axis_register_slice:1.1 axis_register_slice_%d\n", register_slice_ID);
				fprintf(fp, "set_property -dict [list CONFIG.TDATA_NUM_BYTES.VALUE_SRC USER CONFIG.HAS_TLAST.VALUE_SRC USER] [get_bd_cells axis_register_slice_%d]\n", register_slice_ID);
				fprintf(fp, "set_property -dict [list CONFIG.TDATA_NUM_BYTES {4} CONFIG.HAS_TLAST {1}] [get_bd_cells axis_register_slice_%d]\n", register_slice_ID);
				fprintf(fp, "connect_bd_net -net [get_bd_nets processing_system7_0_FCLK_CLK0] [get_bd_pins axis_register_slice_%d/aclk] [get_bd_pins processing_system7_0/FCLK_CLK0]\nconnect_bd_net -net [get_bd_nets rst_processing_system7_0_100M_peripheral_aresetn] [get_bd_pins axis_register_slice_%d/aresetn] [get_bd_pins rst_processing_system7_0_100M/peripheral_aresetn]\n", register_slice_ID, register_slice_ID);
				fprintf(fp, "connect_bd_intf_net [get_bd_intf_pins SST_%d_design_wrapper_%d/m_axis_port_%d] [get_bd_intf_pins axis_register_slice_%d/S_AXIS]\n", tmp_context->id, queuing_index - 1, ports_it->ID , register_slice_ID);

				fprintf(fp, "create_bd_cell -type ip -vlnv Blablabla:user:SST_%d_design_wrapper:1.0 SST_%d_design_wrapper_%d\n", tmp_context->id, tmp_context->id, queuing_index);

				fprintf(fp, "connect_bd_intf_net [get_bd_intf_pins axis_register_slice_%d/M_AXIS] [get_bd_intf_pins SST_%d_design_wrapper_%d/s_axis_dma_%d_in]\n", register_slice_ID, tmp_context->id, queuing_index, tmp_context->SST->stancil_array_id);

				fprintf(fp, "connect_bd_net -net [get_bd_nets processing_system7_0_FCLK_CLK0] [get_bd_pins SST_%d_design_wrapper_%d/ap_clk] [get_bd_pins processing_system7_0/FCLK_CLK0]\n", tmp_context->id, queuing_index);

				fprintf(fp, "connect_bd_net -net [get_bd_nets rst_processing_system7_0_100M_peripheral_aresetn] [get_bd_pins SST_%d_design_wrapper_%d/ap_rst_n] [get_bd_pins rst_processing_system7_0_100M/peripheral_aresetn]\n", tmp_context->id, queuing_index);
				register_slice_ID++;
			}
			fprintf(fp, "connect_bd_intf_net [get_bd_intf_pins SST_%d_design_wrapper_%d/m_axis_port_%d] [get_bd_intf_pins axi_dma_%d/S_AXIS_S2MM]\n", tmp_context->id, queuing_index - 1, ports_it->ID, tmp_context->id);

		} else {
			fprintf(fp, "connect_bd_intf_net [get_bd_intf_pins SST_%d_design_wrapper_0/m_axis_port_%d] [get_bd_intf_pins axi_dma_%d/S_AXIS_S2MM]\n", tmp_context->id, ports_it->ID, tmp_context->id);

		}
		tmp_context = tmp_context ->next;
	}
	if (flag)
		fprintf(fp, "assign_bd_address [get_bd_addr_segs {processing_system7_0/S_AXI_HP0/HP0_DDR_LOWOCM }]\n");
	fprintf(fp, "save_bd_design\n");
	fprintf(fp, "reset_run synth_1\n");
	fprintf(fp, "launch_runs impl_1 -to_step write_bitstream\n");
	fprintf(fp, "wait_on_run impl_1\n");
	fprintf(fp, "wait_on_run synth_1\n");
	//fprintf(fp, "open_run impl_1\n");
	fprintf(fp, "if {[file exists %s/project_1.runs/impl_1/design_1_wrapper.sysdef]} {\n", get_project_path(opt));
	fprintf(fp, "file mkdir %s/project_1.sdk\n", get_project_path(opt));
	fprintf(fp, "file copy -force %s/project_1.runs/impl_1/design_1_wrapper.sysdef %s/project_1.sdk/design_1_wrapper.hdf\n", get_project_path(opt), get_project_path(opt));
	fprintf(fp, "} else {\nputs \"Error during bitstream synthesis\"\n}\n");
	fprintf(fp, "exit\n");//print exit here
	fclose(fp);
	return;
}


void create_sdk_proj_ZYBO(options * opt) {
	FILE *fp;
	fp = fopen("batch_sdk.tcl", "w");
	fprintf(fp, "#!/usr/bin/tclsh\n");
	fprintf(fp, "set_workspace %s/project_1.sdk\n", get_project_path(opt));
	fprintf(fp, "create_project -type hw -name hw_0 -hwspec %s/project_1.sdk/design_1_wrapper.hdf\n", get_project_path(opt));
	fprintf(fp, "create_project -type bsp -name bsp_0 -hwproject hw_0 -proc ps7_cortexa9_0 -os standalone\n");
	fprintf(fp, "create_project -type app -name \"helloworld1\" -hwproject hw_0 -proc ps7_cortexa9_0 -os standalone -lang C -app {Hello World} -bsp bsp_0\n");
	fprintf(fp, "build -type all\n");
	fprintf(fp, "exit\n");
	fclose(fp);
	system(". /opt/Xilinx/SDK/2014.4/settings64_polytool.sh; xsdk -batch -source batch_sdk.tcl > sdk_out 2> sdk_error");
	return;


}

void create_sdk_proj_V7(options * opt) {
	FILE *fp;
	fp = fopen("batch_sdk.tcl", "w");
	fprintf(fp, "#!/usr/bin/tclsh\n");
	fprintf(fp, "set_workspace %s/project_1.sdk\n", get_project_path(opt));
	fprintf(fp, "create_project -type hw -name hw_0 -hwspec %s/project_1.sdk/design_1_wrapper.hdf\n", get_project_path(opt));
	fprintf(fp, "create_project -type bsp -name bsp_0 -hwproject hw_0 -proc microblaze_0 -os standalone\n");
	fprintf(fp, "create_project -type app -name \"helloworld1\" -hwproject hw_0 -proc microblaze_0 -os standalone -lang C -app {Hello World} -bsp bsp_0\n");
	fprintf(fp, "build -type all\n");
	fprintf(fp, "exit\n");
	fclose(fp);
	system(". /opt/Xilinx/SDK/2014.4/settings64_polytool.sh; xsdk -batch -source batch_sdk.tcl > sdk_out 2> sdk_error");
	return;


}

void rebuild_sdk_proj(options * opt) {
	FILE *fp;
	fp = fopen("rebuild_batch_sdk.tcl", "w");
	fprintf(fp, "#!/usr/bin/tclsh\n");
	fprintf(fp, "set_workspace %s/project_1.sdk\n", get_project_path(opt));
	fprintf(fp, "build -type all\n");
	fprintf(fp, "exit\n");
	fclose(fp);
	system(". /opt/Xilinx/SDK/2014.4/settings64_polytool.sh; xsdk -batch -source rebuild_batch_sdk.tcl > rebuild_sdk_out 2> rebuild_sdk_error");
	remove("rebuild_batch_sdk.tcl");
	return;
}

void create_elf_c_source(options * opt) {
	FILE* fp;
	int i, j;
	struct soag_SST* tmp_sst;


	fp = fopen("helloworld.c", "w");
	fprintf(fp, "#include <stdio.h>\n#include <stdlib.h>\n");
	fprintf(fp, "#include \"platform.h\"\n#include \"xaxidma.h\"\n");
	if (opt->board == VIRTEX7) {
		fprintf(fp, "#include \"xtmrctr.h\"\n");
	}
	fprintf(fp, "int init_dma(XAxiDma *axiDmaPtr, int deviceId)\n");
	fprintf(fp, "{\n");
	fprintf(fp, "	XAxiDma_Config *CfgPtr;\n");
	fprintf(fp, "	XStatus status;\n");
	fprintf(fp, "	CfgPtr = XAxiDma_LookupConfig(deviceId);\n");
	fprintf(fp, "	if(!CfgPtr){\n");
	fprintf(fp, "		printf(\"Error looking for AXI DMA config\\n\\r\");\n");
	fprintf(fp, "		return XST_FAILURE;\n");
	fprintf(fp, "	}\n\n");
	fprintf(fp, "	status = XAxiDma_CfgInitialize(axiDmaPtr,CfgPtr);\n");
	fprintf(fp, "	if(status != XST_SUCCESS){\n");
	fprintf(fp, "		printf(\"Error initializing DMA. Error code: %%d\\n\\r\",status);\n");
	fprintf(fp, "		return XST_FAILURE;\n");
	fprintf(fp, "	}\n");
	fprintf(fp, "	XAxiDma_IntrDisable(axiDmaPtr, XAXIDMA_IRQ_ALL_MASK,XAXIDMA_DEVICE_TO_DMA);\n");
	fprintf(fp, "	XAxiDma_IntrDisable(axiDmaPtr, XAXIDMA_IRQ_ALL_MASK,XAXIDMA_DMA_TO_DEVICE);\n\n");
	fprintf(fp, "	return XST_SUCCESS;\n}\n\n");
	fprintf(fp, "#define D_TYPE float\n\n");
	fprintf(fp, "int main()\n{\n");
	fprintf(fp, "    init_platform();\n");
	fprintf(fp, "		srand(100);\n");


	if (opt->board == VIRTEX7) {
		fprintf(fp, "int beginTime; \n");
		fprintf(fp, "int endTime; \n");
		fprintf(fp, "int calibration; \n");
		fprintf(fp, "int timeRun; \n");
	}
	struct scop_context *tmp_context = opt->contexts;
	int ssts_max_dim = 0;
	while ( tmp_context != NULL) {
		if (encountered_error(tmp_context)) {
			tmp_context = tmp_context->next;
			continue;
		}
		fprintf(fp, "	XAxiDma AxiDma%d;\n", tmp_context->id);

		if (tmp_context->SST->dim > ssts_max_dim)
			ssts_max_dim = tmp_context->SST->dim;

		tmp_context = tmp_context->next;
	}
	fprintf(fp, "\n	XStatus Status;\n\n");
	fprintf(fp, "	Xil_DCacheDisable();\n");
	fprintf(fp, "	Xil_ICacheDisable();\n\n");
	fprintf(fp, "register int ");
	for (i = 0 ; i < ssts_max_dim; i++) {
		fprintf(fp, "i%d, ", i);
	}
	fprintf(fp, "index;\n");

	int p_is_declared = 0;

	tmp_context = opt->contexts;
	while ( tmp_context != NULL) {
		if (encountered_error(tmp_context)) {
			tmp_context = tmp_context->next;
			continue;
		}
		fprintf(fp, "	Status = init_dma(&AxiDma%d,XPAR_AXI_DMA_%d_DEVICE_ID);\n", tmp_context->id, tmp_context->id);
		tmp_context = tmp_context->next;
	}

	if (opt->board == VIRTEX7) {
		fprintf(fp, "XTmrCtr timer;\n");
		fprintf(fp, "XTmrCtr_Initialize(&timer, XPAR_TMRCTR_0_DEVICE_ID);\n");
		fprintf(fp, "XTmrCtr_SetResetValue(&timer, 0, 0x00000000);\n");
	}
	tmp_context = opt->contexts;
	while ( tmp_context != NULL) {
		if (encountered_error(tmp_context)) {
			tmp_context = tmp_context->next;
			continue;
		}
		tmp_sst = tmp_context->SST;
		if (!p_is_declared) {
			fprintf(fp, "	D_TYPE *p = (D_TYPE*)malloc(");
			p_is_declared = 1;
		}
		else {
			fprintf(fp, "	p = (D_TYPE*)malloc(");
		}

		for (i = 0 ; i < tmp_sst->dim; i++) {
			fprintf(fp, "%d*", tmp_sst->size_dim[i] + 1);
		}

		fprintf(fp, "sizeof(D_TYPE));\n");
		fprintf(fp, "	if(p == NULL){\n");
		fprintf(fp, "		printf(\"\\r\\n--- 'p' malloc failed! --- \\r\\n\");\n");

		fprintf(fp, "	}\n");


		for (i = (tmp_sst->dim) - 1; i >= 0; i--) {
			fprintf(fp, "	for (i%d = 0; i%d < %d; i%d++)\n", i, i, tmp_sst->size_dim[i] + 1, i);
		}
		fprintf(fp, "	{\n" );
		fprintf(fp, "			index = ");
		for (i = 0 ; i < tmp_sst->dim; i++) {
			fprintf(fp, "(i%d)", i);
			for (j = 0; j < i; j++)
				fprintf(fp, "*%d", tmp_sst->size_dim[j] + 1);
			if (i == (tmp_sst->dim) - 1)
				fprintf(fp, ";\n");
			else
				fprintf(fp, "+");
		}

		fprintf(fp, "			p[index] = rand() %% 100;\n");
		fprintf(fp, "		}\n\n");

		if (opt->board == VIRTEX7) {
			fprintf(fp, "XTmrCtr_Reset(&timer, 0);\n" );
			fprintf(fp, "XTmrCtr_Start(&timer, 0);\n" );
			fprintf(fp, "beginTime = XTmrCtr_GetValue(&timer, XPAR_TMRCTR_0_DEVICE_ID);\n" );
			fprintf(fp, "endTime = XTmrCtr_GetValue(&timer, XPAR_TMRCTR_0_DEVICE_ID);\n");
			fprintf(fp, "calibration = endTime - beginTime;\n");
		}

		fprintf(fp, "printf(\"\\r\\nstarting computation\\r\\n\");\n" );

		if (opt->board == VIRTEX7) {
			fprintf(fp, "beginTime = XTmrCtr_GetValue(&timer, XPAR_TMRCTR_0_DEVICE_ID);\n");
		}
		
		fprintf(fp, "		Status = XAxiDma_SimpleTransfer(&AxiDma%d,(u32) p, ", tmp_context->id);

		for (i = 0 ; i < tmp_sst->dim; i++) {
			fprintf(fp, "%d*", tmp_sst->size_dim[i] + 1);
		}

		fprintf(fp, "sizeof(D_TYPE), XAXIDMA_DMA_TO_DEVICE);\n");

		fprintf(fp, "		if (Status != XST_SUCCESS) {\n");
		fprintf(fp, "			return XST_FAILURE;\n");
		fprintf(fp, "		}\n");


		fprintf(fp, "		Status = XAxiDma_SimpleTransfer(&AxiDma%d,(u32) p, ", tmp_context->id);

		for (i = 0 ; i < tmp_sst->dim; i++) {
			fprintf(fp, "%d*", tmp_sst->size_dim[i] + 1);
		}

		fprintf(fp, "sizeof(D_TYPE), XAXIDMA_DEVICE_TO_DMA);\n");

		fprintf(fp, "		if (Status != XST_SUCCESS) {\n");
		fprintf(fp, "			return XST_FAILURE;\n");
		fprintf(fp, "		}\n");

		fprintf(fp, "		while ((XAxiDma_Busy(&AxiDma%d,XAXIDMA_DEVICE_TO_DMA))||(XAxiDma_Busy(&AxiDma%d,XAXIDMA_DMA_TO_DEVICE)));\n", tmp_context->id, tmp_context->id);

		if (opt->board == VIRTEX7) {
			fprintf(fp, "	endTime = XTmrCtr_GetValue(&timer, XPAR_TMRCTR_0_DEVICE_ID);\n" );
			fprintf(fp, "	timeRun = (endTime - beginTime - calibration);\n" );
			fprintf(fp, "	printf(\"\\r\\nFinished in %%d cc\\r\\n\", timeRun);\n");
			fprintf(fp, "	printf(\"\\r\\nTime: %%f s\\r\\n\", (float)(1.0*timeRun)/XPAR_TMRCTR_0_CLOCK_FREQ_HZ);\n" );

		}

		for (i = (tmp_sst->dim) - 1; i >= 0; i--) {
			fprintf(fp, "	for (i%d = 0; i%d < %d; i%d++)\n", i, i, tmp_sst->size_dim[i] + 1, i);
		}
		fprintf(fp, "	{\n" );
		fprintf(fp, "			index = ");
		for (i = 0 ; i < tmp_sst->dim; i++) {
			fprintf(fp, "(i%d)", i);
			for (j = 0; j < i; j++)
				fprintf(fp, "*%d", tmp_sst->size_dim[j] + 1);
			if (i == (tmp_sst->dim) - 1)
				fprintf(fp, ";\n");
			else
				fprintf(fp, "+");
		}
//				printf("PL:A[%d][%d][%d] = %f\n\r",i3,i2,i1,p[index]);
		fprintf(fp, "				printf(\"PL:A");
		for (i = 0 ; i < tmp_sst->dim; i++)
			fprintf(fp, "[%%d]");

		fprintf(fp, " = %%f\\n\\r\",");
		for (i = (tmp_sst->dim) - 1; i >= 0; i--)
			fprintf(fp, "i%d,", i);
		fprintf(fp, "p[index]);\n" );

		fprintf(fp, "		}\n\n");

		fprintf(fp, "printf(\"Finish test on SST_%d\\n\\r\");\n", tmp_context->id);
		fprintf(fp, "	free(p);\n");
		tmp_context = tmp_context->next;
	}

	fprintf(fp, "    return 0;\n}\n");
	fclose(fp);

	return;
}

int generate_Bitstream(options * opt) {
	char *path_base_design = NULL;

	system(get_project_creation_command(opt));

	switch (opt->board) {
	case ZYBO:
		create_ZYBO_Bitstream_tcl(opt);
		break;
	case VIRTEX7:
		create_VIRTEX7_Bitstream_tcl(opt);
		break;
	default:
		break;
	}

	system("/opt/clantool/src/generate_bitstream.sh > bitstream_SST_out 2> bitstream_error");

	if ( access( "complete_hw/project_1.runs/impl_1/design_1_wrapper.sysdef", F_OK ) == -1 ) {
		//TODO check vivado errors and report if the failure is due
		return 1;
	}



	switch (opt->board) {
	case ZYBO:
		create_sdk_proj_ZYBO(opt);
		break;
	case VIRTEX7:
		create_sdk_proj_V7(opt);
		break;
	default:
		break;
	}

	chdir(opt->root_directory);
	chdir("complete_hw/project_1.sdk/helloworld1/src");
	create_elf_c_source(opt);

	//Patch the linker script
	switch (opt->board) {
	case ZYBO:
		system("cp -f /opt/clantool/base_prj/ZYNQ_Polytool_base/lscript.ld .");
		chdir(opt->root_directory);
		system("cp /opt/clantool/base_prj/ZYNQ_Polytool_base/xmd_auto.tcl .");
		system("cp /opt/clantool/base_prj/ZYNQ_Polytool_base/zybo_run.sh . && chmod +x zybo_run.sh");
		break;
	case VIRTEX7:
		system("cp -f /opt/clantool/base_prj/V7_Polytool_base/lscript.ld .");
		break;
	default:
		break;
	}

	chdir(opt->root_directory);
	rebuild_sdk_proj(opt);
	return 0;
}
