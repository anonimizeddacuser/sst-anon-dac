/*
 *   This file is part of PolyFPGA.
 *
 *   PolyFPGA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * 	 Copyright (C) 2015 Giulio Stramondo
 *
 */
 
#ifndef ERROR_H
#define ERROR_H

#include "struct.h"
#include <stdio.h>
//Definitions of error codes

#define NO_ERROR 0
#define PARAMETERS 1
#define INCOHERENT_SUB_FUNC 2
#define INCOHERENT_DOMAIN_ARRAY 3
#define FILE_NOT_FOUND_VIVADO_TCL 4
#define TOO_LARGE_FIFO 5

//GLOBAL ERROR CODES
#define GENERIC_GLOBAL_ERROR 1
#define WRONG_PARSING_ARG_NUMBER 2
#define NO_SCOPS_FOUND 3
 
//Definiion of modules 
#define PARSER_MOD 1
#define EVALUATE_SCOP_MOD 2
#define SOG_MOD 3
#define SOAG_MOD 4
#define IP_GEN_MOD 5
#define BIT_GEN_MOD 6

char* get_error(int err_code);

char* get_module(int mod);

char* get_error_message(int err_code, int mod);

int encountered_error(scop_context *scop_cntx);

void set_error(scop_context *scop_cntx, int err_code, int mod);

void print_error_messages(options* opt);

void dump_error_messages(options* opt, char *filename);

#endif