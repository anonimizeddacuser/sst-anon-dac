/*
 *   This file is part of PolyFPGA.
 *
 *   PolyFPGA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * 	 Copyright (C) 2015 Giulio Stramondo
 *
 */
 
#include "hls_generator.h"


void generate_hls(options *opt) {

	scop_context *tmp_context = NULL;

	tmp_context = opt->contexts;

	struct soag_SST* tmp_SST = NULL;

	int i = 0;
	while (tmp_context != NULL) {

		if (encountered_error(tmp_context)) {
			tmp_context = tmp_context->next;
			continue;
		}

		chdir(opt->root_directory);
		chdir(tmp_context->path);
		mkdir("hls_sources", 0755);
		chdir("hls_sources");
		tmp_SST = tmp_context->SST;

		struct soag_filter *filters_it = tmp_SST->channels->filters;
		while (filters_it != NULL) {
			create_hls_filter_from_soag_filter(filters_it);
			filters_it = filters_it->next;
		}

		struct soag_kernel *kernels = tmp_SST -> kernels;
		while (kernels != NULL) {
			//create_kernel(kernels->ID, tmp_SST->dim, tmp_SST->size_dim, kernels->body);
			create_kernel_from_soag_kernel(kernels);
			kernels = kernels -> next;
		}

		struct soag_demux *demux = tmp_SST -> demux;
		create_hls_demux(demux);

		chdir("..");
		tmp_context = tmp_context->next;
		i++;
	}


}

void create_hls_filter_from_soag_filter(struct soag_filter* filter) {
	int filter_ID = filter -> ID;
	int ndim = filter-> channel ->SST ->dim;
	int *size_dim = filter-> channel ->SST -> size_dim;
	osl_relation_p domain = filter -> access -> statement-> domain;
	osl_relation_p access = filter -> access -> access_relation;

	int input_fifo_ID;
	int has_successor = 0;
	int outputs_border = 0;

	int attached_to_dma = 0;
	struct soag_channel_link *links_it;
	links_it = filter->channel->links;

	if (filter == filter->channel->attached_to_DMA) {
		attached_to_dma = 1;
		input_fifo_ID = -1;

		while (links_it != NULL) {
			if (links_it->from == filter ) {
				has_successor = 1;
			}
			links_it = links_it->next;
		}
	}

	else {

		while (links_it != NULL) {
			if (links_it->to == filter ) {
				input_fifo_ID = links_it->from ->ID;
			}
			if (links_it->from == filter ) {
				has_successor = 1;
			}
			links_it = links_it->next;
		}

	}

	int channel_ID = filter-> channel ->ID ;
	int stancil_array_id = filter-> channel ->SST->stancil_array_id;
	int sst_id = filter-> channel ->SST-> num_ID;

	if ((channel_ID == stancil_array_id) && filter->center) {
		outputs_border = 1;
	}

	char * filtercondition = get_filter_equation(domain, access);
	create_hls_filter(sst_id, !has_successor, filter->center, input_fifo_ID, filter-> channel ->ID, attached_to_dma, filter_ID, ndim, size_dim, filtercondition);

	return;
}

void create_hls_filter(int sst_id, int last, int center , int input_fifo_ID, int channel_ID, int attached_to_dma, int filter_ID, int ndim, int size_dim[ndim], char *filtercondition) {
	FILE *fp;
	char filename[15];
	char id[6];
	filename[0] = '\0';

	strcat(filename, "filter_");
	sprintf(id, "%d_%d_%d", sst_id, channel_ID, filter_ID);
	strcat(filename, id);
	strcat(filename, ".cpp");

	fp = fopen(filename, "w");

	fprintf(fp, "#include <ap_int.h>\n#include <hls_stream.h>\n\n");

	int i = 0;
	for (i = 0; i < ndim; i++) {
		fprintf(fp, "#define %c %d\n", 78 + i, size_dim[i]);
	}

	fprintf(fp, "\n#define data_type float\n" );

	fprintf(fp, "template<int D> struct ap_axis{\n" );
	fprintf(fp, "  ap_int<D> data;\n" );
	fprintf(fp, "  ap_uint<1> last;\n" );
	fprintf(fp, "};\n\n" );


	fprintf(fp, "typedef ap_axis<sizeof(data_type)*8> data;\n");
	fprintf(fp, "typedef hls::stream<data> data_stream;\n\n");



	if (attached_to_dma)
		fprintf(fp, "void filter_%d_%d_%d(data_stream &s_axis_dma_%d_in, data_stream &filter_%d", sst_id, channel_ID, filter_ID, channel_ID, filter_ID);
	else
		fprintf(fp, "void filter_%d_%d_%d(data_stream &fifo_%d_%d, data_stream &filter_%d", sst_id, channel_ID, filter_ID, channel_ID, input_fifo_ID, filter_ID);


	if (center)
		fprintf(fp, ",data_stream &border");
	if (!last)
		fprintf(fp, ",data_stream &fifo_%d_%d){\n", channel_ID, filter_ID);
	else
		fprintf(fp, "){\n");




	if (attached_to_dma)
		fprintf(fp, "#pragma HLS INTERFACE axis port=s_axis_dma_%d_in\n", channel_ID);
	else
		fprintf(fp, "#pragma HLS INTERFACE axis port=fifo_%d_%d\n", channel_ID, input_fifo_ID);

	if (!last) {
		fprintf(fp, "#pragma HLS INTERFACE axis port=fifo_%d_%d\n", channel_ID, filter_ID);
	}
	if (center)
		fprintf(fp, "#pragma HLS INTERFACE axis port=border\n");
	fprintf(fp, "#pragma HLS INTERFACE axis port=filter_%d\n\n", filter_ID);
	fprintf(fp, "#pragma HLS INTERFACE ap_ctrl_none port=return\n\n");

	fprintf(fp, "  data in;\n\n");

	i = 0;
	for (i = 0; i < ndim ; i++) {
		fprintf(fp, "  for(register int i%d = 0; i%d <= %c; i%d++)\n",  i,  i, 78 + i,  i);
	}

	if (attached_to_dma)
		fprintf(fp, "  {\n  in = s_axis_dma_%d_in.read();\n\n", channel_ID);
	else
		fprintf(fp, "  {\n  in = fifo_%d_%d.read();\n\n", channel_ID, input_fifo_ID);


	if (!last) {
		fprintf(fp, "  if(%s){\n    filter_%d.write(in);\n    fifo_%d_%d.write(in);\n  }", filtercondition, filter_ID, channel_ID, filter_ID);
	} else
	{
		fprintf(fp, "  if(%s){\n    filter_%d.write(in);\n  }", filtercondition, filter_ID);
	}
	if (!last) {
		if (center) {
			fprintf(fp, "  else{\n    fifo_%d_%d.write(in);\nborder.write(in);\n}\n}\n}", channel_ID, filter_ID );
		} else {
			fprintf(fp, "  else\n    fifo_%d_%d.write(in);\n}\n}", channel_ID, filter_ID );
		}
	}
	else {
		if (center) {
			fprintf(fp, "  else\n    border.write(in);\n}\n}" );
		} else {
			fprintf(fp, " \n}\n}");
		}
	}

	fclose(fp);
	return;

}


char *generate_kernel_exp(char *original_body) {

	char *work_body = remove_spaces_from_statement(original_body);

	char** accesses = get_access_str_from_statement(work_body);

	int accesses_nb = count_accesses_in_statement(work_body);

	char* old_body = NULL;

	int i = 0;
	//char *replacement=(char *) malloc (sizeof(char)*10);
	for (i = 1; i < accesses_nb; i++) {
		char *replacement = (char *) malloc (sizeof(char) * 10);
		sprintf(replacement, "acc_%d", i - 1);
		old_body = work_body;
		work_body = str_replace(work_body, accesses[i], replacement);
		free(old_body);
	}
	return work_body;
}

void create_kernel_from_soag_kernel(struct soag_kernel *kernel) {

	int kernel_ID = kernel->ID;
	struct soag_SST *tmp_SST = kernel->SST;
	int ndim = kernel->SST->dim;
	int *size_dim = kernel->SST->size_dim;
	char *body = kernel->body;
	struct soag_channel *channels_it = tmp_SST->channels;

	while (channels_it != NULL) {
		if ( channels_it->ID == tmp_SST->stancil_array_id ) {
			break;
		}
		channels_it = channels_it ->next;
	}

	struct soag_filter *filters_it = channels_it->filters;

	while (filters_it != NULL) {
		if (filters_it->center)
			break;
		filters_it = filters_it->next;
	}

	osl_relation_p domain = filters_it -> access -> statement-> domain;

	char *loops = get_loops_from_domain(domain);

	create_kernel(tmp_SST->num_ID, kernel_ID, ndim, size_dim, body, loops) ;

}

void create_kernel(int sst_ID, int kernel_ID, int ndim, int size_dim[ndim], char *body, char *loops) {
	char *work_body = remove_spaces_from_statement(body);


	int accesses = count_accesses_in_statement(work_body);
	int reads = accesses - 1;

	FILE *fp;
	char filename[15];
	char id[6];
	filename[0] = '\0';

	strcat(filename, "kernel_");
	sprintf(id, "%d_%d", sst_ID, kernel_ID);
	strcat(filename, id);
	strcat(filename, ".cpp");

	fp = fopen(filename, "w");

	fprintf(fp, "#include <ap_int.h>\n#include <hls_stream.h>\n\n");

	int i = 0;
	for (i = 0; i < ndim; i++) {
		fprintf(fp, "#define %c %d\n", 78 + i, size_dim[i]);
	}

	fprintf(fp, "\n#define data_type float\n" );

	fprintf(fp, "template<int D> struct ap_axis{\n" );
	fprintf(fp, "  ap_int<D> data;\n" );
	fprintf(fp, "  ap_uint<1> last;\n" );
	fprintf(fp, "};\n\n" );


	fprintf(fp, "typedef ap_axis<sizeof(data_type)*8> data;\n");
	fprintf(fp, "typedef hls::stream<data> data_stream;\n\n");

	fprintf(fp, "inline data_type pop(data const &e){\n");
	fprintf(fp, "  union\n  {\n    int ival;\n    data_type oval;\n");
	fprintf(fp, "  } converter;\n  converter.ival = e.data;\n");
	fprintf(fp, "  data_type ret = converter.oval;\n  return ret;\n}\n");


	fprintf(fp, "inline data push(data_type const &v, ap_uint<1> last = 0){\n");
	fprintf(fp, "  data e;\n\n");
	fprintf(fp, "  union\n  {\n    int oval;\n    data_type ival;\n  } converter;\n");
	fprintf(fp, "  converter.ival = v;\n  e.data = converter.oval;\n  e.last = last ? 1 : 0;\n");
	fprintf(fp, "  return e;\n}\n\n");

	fprintf(fp, "void kernel_%d_%d(", sst_ID, kernel_ID);

	for ( i = 0 ; i < reads ; i++) {
		fprintf(fp, "data_stream &port_%d,", i);
	}

	fprintf(fp, "data_stream &demux){\n" );

	for ( i = 0 ; i < reads ; i++) {
		fprintf(fp, "#pragma HLS INTERFACE axis port=port_%d\n", i);
	}

	fprintf(fp, "#pragma HLS INTERFACE axis port=demux\n#pragma HLS INTERFACE ap_ctrl_none port=return\n\n");

	//fprintf(fp, "#pragma HLS RESOURCE variable=out core=FAddSub_fulldsp\n");

	fprintf(fp, "data_type ");

	for ( i = 0 ; i < reads ; i++) {
		fprintf(fp, "acc_%d, ", i);
	}

	fprintf(fp, "out;\n");

	fprintf(fp, "data out_struct;\n");

	//for ( i = 0 ; i < ndim ; i++) {
	//	fprintf(fp, "for (register int i%d = 1; i%d < %c; i%d++) \n", i, i, 78 + i, i);
	//}

	fprintf(fp, "%s\n", loops);

	//decomment pragma
	fprintf(fp, "{\n #pragma HLS LOOP_FLATTEN off\n#pragma HLS PIPELINE II=1\n");

	for ( i = 0 ; i < reads ; i++) {
		fprintf(fp, "acc_%d = pop(port_%d.read());\n ", i, i);
	}


	fprintf(fp, "out = "); //CONVERT BODY EXP USING ACCESSES

	char* expression = generate_kernel_exp(body);

	strtok(expression, "=");
	expression =  strtok(NULL, "=");
	fprintf(fp, "%s", expression);



	fprintf(fp, "\n");

	fprintf(fp, "out_struct = push(out);\n");

	fprintf(fp, "demux.write(out_struct);\n}\n}\n");
	fclose(fp);
}

void create_hls_demux(struct soag_demux* demux) {
	FILE *fp;
	struct soag_SST *SST = demux -> SST;

	char filename[15];
	char id[6];
	filename[0] = '\0';

	strcat(filename, "demux_");
	sprintf(id, "%d", SST->num_ID);
	strcat(filename, id);
	strcat(filename, ".cpp");
	
	fp = fopen(filename, "w");
	int ndim = demux -> SST -> dim;
	int *size_dim = SST-> size_dim;

	struct soag_channel *channels_it = SST ->channels;

	while (channels_it != NULL) {
		if ( channels_it->ID == SST ->stancil_array_id ) {
			break;
		}
		channels_it = channels_it ->next;
	}

	struct soag_filter *filters_it = channels_it->filters;

	while (filters_it != NULL) {
		if (filters_it->center)
			break;
		filters_it = filters_it->next;
	}

	osl_relation_p domain = filters_it -> access -> statement-> domain;
	osl_relation_p access = filters_it -> access ->access_relation;
	char * filtercondition = get_filter_equation(domain, access);

	fprintf(fp, "#include <ap_int.h>\n#include <hls_stream.h>\n\n");

	int i = 0;
	for (i = 0; i < ndim; i++) {
		fprintf(fp, "#define %c %d\n", 78 + i, size_dim[i]);
	}

	fprintf(fp, "\n#define data_type float\n" );

	fprintf(fp, "template<int D> struct ap_axis{\n" );
	fprintf(fp, "  ap_int<D> data;\n" );
	fprintf(fp, "  ap_uint<1> last;\n" );
	fprintf(fp, "};\n\n" );


	fprintf(fp, "typedef ap_axis<sizeof(data_type)*8> data;\n");
	fprintf(fp, "typedef hls::stream<data> data_stream;\n\n");

	struct soag_demux_port *ports = demux -> ports;

	fprintf(fp, "void demux_%d( ", SST->num_ID);


	while (ports != NULL) {
		if (ports ->next != NULL) {
			if ( ports->port_type != DEMUX_OUT)
				fprintf(fp, "data_stream &port_%d,", ports->ID);
			else
				fprintf(fp, "data_stream &m_axis_port_%d,", ports->ID);
		}
		else {
			if ( ports->port_type != DEMUX_OUT)
				fprintf(fp, "data_stream &port_%d", ports->ID);
			else
				fprintf(fp, "data_stream &m_axis_port_%d", ports->ID);
		}
		ports = ports ->next;
	}

	fprintf(fp, "){ \n");

	ports = demux -> ports;
	while (ports != NULL) {
		if ( ports->port_type != DEMUX_OUT)
			fprintf(fp, "#pragma HLS INTERFACE axis port=port_%d\n", ports->ID);
		else
			fprintf(fp, "#pragma HLS INTERFACE axis port=m_axis_port_%d\n", ports->ID);
		ports = ports ->next;
	}

	fprintf(fp, "#pragma HLS INTERFACE ap_ctrl_none port=return\n");

	fprintf(fp, "\ndata in;\n");

	for ( i = 0 ; i < ndim ; i++) {
		fprintf(fp, "for (register int i%d = 0; i%d <= %c; i%d++) \n", i, i, 78 + i, i);
	}

	fprintf(fp, "{\n" );

	//TODO : HERE IT SHOULD BE HANDLED THE CASE WITH CYCLIC DEPENDENCY
	//SO WITH MORE KERNELS
	//each kernel have to store it's own domain, and there should be a
	//set of ifs one for each domain that takes input from the respective domain
	//then there should be the last else that handles the border
	//
	//At this stage cyclic dependencies are not handled, so I assume that
	//there is just one kernel, so I'll just generate demux with one if.

	//the taken condition is the stancil array's central filter
	// filtering condition

	fprintf(fp, "if( %s){\n", filtercondition );

	ports = demux -> ports;
	struct soag_demux_port *ports_kernel_in = NULL;
	struct soag_demux_port *ports_border_in = NULL;
	struct soag_demux_port *ports_demux_out = NULL;

	//find kernel_in port read from the port
	//then do else branch

	while (ports != NULL) {
		if (ports->port_type == KERNEL_IN)
			ports_kernel_in = ports;
		else if (ports->port_type == BORDER_IN)
			ports_border_in = ports;
		else if ( ports->port_type == DEMUX_OUT)
			ports_demux_out = ports;
		ports = ports ->next;
	}

	fprintf(fp, "in = port_%d.read();\n}\n", ports_kernel_in->ID);
	fprintf(fp, "else{\n");
	fprintf(fp, "in = port_%d.read();\n}\n", ports_border_in->ID);





	fprintf(fp, "if( ");
	for ( i = 0 ; i < SST-> dim ; i++) {
		if ( i == SST->dim - 1) {
			fprintf(fp, "i%d == %d ){\n", i, SST->size_dim[i]);
		}
		else {
			fprintf(fp, "i%d == %d && ", i, SST->size_dim[i]);
		}
	}

	fprintf(fp, "in.last = 1;\nm_axis_port_%d.write(in);\n}\n", ports_demux_out->ID);
	fprintf(fp, "else {\nin.last = 0;\nm_axis_port_%d.write(in);\n}\n}\n}\n", ports_demux_out->ID);
	fclose(fp);
	return ;
}