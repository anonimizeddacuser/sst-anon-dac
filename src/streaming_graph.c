/*
 *   This file is part of PolyFPGA.
 *
 *   PolyFPGA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *   Copyright (C) 2015 Giulio Stramondo
 *
 */
 
#include "streaming_graph.h"

osl_dependence_p edge_removal(osl_dependence_p dep ) {
  osl_dependence_p result = NULL;
  osl_dependence_p previous_ptr = NULL;
  osl_dependence_p current_ptr = NULL;

  previous_ptr = dep;
  while (previous_ptr != NULL) {
    if (previous_ptr->depth != 1 || (previous_ptr->label_source < previous_ptr-> label_target )) {
      //printf("depth %d, source:%d ,target:%d\n", previous_ptr->depth,previous_ptr->label_source,previous_ptr-> label_target);
      if (result == NULL) {
        result = osl_dependence_clone(previous_ptr);
        result->next = NULL;
        current_ptr = result;
      }
      else {
        current_ptr->next = osl_dependence_clone(previous_ptr);
        current_ptr = current_ptr->next;
        current_ptr->next = NULL;
      }
    }
    previous_ptr = previous_ptr->next;
  }
  return result;
}

sog_access* create_sog_access() {
  sog_access* sog_access_res = NULL;
  sog_access_res = (sog_access *) malloc(sizeof(sog_access));
  sog_access_res->type = READ;
  sog_access_res->access_ref = 0;
  sog_access_res->statement = NULL ;
  sog_access_res->string = NULL;
  sog_access_res->next = NULL;
  return sog_access_res;
}

sog_statement* create_sog_statement() {
  sog_statement *sog_statement_res = NULL;
  sog_statement_res = (sog_statement *) malloc(sizeof(sog_statement));
  sog_statement_res->access = NULL;
  sog_statement_res->access_nb = 0;
  sog_statement_res->statement_ref = 0 ;
  sog_statement_res-> body = NULL;
  sog_statement_res->next = NULL ;
  return sog_statement_res;
}
sog_statement* allocate_sog_statements(osl_scop_p scop) {
  osl_statement_p statements = scop->statement;
  sog_statement *sog_statement_head = NULL;
  sog_statement *sog_statement_current = NULL;
  sog_statement *sog_statement_last = NULL;
  osl_body_p body = NULL;

  int i = 0;
  while (statements != NULL) {
    sog_statement_current = create_sog_statement();
    if (sog_statement_head == NULL)
    {
      sog_statement_head =  sog_statement_current;
    }

    body = (osl_body_p)osl_generic_lookup(statements->extension, OSL_URI_BODY);

    char ** access = get_access_str_from_statement(body->expression->string[0]);
    sog_statement_current -> body = strdup (body->expression->string[0]);
    sog_statement_current -> access_nb  = osl_relation_list_count(statements->access);
    sog_statement_current -> statement_ref = i;
    sog_statement_current -> domain = statements -> domain;
    sog_statement_current -> scattering = statements -> scattering;
    int j = 0;

    sog_access *access_head = NULL;
    sog_access *access_last = NULL;
    sog_access *access_current = NULL;

    for (j = 0; j < sog_statement_current->access_nb; j++) {
      access_current = create_sog_access();
      if (access_head == NULL)
      {
        access_head = access_current;
      }
      access_current-> access_ref = j;
      access_current->statement = sog_statement_current;
      access_current->string = access[j];

      osl_relation_list_p tmp_access_lst = statements->access;
      int k = 0;
      for (k = 0; k < j; k++)
        tmp_access_lst = tmp_access_lst -> next;

      access_current -> access_relation = tmp_access_lst -> elt;

      if (j == 0)
        access_current->type = WRITE;
      else
        access_current->type = READ;

      access_current -> next = NULL;
      if (access_last != NULL)
      {
        access_last->next = access_current;
      }
      access_last = access_current;
    }

    sog_statement_current->access = access_head;

    if (sog_statement_last != NULL) {
      sog_statement_last->next = sog_statement_current;
    }
    sog_statement_last = sog_statement_current;
    statements = statements->next;
    i++;

  }
  return sog_statement_head;
}

void dump_sog_statement(sog_statement *try_stmt) {
  sog_access *try_access = NULL;
  while (try_stmt != NULL) {
    printf("S%d: %s\n", try_stmt -> statement_ref, try_stmt->body);
    printf("Iteration Domain\n");
    osl_relation_dump(stdout , try_stmt -> domain);

    printf("scattering Function\n");
    osl_relation_dump(stdout , try_stmt -> scattering);

    try_access = try_stmt->access;
    while (try_access != NULL) {
      printf("ref%d: %s ", try_access -> access_ref, try_access->string);
      if (try_access->type == READ)
        printf("READ\n");
      else
        printf("WRITE\n");
      printf("Access Relation\n");
      osl_relation_dump(stdout , try_access -> access_relation);

      try_access = try_access->next;
    }
    printf("\n");
    try_stmt = try_stmt->next;
  }
  return;
}

void generate_dot_from_sog_graph(FILE* output, sog_graph* graph) {
  fprintf(output, "digraph G {\n# Streaming Oriented Graph\n");
  sog_access *tmp_read_access = NULL;
  sog_access *tmp_write_access = NULL;
  int stmt_ref_source = 0;
  int stmt_ref_dest = 0;

  while (graph != NULL) {
    tmp_read_access = graph->from;
    tmp_write_access = graph->to;
    stmt_ref_source = tmp_read_access -> statement -> statement_ref;
    stmt_ref_dest = tmp_write_access -> statement -> statement_ref;
    char *stmt_str = tmp_write_access -> statement -> body;
    if (tmp_write_access->type == WRITE)
      fprintf(output, "\t\tA%d_S%d [label=\"%s\", xlabel=\"%s\"shape=\"rect\"];\n", tmp_write_access->access_ref, stmt_ref_dest, tmp_write_access->string, stmt_str);
    else
      fprintf(output, "\t\tA%d_S%d [label=\"%s\"];\n", tmp_write_access->access_ref, stmt_ref_dest, tmp_write_access->string);

    fprintf(output, "\t\tA%d_S%d [label=\"%s\"];\n", tmp_read_access->access_ref, stmt_ref_source, tmp_read_access->string);
    fprintf(output, "\t\tA%d_S%d -> A%d_S%d;\n",
            tmp_read_access->access_ref, stmt_ref_source, tmp_write_access->access_ref, stmt_ref_dest);

    graph = graph -> next;
  }
  fprintf(output, "}\n");
}

void dump_edge (FILE* output, sog_graph* graph) {
  sog_access *tmp_read_access = NULL;
  sog_access *tmp_write_access = NULL;
  int stmt_ref_source = 0;
  int stmt_ref_dest = 0;

  tmp_read_access = graph->from;
  tmp_write_access = graph->to;
  stmt_ref_source = tmp_read_access -> statement -> statement_ref;
  stmt_ref_dest = tmp_write_access -> statement -> statement_ref;
  fprintf(output, "A%d_S%d -> A%d_S%d;\n",
          tmp_read_access->access_ref, stmt_ref_source, tmp_write_access->access_ref, stmt_ref_dest);
  return;
}


void generate_dot_from_sog_statement(FILE* output, sog_statement *try_stmt, osl_dependence_p dep) {
  fprintf(output, "digraph G {\n# Streaming Oriented Graph with original statements\n");
  sog_access *tmp_access = NULL;
  sog_access *tmp_write_access = NULL;

  while (try_stmt != NULL) {
    fprintf(output, "\tsubgraph cluster_%d {\n\t\tlabel=\"S%d\";\n", try_stmt->statement_ref, try_stmt->statement_ref );
    tmp_write_access =  try_stmt->access;
    tmp_access = tmp_write_access->next;
    fprintf(output, "\t\tA%d_S%d [label=\"%s\"];\n", tmp_write_access->access_ref, try_stmt->statement_ref, tmp_write_access->string);
    while (tmp_access != NULL) {
      fprintf(output, "\t\tA%d_S%d [label=\"%s\"];\n", tmp_access->access_ref, try_stmt->statement_ref, tmp_access->string);
      fprintf(output, "\t\tA%d_S%d -> A%d_S%d;\n", tmp_access->access_ref, try_stmt->statement_ref, tmp_write_access->access_ref, try_stmt->statement_ref);
      tmp_access = tmp_access->next;
    }
    fprintf(output, "\t}\n\n");
    try_stmt = try_stmt->next;
  }

  while (dep != NULL) {
    fprintf(output, "\tA%d_S%d -> A%d_S%d;\n", dep->ref_source, dep->label_source, dep->ref_target, dep->label_target);
    dep = dep -> next;
  }

  fprintf(output, "}\n");
  return;
}

sog_access *get_sog_access(sog_statement *try_stmt, int stmt_ref, int acc_ref) {
  sog_access* result = NULL;
  sog_access* tmp_access = NULL;

  while (try_stmt != NULL) {
    if (try_stmt->statement_ref == stmt_ref) {
      tmp_access = try_stmt->access;
      while (tmp_access != NULL && tmp_access->access_ref != acc_ref)
        tmp_access = tmp_access -> next;

      if (tmp_access != NULL && tmp_access->access_ref == acc_ref) {
        result = tmp_access;
        return result;
      }
    }
    try_stmt = try_stmt->next;
  }
  return result;
}


sog_graph_edges_list *get_edges_starting_from_sog_access(sog_graph *graph, sog_access* from) {
  sog_graph_edges_list *result = NULL;
  sog_graph_edges_list *result_tmp = NULL;
  sog_graph_edges_list *result_last = NULL;

  while (graph != NULL) {
    if (graph->from == from) {
      result_tmp = (sog_graph_edges_list *)malloc(sizeof(sog_graph_edges_list));
      result_tmp->edge = graph;
      result_tmp->next = NULL;

      if (result == NULL)
        result = result_tmp;

      if (result_last != NULL)
        result_last ->next = result_tmp;

      result_last = result_tmp;
    }
    graph = graph->next;
  }
  return result;
}

sog_graph_edges_list *get_edges_arriving_to_sog_access(sog_graph *graph, sog_access* to) {
  sog_graph_edges_list *result = NULL;
  sog_graph_edges_list *result_tmp = NULL;
  sog_graph_edges_list *result_last = NULL;

  while (graph != NULL) {
    if (graph->to == to) {
      result_tmp = (sog_graph_edges_list *)malloc(sizeof(sog_graph_edges_list));
      result_tmp->edge = graph;
      result_tmp->next = NULL;

      if (result == NULL)
        result = result_tmp;

      if (result_last != NULL)
        result_last ->next = result_tmp;

      result_last = result_tmp;
    }
    graph = graph->next;
  }
  return result;
}

void dump_sog_graph_edges_list(FILE* output, sog_graph_edges_list* edge_list) {
  while (edge_list != NULL) {
    dump_edge(output, edge_list->edge);

    edge_list = edge_list->next;
  }

}

sog_graph *generate_sog_graph_from_sog_statement(sog_statement *try_stmt, osl_dependence_p dep) {
  sog_access *tmp_access = NULL;
  sog_access *tmp_write_access = NULL;
  sog_graph  *head_streaming_graph = NULL;
  sog_graph  *tmp_streaming_graph = NULL;
  sog_graph  *tmp_streaming_graph_last = NULL;
  sog_statement *head_stmt = try_stmt;
  while (try_stmt != NULL) {

    tmp_write_access = try_stmt->access;
    tmp_access = tmp_write_access->next;

    while (tmp_access != NULL) {
      tmp_streaming_graph = (sog_graph*) malloc(sizeof(sog_graph));
      tmp_streaming_graph -> from = tmp_access;
      tmp_streaming_graph -> to = tmp_write_access;
      tmp_streaming_graph -> next = NULL;

      if (head_streaming_graph == NULL)
        head_streaming_graph = tmp_streaming_graph;

      if (tmp_streaming_graph_last != NULL)
        tmp_streaming_graph_last -> next = tmp_streaming_graph;

      tmp_streaming_graph_last = tmp_streaming_graph;
      tmp_access = tmp_access -> next;
    }
    try_stmt = try_stmt -> next;
  }

  while (dep != NULL) {
    tmp_access = get_sog_access(head_stmt, dep->label_source, dep->ref_source);
    tmp_write_access = get_sog_access(head_stmt, dep->label_target, dep->ref_target);

    tmp_streaming_graph = (sog_graph*) malloc(sizeof(sog_graph));
    tmp_streaming_graph -> from = tmp_access;
    tmp_streaming_graph -> to = tmp_write_access;
    tmp_streaming_graph -> next = NULL;

    if (head_streaming_graph == NULL)
      head_streaming_graph = tmp_streaming_graph;

    if (tmp_streaming_graph_last != NULL)
      tmp_streaming_graph_last -> next = tmp_streaming_graph;

    tmp_streaming_graph_last = tmp_streaming_graph;
    dep = dep->next;
  }


  return head_streaming_graph;
}

int access_lst_contain_sog_access(access_lst *lst, sog_access* iscontained) {
  while (lst != NULL) {
    if (lst->access == iscontained)
      return 1;
    lst = lst->next;
  }
  return 0;
}


access_lst *get_write_access_from_sog_graph(sog_graph *graph) {
  access_lst *head_res = NULL ;
  access_lst *current_res = NULL ;
  access_lst *last_res = NULL ;

  sog_access *tmp_access_from = NULL;
  sog_access *tmp_access_to = NULL;

  while (graph != NULL) {
    tmp_access_from = graph->from;
    tmp_access_to = graph->to;

    if (tmp_access_from->type == WRITE &&
        !access_lst_contain_sog_access(head_res, tmp_access_from)) {
      current_res = (access_lst*) malloc(sizeof(access_lst));
      current_res->next = NULL;
      current_res->access = tmp_access_from;
      if (head_res == NULL) {
        head_res = current_res;
      }
      if (last_res != NULL)
        last_res->next = current_res;
      last_res = current_res;
    }

    if (tmp_access_to->type == WRITE &&
        !access_lst_contain_sog_access(head_res, tmp_access_to)) {
      current_res = (access_lst*) malloc(sizeof(access_lst));
      current_res->next = NULL;
      current_res->access = tmp_access_to;
      if (head_res == NULL) {
        head_res = current_res;
      }
      if (last_res != NULL)
        last_res->next = current_res;
      last_res = current_res;
    }

    graph = graph -> next;
  }
  return head_res;
}

access_lst *get_read_access_from_sog_graph(sog_graph *graph) {
  access_lst *head_res = NULL ;
  access_lst *current_res = NULL ;
  access_lst *last_res = NULL ;

  sog_access *tmp_access_from = NULL;
  sog_access *tmp_access_to = NULL;

  while (graph != NULL) {
    tmp_access_from = graph->from;
    tmp_access_to = graph->to;

    if (tmp_access_from->type == READ &&
        !access_lst_contain_sog_access(head_res, tmp_access_from)) {
      current_res = (access_lst*) malloc(sizeof(access_lst));
      current_res->next = NULL;
      current_res->access = tmp_access_from;
      if (head_res == NULL) {
        head_res = current_res;
      }
      if (last_res != NULL)
        last_res->next = current_res;
      last_res = current_res;
    }

    if (tmp_access_to->type == READ &&
        !access_lst_contain_sog_access(head_res, tmp_access_to)) {
      current_res = (access_lst*) malloc(sizeof(access_lst));
      current_res->next = NULL;
      current_res->access = tmp_access_to;
      if (head_res == NULL) {
        head_res = current_res;
      }
      if (last_res != NULL)
        last_res->next = current_res;
      last_res = current_res;
    }

    graph = graph -> next;
  }
  return head_res;
}
access_lst *get_write_access(sog_statement *try_stmt) {
  access_lst *head_res = NULL ;
  access_lst *current_res = NULL ;
  access_lst *last_res = NULL ;
  sog_access *tmp_access = NULL;


  while (try_stmt != NULL) {
    tmp_access = try_stmt -> access;
    while (tmp_access != NULL) {
      if (tmp_access->type == WRITE) {
        current_res = (access_lst*) malloc(sizeof(access_lst));
        current_res->next = NULL;
        current_res->access = tmp_access;
        if (head_res == NULL) {
          head_res = current_res;
        }
        if (last_res != NULL)
          last_res->next = current_res;
        last_res = current_res;
      }
      tmp_access = tmp_access ->next;
    }
    try_stmt = try_stmt->next;
  }
  return head_res;
}

void dump_sog_access(FILE* output, sog_access* tmp_access) {
  sog_statement* stmt = NULL;
  stmt = tmp_access-> statement;
  fprintf(output, "ref%d: %s, s%d:%s\n", tmp_access -> access_ref, tmp_access->string, stmt->statement_ref, stmt->body);
  return;
}

void dump_sog_access_lst(FILE* output, access_lst * acc_lst ) {
  sog_access* tmp_access;

  while (acc_lst != NULL ) {
    tmp_access = acc_lst -> access;
    dump_sog_access(output, tmp_access);
    acc_lst = acc_lst->next;
  }
  return;
}

int count_access_lst(access_lst * acc_lst) {
  int result = 0;
  while (acc_lst != NULL) {
    result ++;
    acc_lst = acc_lst -> next;
  }
  return result;
}

int count_sog_graph_edges_list(sog_graph_edges_list* edges_lst) {
  int result = 0;
  while (edges_lst != NULL) {
    result ++;
    edges_lst = edges_lst -> next;
  }
  return result;
}


int access_to_same_array(sog_access* acc_1, sog_access* acc_2) {
  char *array1 = NULL;
  char *array2 = NULL;
  char* acc_str1 = acc_1->string;
  char* acc_str2 = acc_2->string;
  char* local_acc_str1 = NULL;
  char* local_acc_str2 = NULL;

  local_acc_str1 = strdup (acc_str1);
  local_acc_str2 = strdup (acc_str2);

  array1 = strtok(local_acc_str1, "[");
  array2 = strtok(local_acc_str2, "[");
  if (!strcmp(array1, array2)) {
    free(local_acc_str1);
    free(local_acc_str2);
    return 1;
  }
  else {
    free(local_acc_str1);
    free(local_acc_str2);
    return 0;
  }
}
//TODO FINISH WITH THE DOMAIN ANALYSIS
//TODO TESTS
sog_graph_edges_list* get_copy_edges(sog_graph* graph) {
  access_lst *write_access_lst = NULL;

  sog_graph_edges_list* res_edges = NULL;
  sog_graph_edges_list* current_edges = NULL;
  sog_graph_edges_list* last_edges = NULL;

  sog_graph_edges_list* tmp_edges = NULL;
  sog_graph *tmp_edge = NULL;
  sog_graph *tmp_deeper_edge = NULL;
  sog_access *tmp_access = NULL;
  sog_graph_edges_list *to_edges = NULL;
  int counter = 0;

  //Starting node must be a write node
  write_access_lst = get_write_access_from_sog_graph(graph);

  while (write_access_lst != NULL) {
    tmp_access = write_access_lst->access;
    to_edges = get_edges_starting_from_sog_access(graph, tmp_access);
    counter = count_sog_graph_edges_list(to_edges);

    //There must be just one outgoing edge
    if (counter == 1) {
      tmp_edge = to_edges -> edge;
      //There must not be a loop going back to the starting write node
      if (tmp_edge->to != tmp_access) {

        sog_access *tmp_access_to = tmp_edge->to;

        osl_relation_p D1 = tmp_access ->statement->domain;
        osl_relation_p A1 = tmp_access -> access_relation;

        osl_relation_p D2 = tmp_access_to ->statement->domain;
        osl_relation_p A2 = tmp_access_to -> access_relation;
        //if N is a read node
        if (tmp_edge->to->type == READ) {
          sog_access *corresponding_write_access = NULL;
          tmp_edges = get_edges_starting_from_sog_access(graph, tmp_edge->to);
          tmp_deeper_edge = tmp_edges -> edge;
          corresponding_write_access = tmp_deeper_edge->to;
          //the corresponding write access must not
          // refer to the same array as w
          if (!access_to_same_array(tmp_access, corresponding_write_access)) {
            //TODO
            //check the same data domain
            if (same_data_domain(D1, A1, D2, A2)) {
              current_edges = (sog_graph_edges_list*) malloc(sizeof(sog_graph_edges_list));
              current_edges -> next = NULL;
              current_edges -> edge = tmp_edge;

              if (res_edges == NULL)
                res_edges = current_edges;

              if (last_edges != NULL)
                last_edges->next = current_edges;

              last_edges = current_edges;
            }
          }
        } else {
          // TODO
          // check the same data domain


          if (same_data_domain(D1, A1, D2, A2)) {
            current_edges = (sog_graph_edges_list*) malloc(sizeof(sog_graph_edges_list));
            current_edges -> next = NULL;
            current_edges -> edge = tmp_edge;

            if (res_edges == NULL)
              res_edges = current_edges;

            if (last_edges != NULL)
              last_edges->next = current_edges;

            last_edges = current_edges;
          }
        }
      }
    }
    write_access_lst = write_access_lst->next;
  }

  return res_edges;
}


char *str_replace(char *orig, char *rep, char *with) {
  char *result; // the return string
  char *ins;    // the next insert point
  char *tmp;    // varies
  int len_rep;  // length of rep
  int len_with; // length of with
  int len_front; // distance between rep and end of last rep
  int count;    // number of replacements

  if (!orig)
    return NULL;
  if (!rep)
    rep = "";
  len_rep = strlen(rep);
  if (!with)
    with = "";
  len_with = strlen(with);

  ins = orig;
  for (count = 0; tmp = strstr(ins, rep); ++count) {
    ins = tmp + len_rep;
  }

  // first time through the loop, all the variable are set correctly
  // from here on,
  //    tmp points to the end of the result string
  //    ins points to the next occurrence of rep in orig
  //    orig points to the remainder of orig after "end of rep"
  tmp = result = malloc(strlen(orig) + (len_with - len_rep) * count + 1);

  if (!result)
    return NULL;

  while (count--) {
    ins = strstr(orig, rep);
    len_front = ins - orig;
    tmp = strncpy(tmp, orig, len_front) + len_front;
    tmp = strcpy(tmp, with) + len_with;
    orig += len_front + len_rep; // move to next "end of rep"
  }
  strcpy(tmp, orig);
  return result;
}

char *wrap_in_parenthesis(char *original) {

  char *wrapped_from_body = (char *)malloc(sizeof(char) * strlen(original) + 2);
  wrapped_from_body[0] = '\0';
  strcat(wrapped_from_body, "(");
  strcat(wrapped_from_body, original);
  strcat(wrapped_from_body, ")");
  return wrapped_from_body;
}

char *select_assignment_expression(char *body) {
  char *tmp = strdup(body);
  strtok(tmp, "=");
  char *tmp1 = strtok(NULL, "=");
  char *res = strtok(tmp1, ";");
  return res;
}
//Doesn't simply remove the edge
//see page 87

//TODO NEED TO CREATE UNIQUE IDENTIFIERS (REFERENCES) FOR COMPOUND STATEMENT
int remove_copy_edge(sog_graph* graph, sog_graph_edges_list* edges_lst) {

  sog_graph *edge_to_remove = edges_lst ->edge;
  sog_access *acc_to_remove = edge_to_remove -> to;
  sog_access *acc_from = edge_to_remove -> from;
  sog_graph* edge_before = graph;
  while (edge_before->next != edge_to_remove) {
    edge_before = edge_before->next;
    if (edge_before == NULL) {
      printf("Error edge_before not found (remove_edge)\n");
      return 0;
    }
  }

  if (acc_to_remove -> type == READ) {
    edge_before->next = edge_to_remove ->next;
    sog_graph_edges_list* connected_to_removed = get_edges_starting_from_sog_access(graph, acc_to_remove);

    while (connected_to_removed != NULL) {
      connected_to_removed -> edge -> from =  acc_from;
      connected_to_removed = connected_to_removed ->next;
    }
    free(edge_to_remove);
  }
  else {
    edge_before->next = edge_to_remove ->next;
    //Creating new node information
    sog_access *new_compound_access = (sog_access *)malloc(sizeof(sog_access));
    new_compound_access->access_ref = 99;
    new_compound_access ->string = strdup(acc_to_remove->string);
    new_compound_access ->type = WRITE;
    new_compound_access -> access_relation = osl_relation_clone(acc_to_remove->access_relation);
    new_compound_access ->next = NULL;
    sog_statement *new_fake_stmt = (sog_statement *)malloc(sizeof(sog_statement));
    new_fake_stmt -> statement_ref = 99;
    new_fake_stmt -> domain = NULL;
    new_fake_stmt -> scattering = NULL;
    new_fake_stmt -> next = NULL;
    new_fake_stmt -> access_nb = 1;
    new_fake_stmt -> access = new_compound_access;

    //creating assignment expression
    char *from_body = strdup(acc_from ->statement->body);
    char *wrapped_from_body = wrap_in_parenthesis(select_assignment_expression(from_body));
    char *original_assignment = acc_to_remove->statement->body;
    char *compound_body = str_replace(original_assignment, acc_from->string, wrapped_from_body);

    if(!strcmp(original_assignment,compound_body)){
      return INCOHERENT_SUB_FUNC;
    }

    new_fake_stmt -> body = compound_body;
    new_compound_access -> statement = new_fake_stmt;

    sog_graph_edges_list* connected_to_removed = get_edges_starting_from_sog_access(graph, acc_to_remove);

    while (connected_to_removed != NULL) {
      connected_to_removed -> edge -> from =  new_compound_access;
      connected_to_removed = connected_to_removed -> next;
    }

    connected_to_removed = get_edges_arriving_to_sog_access(graph, acc_from);

    while (connected_to_removed != NULL) {
      connected_to_removed -> edge -> to =  new_compound_access;
      connected_to_removed = connected_to_removed -> next;
    }
    free(edge_to_remove);
  }
  return 0;
}


void generate_sog_data(options *opt) {
  scop_context *tmp_context = NULL;
  tmp_context = opt->contexts;
  FILE* sog_output;

  while (tmp_context != NULL) {
    //checks if this scop has encoutered an error before
    //if so skips to the next scop
    if(encountered_error(tmp_context)){
      tmp_context = tmp_context->next;
      continue;
    }

    chdir(opt->root_directory);
    chdir(tmp_context->path);
    tmp_context->dep_edge_removed = edge_removal(tmp_context->dep);
    dump_deps_to_file(tmp_context->dep_edge_removed, "tmp.candl.edge_removal.dot");

    tmp_context->sog_stmts = allocate_sog_statements(tmp_context->scop);

    sog_output = fopen("tmp.sog.dot", "w");
    generate_dot_from_sog_statement(sog_output, tmp_context->sog_stmts, tmp_context->dep_edge_removed);
    fclose(sog_output);

    tmp_context->graph = generate_sog_graph_from_sog_statement(tmp_context->sog_stmts, tmp_context->dep_edge_removed);
    sog_output = fopen("tmp.sog1.dot", "w");
    generate_dot_from_sog_graph(sog_output, tmp_context->graph);
    fclose(sog_output);

    sog_graph_edges_list* copy_edges = NULL;
    copy_edges = get_copy_edges(tmp_context->graph);

    char tmp_filename[20];
    char tmp[4];
    int i = 0;
    int err = 0;
    while (copy_edges != NULL) {
      tmp_filename[0] = '\0';
      strcat(tmp_filename, "tmp.sog.copyremoved");
      sprintf(tmp, "%d", i);
      strcat(tmp_filename, tmp);
      strcat(tmp_filename, ".dot");

      err = remove_copy_edge(tmp_context->graph, copy_edges);

      //An error was found
      //flag the current scop with the error informations
      if(err){
        tmp_context->err_id=err;
        tmp_context->module_id= THIS_MOD;
        set_error(tmp_context,err,THIS_MOD);
      }

      sog_output = fopen(tmp_filename, "w");
      generate_dot_from_sog_graph(sog_output, tmp_context->graph);
      fclose(sog_output);
      copy_edges = get_copy_edges(tmp_context->graph);
      i++;
    }

    tmp_context = tmp_context->next;
  }
  chdir(opt->root_directory);
  return;
}

void generate_sog_data_no_dump(options *opt) {
  scop_context *tmp_context = NULL;
  tmp_context = opt->contexts;
  FILE* sog_output;

  while (tmp_context != NULL) {
    //checks if this scop has encoutered an error before
    //if so skips to the next scop
    if(encountered_error(tmp_context)){
      tmp_context = tmp_context->next;
      continue;
    }

    //chdir(opt->root_directory);
    //chdir(tmp_context->path);
    tmp_context->dep_edge_removed = edge_removal(tmp_context->dep);
    //dump_deps_to_file(tmp_context->dep_edge_removed, "tmp.candl.edge_removal.dot");

    tmp_context->sog_stmts = allocate_sog_statements(tmp_context->scop);

    //sog_output = fopen("tmp.sog.dot", "w");
    //generate_dot_from_sog_statement(sog_output, tmp_context->sog_stmts, tmp_context->dep_edge_removed);
    //fclose(sog_output);

    tmp_context->graph = generate_sog_graph_from_sog_statement(tmp_context->sog_stmts, tmp_context->dep_edge_removed);
    //sog_output = fopen("tmp.sog1.dot", "w");
    //generate_dot_from_sog_graph(sog_output, tmp_context->graph);
    //fclose(sog_output);

    sog_graph_edges_list* copy_edges = NULL;
    copy_edges = get_copy_edges(tmp_context->graph);

   // char tmp_filename[20];
    //char tmp[4];
    int i = 0;
    int err = 0;
    while (copy_edges != NULL) {
      //tmp_filename[0] = '\0';
      //strcat(tmp_filename, "tmp.sog.copyremoved");
      //sprintf(tmp, "%d", i);
      //strcat(tmp_filename, tmp);
      //strcat(tmp_filename, ".dot");

      err = remove_copy_edge(tmp_context->graph, copy_edges);

      //An error was found
      //flag the current scop with the error informations
      if(err){
        tmp_context->err_id=err;
        tmp_context->module_id= THIS_MOD;
        set_error(tmp_context,err,THIS_MOD);
      }

      //sog_output = fopen(tmp_filename, "w");
      //generate_dot_from_sog_graph(sog_output, tmp_context->graph);
      //fclose(sog_output);
      copy_edges = get_copy_edges(tmp_context->graph);
      i++;
    }

    tmp_context = tmp_context->next;
  }
  //chdir(opt->root_directory);
  return;
}