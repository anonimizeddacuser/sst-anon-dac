/*
 *   This file is part of PolyFPGA.
 *
 *   PolyFPGA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *   Copyright (C) 2015 Giulio Stramondo
 *
 */

#ifndef STRUCT_H
#define STRUCT_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <clan/clan.h>
#include <osl/scop.h>

#include <osl/scop.h>
#include <osl/macros.h>
#include <osl/util.h>
#include <osl/extensions/dependence.h>
#include <osl/extensions/arrays.h>
#include <candl/macros.h>
#include <candl/dependence.h>
#include <candl/violation.h>
#include <candl/options.h>
#include <candl/scop.h>
#include <candl/util.h>
#include <candl/piplib.h>

#include "struct_soag.h"
#include "struct_sog.h"


struct scop_context
{
  //ALL
  int err_id;
  int module_id;

  //Parser
  int id;
  osl_scop_p scop;
  char* path;
  osl_dependence_p dep;

  //SOA
  osl_dependence_p dep_edge_removed;
  sog_statement *sog_stmts;
  sog_graph *graph;

  //SOA
  struct soag_SST* SST;

  struct scop_context *next;
};

  struct queuing {
    int* args;
    int arg_nb;
  } queuing;

struct options {
  int step;
  int board;
  struct queuing* queuing;
  char root_directory[1024];
  char *input;
  struct scop_context *current; //TODO check if this attribute is really used
  struct scop_context *contexts;
};

typedef struct scop_context scop_context;
typedef struct options options;
#endif