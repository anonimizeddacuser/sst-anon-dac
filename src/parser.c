/*
 *   This file is part of PolyFPGA.
 *
 *   PolyFPGA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * 	 Copyright (C) 2015 Giulio Stramondo
 *
 */

#include "parser.h"

options *init_opt() {
	options* res = (options *) malloc(sizeof(options));
	res->step = 0;
	res->input = NULL;
	res->board = 0; // Zybo
	res->contexts = NULL;
	res->queuing = NULL;
	getcwd(res->root_directory, sizeof(res->root_directory));
	//printf("ROOT DIRECTORY: %s\n", res->root_directory);
	return res;
}

options* parse_args(int argv , char* argc[]) {
	char *inputfile = NULL;
	int i = 0, j = 0;
	options* res;
	res = init_opt();

	for (i = 1; i < argv; i++) {
		if (argc[i][0] == '-') {
			//specify options
			if (argc[i][1] == 's') {
				res->step = atoi(argc[++i]);
				continue;
			}

			else if (argc[i][1] == 'h') {
				print_usage();
				return NULL;
			}

			else if (argc[i][1] == 'b') {
				res->board = atoi(argc[++i]); //overwrite default board
				continue;
			}

			else if (argc[i][1] == 'q') {
				struct queuing* queueing_info = (struct queuing*) malloc(sizeof(struct queuing));

				char* queuing_args = argc[++i];
				int arg_nb = 1;
				int args_str_len = 0;
				for ( j = 0 ; queuing_args[j] != '\0' ; j++ ) {
					if (queuing_args[j] == ',') {
						arg_nb++;
					}
					args_str_len++;
				}
				char *queuing_args_local = (char*) malloc(sizeof(char) * args_str_len);
				strcpy(queuing_args_local, queuing_args);
				queueing_info->arg_nb = arg_nb;
				int *args = (int *) malloc( sizeof(int) * arg_nb);

				int index = 0;
				char* tok = strtok(queuing_args_local, ",");
				while (tok != NULL) {
					args[index++] = atoi(tok);
					tok = strtok(NULL, ",");
					if (args[index-1] < 1) {
						print_usage();
						return NULL;
					}
				}
				queueing_info->args = args;
				res->queuing = queueing_info;
				free(queuing_args_local);
			}
			else {
				print_usage();
				return NULL;
			}
		}
		else {
			res->input = argc[i];
		}
	}
	return res;
}

void dump_scop_to_file(osl_scop_p scop, char *filename) {
	FILE *fp;
	clan_options_p options_clan;
	options_clan = clan_options_malloc();
	options_clan->castle = 0;
	options_clan->name = strdup(filename);
	fp = fopen(filename, "w");
	clan_scop_print(fp, scop, options_clan);
	fclose(fp);
	clan_options_free(options_clan);
	return;
}

void dump_deps_to_file(osl_dependence_p dep, char *filename) {
	FILE *candl_output;
	candl_output = fopen(filename, "w");
	candl_dependence_pprint(candl_output, dep);
	fclose(candl_output);
}

void dump_access_to_file(osl_scop_p scop, char *filename) {
	FILE *access_output;
	access_output = fopen(filename, "w");
	my_print_statements_access_from_statement_body(access_output , scop);
	fclose(access_output);
}

//FIXME
// The output dependences keep references to the orig_scop
// that for this reason cant be destroyed
// possibly pass the real scop as argument
// so it can be freed later at the end of the program's execution

//well... actually... that was tried already and it was complaining about
//the precision... otherwise there wouldnt be the need to write an intermediate
//file for passing the around the scop... bah.
osl_dependence_p get_dependency_from_scop_file(char *filename) {
	FILE *candl_input;
	candl_input = fopen(filename, "r");
	osl_dependence_p dep;
	osl_scop_p orig_scop;
	candl_options_p options_candl = candl_options_malloc();
	options_candl->waw = 0;
	options_candl->war = 0;
	osl_interface_p registry = osl_interface_get_default_registry();
	orig_scop = osl_scop_pread(candl_input, registry, 0);
	osl_interface_free(registry);
	candl_scop_usr_init(orig_scop);
	candl_dependence_add_extension(orig_scop, options_candl);
	dep = osl_generic_lookup(orig_scop->extension, OSL_URI_DEPENDENCE);
	fclose(candl_input);
	//osl_scop_free(orig_scop);
	candl_options_free(options_candl);
	return dep;
}

osl_scop_p copy_current_scop(osl_scop_p input_scop) {
	osl_scop_p res;
	res = osl_scop_clone(input_scop);
	res ->next = NULL;
	return res;
}

void parse_input_no_dump(options* opt) {
	clan_options_p options_clan;
	candl_options_p options_candl;

	osl_scop_p scop = NULL;
	osl_scop_p tmp_scop = NULL;
	osl_dependence_p dep;

	FILE *input = NULL;
	int err = 0;
	int i = 0;

	input = fopen(opt->input, "r");

	if (input == NULL) {
		fprintf(stderr, "Error: Unable to open input file %s\n", opt->input);
		return;
	}

	options_clan = clan_options_malloc();
	options_clan->castle = 0;
	options_clan->name = strdup(opt->input);

	options_candl = candl_options_malloc();
	options_candl->waw = 0;
	options_candl->war = 0;

	scop = clan_scop_extract(input, options_clan);
	fclose(input);

	tmp_scop = scop;

	scop_context *tmp_old = NULL;

	while (tmp_scop != NULL) {
		osl_scop_p tmp_scop2;
		tmp_scop2 = copy_current_scop(tmp_scop);
		dump_scop_to_file(tmp_scop2, "tmp.clan.scop");
		dep = get_dependency_from_scop_file("tmp.clan.scop");
		remove("tmp.clan.scop");


		scop_context *tmp = (scop_context*) malloc (sizeof(scop_context));
		tmp->scop = tmp_scop2;
		tmp->path = NULL;
		tmp->dep = dep;
		tmp->id = i;
		tmp->next = NULL;
		tmp->err_id = 0;
		tmp->module_id = 0;
		tmp->dep_edge_removed = NULL;
		tmp->sog_stmts = NULL;
		tmp->graph = NULL;


		if (opt->contexts == NULL)
			opt->contexts = tmp;

		if (tmp_old != NULL)
			tmp_old->next = tmp;

		tmp_old = tmp;

		tmp_scop = tmp_scop->next;
		i++;
	}
}

int parse_input(options* opt) {

	clan_options_p options_clan;
	candl_options_p options_candl;

	osl_scop_p scop = NULL;
	osl_scop_p tmp_scop = NULL;
	osl_dependence_p dep;

	FILE *input = NULL;
	int err = 0;
	int i = 0;

	input = fopen(opt->input, "r");

	if (input == NULL) {
		fprintf(stderr, "Error: Unable to open input file %s\n", opt->input);
		return;
	}

	options_clan = clan_options_malloc();
	options_clan->castle = 0;
	options_clan->name = strdup(opt->input);

	options_candl = candl_options_malloc();
	options_candl->waw = 0;
	options_candl->war = 0;

	scop = clan_scop_extract(input, options_clan);
	fclose(input);

	tmp_scop = scop;


	for (i = 0; tmp_scop != NULL; i++) {
		tmp_scop = tmp_scop->next;
	}


	printf("Found %d scops in input file\n", i);
	if(i == 0){
		return 1;
	}

	tmp_scop = scop;
	i = 0;
	scop_context *tmp_old = NULL;
	while (tmp_scop != NULL) {
		char buf[5];
		//clan_scop_print(stdout, tmp_scop, options_clan);
		char *path = (char *)malloc(sizeof(char) * 10);
		path[0] = '\0';
		strcat(path, "scop_");
		sprintf(buf, "%d", i);
		strcat(path, buf);
		err = mkdir(path, 0755);


		chdir(path);

		if (err != 0)
			printf("Error while creating directory %s\n", path);


		osl_scop_p tmp_scop2;
		tmp_scop2 = copy_current_scop(tmp_scop);
		dump_scop_to_file(tmp_scop2, "tmp.clan.scop");
		dump_access_to_file(tmp_scop2, "tmp.access.dat");
		dep = get_dependency_from_scop_file("tmp.clan.scop");
		dump_deps_to_file(dep, "tmp.candl.dot");


		scop_context *tmp = (scop_context*) malloc (sizeof(scop_context));
		tmp->scop = tmp_scop2;
		tmp->path = strdup (path);
		tmp->dep = dep;
		tmp->id = i;
		tmp->next = NULL;
		tmp->err_id = 0;
		tmp->module_id = 0;
		tmp->dep_edge_removed = NULL;
		tmp->sog_stmts = NULL;
		tmp->graph = NULL;


		if (opt->contexts == NULL)
			opt->contexts = tmp;

		if (tmp_old != NULL)
			tmp_old->next = tmp;

		tmp_old = tmp;

		chdir("..");
		tmp_scop = tmp_scop->next;
		i++;
	}

	return 0;
}
void print_usage() {
	int align = -30;
	printf("Usage: polyfpga [options] file\n");
	printf("Options:\n");
	printf("\t-h %*s %s\n", align, " ", "Display this information.");
	printf("\t-s %*s %s\n", align, "<num>",  "If set to 1 disable user interaction. Default 0.");
	printf("\t-b %*s %s\n", align, "<num>",  "Select board to use, 0 Zybo, 1 V707. Default 0.");
	printf("\t-q %*s %s\n",  align, "<num>(,<num>)*", "Specify queuing number for each SST. Default unset.");
	return;
}

void dump_context (scop_context* cont) {
	while (cont != NULL) {
		printf("ID: %d\n", cont->id);
		printf("scop: %p\n", cont->scop);
		printf("dep: %p\n", cont->dep);
		printf("path: %s\n", cont->path);
		cont = cont->next;
	}
}