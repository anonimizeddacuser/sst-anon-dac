/*
 *   This file is part of PolyFPGA.
 *
 *   PolyFPGA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *   Copyright (C) 2015 Giulio Stramondo
 *
 */

#ifndef STRUCT_SOG_H
#define STRUCT_SOG_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <clan/clan.h>
#include <osl/scop.h>

#include <osl/scop.h>
#include <osl/macros.h>
#include <osl/util.h>
#include <osl/extensions/dependence.h>
#include <osl/extensions/arrays.h>
#include <candl/macros.h>
#include <candl/dependence.h>
#include <candl/violation.h>
#include <candl/options.h>
#include <candl/scop.h>
#include <candl/util.h>
#include <candl/piplib.h>

struct sog_access
{

  int access_ref; // if created by remove_copy_edge is 0
  struct sog_statement *statement;
  char *string;
  int type; //read, write
  osl_relation_p access_relation;
  struct sog_access *next;
};

struct sog_statement {
  struct sog_access* access; // list of access ( NULL TERMINATED )
  int statement_ref;
  osl_relation_p domain;
  osl_relation_p scattering;
  int access_nb; // number of access in the list
  char* body;
  struct sog_statement *next;
};

struct sog_graph {
  struct sog_access *from;
  struct sog_access *to;
  struct sog_graph *next;
};

struct sog_graph_edges_list {
  struct sog_graph *edge;
  struct sog_graph_edges_list *next;
};

struct access_lst {
  struct sog_access* access;
  struct access_lst *next;
};

typedef struct access_lst access_lst;

typedef struct sog_graph_edges_list sog_graph_edges_list;

typedef struct sog_graph sog_graph;

typedef struct sog_statement sog_statement;

typedef struct sog_access sog_access;

#endif