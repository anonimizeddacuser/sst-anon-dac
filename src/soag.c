/*
 *   This file is part of PolyFPGA.
 *
 *   PolyFPGA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * 	 Copyright (C) 2015 Giulio Stramondo
 *
 */
 
#include "soag.h"

void generate_soag_data(options *opt) {

	scop_context *tmp_context = NULL;

	tmp_context = opt->contexts;

	struct soag_SST* tmp_SST = NULL;

	int i = 0;
	while (tmp_context != NULL) {

		if (encountered_error(tmp_context)) {
			tmp_context = tmp_context->next;
			continue;
		}

		chdir(opt->root_directory);
		chdir(tmp_context->path);

		tmp_SST = (struct soag_SST*) malloc (sizeof(struct soag_SST));
		tmp_context->SST = tmp_SST;

		char *tmp =  (char*) malloc(sizeof(char) * 8);
		sprintf(tmp, "SST_%d", tmp_context->id);

		tmp_SST->ID = tmp;
		tmp_SST->num_ID = tmp_context->id;
		
		if (check_dimension_coherency(tmp_context)) {
			set_error(tmp_context, INCOHERENT_DOMAIN_ARRAY, THIS_MOD);
			tmp_context = tmp_context->next;
			continue;
		}


		tmp_SST->stancil_array_id = get_stancil_array_ID(tmp_context);
		tmp_SST-> dim = get_stancil_array_DIM(tmp_context);
		tmp_SST->size_dim = compute_stancil_array_dimensions(tmp_context);

		create_channels(tmp_context);
		create_channel_links(tmp_SST->channels);
		create_channel_ports(tmp_SST->channels);
		create_kernels(tmp_context);
		make_channels_kernels_links(tmp_SST);
		create_demux(tmp_context);
		//dump_SST(tmp_SST);

		FILE *output;
		output = fopen("soag.dot", "w");
		dump_soag_dot(output,tmp_SST);
		fclose (output);

		tmp_context = tmp_context->next;
		i++;
	}
}

void generate_soag_data_no_dump(options *opt) {

	scop_context *tmp_context = NULL;

	tmp_context = opt->contexts;

	struct soag_SST* tmp_SST = NULL;

	int i = 0;
	while (tmp_context != NULL) {

		if (encountered_error(tmp_context)) {
			tmp_context = tmp_context->next;
			continue;
		}

		//chdir(opt->root_directory);
		//chdir(tmp_context->path);

		tmp_SST = (struct soag_SST*) malloc (sizeof(struct soag_SST));
		tmp_context->SST = tmp_SST;

		char *tmp =  (char*) malloc(sizeof(char) * 8);
		sprintf(tmp, "SST_%d", tmp_context->id);

		tmp_SST->ID = tmp;
		tmp_SST->num_ID = tmp_context->id;
		
		if (check_dimension_coherency(tmp_context)) {
			set_error(tmp_context, INCOHERENT_DOMAIN_ARRAY, THIS_MOD);
			return;
		}


		tmp_SST->stancil_array_id = get_stancil_array_ID(tmp_context);
		tmp_SST-> dim = get_stancil_array_DIM(tmp_context);
		tmp_SST->size_dim = compute_stancil_array_dimensions(tmp_context);

		create_channels(tmp_context);
		create_channel_links(tmp_SST->channels);
		create_channel_ports(tmp_SST->channels);
		create_kernels(tmp_context);
		make_channels_kernels_links(tmp_SST);
		create_demux(tmp_context);
		//dump_SST(tmp_SST);

		//FILE *output;
		//output = fopen("soag.dot", "w");
		//dump_soag_dot(output,tmp_SST);
		//fclose (output);

		tmp_context = tmp_context->next;
		i++;
	}
}

void dump_soag_dot(FILE *output, struct soag_SST* SST) {
	struct soag_channel *channels_it = SST->channels;

	int subgraph = 0;
	fprintf(output, "digraph G {\n");


	while (channels_it != NULL) {
		fprintf(output, "\tDMA_%d [label = \"DMA_%d\"];\n", channels_it->ID, channels_it->ID);
		channels_it = channels_it->next;
	}

	fprintf(output, "\tOUTPUT [label = \"OUTPUT\"];\n" );

	channels_it = SST->channels;

	while (channels_it != NULL) {
		fprintf(output, "\tDMA_%d -> dma_port_%d;\n\n", channels_it->ID, channels_it->ID);
		channels_it = channels_it->next;
	}


	fprintf(output, "\tsubgraph cluster_%d {\n\t\tlabel=\"SST\";\n\n", subgraph++);

	for (channels_it = SST->channels;
	        channels_it != NULL;
	        channels_it = channels_it->next) {
		fprintf(output, "\t\tdma_port_%d [label = \"DMA_%d_IN\"];\n", channels_it->ID, channels_it->ID);
	}

	fprintf(output, "\t\tSST_port_out [label = \"SST_OUT\"];\n");

	for (channels_it = SST->channels;
	        channels_it != NULL;
	        channels_it = channels_it->next) {
		fprintf(output, "\t\tdma_port_%d -> filter_%d_%d;\n", channels_it->ID, channels_it->ID, channels_it->attached_to_DMA->ID);
	}

	fprintf(output, "\n");
	for (channels_it = SST->channels;
	        channels_it != NULL;
	        channels_it = channels_it->next) {
		fprintf(output, "\t\tsubgraph cluster_%d {\n\t\t\tlabel=\"Channel_%d\";\n\n", subgraph++, channels_it->ID);

		struct soag_filter *filters_it = channels_it-> filters;
		struct soag_channel_link *links_it = channels_it ->links;


		struct soag_channel_port *ports_it = channels_it->ports;

		while (ports_it != NULL) {
			switch (ports_it->port_type) {
			case FILTER_OUT:
				fprintf(output, "\t\t\tchannel_%d_port_%d [label=\"FILTER_OUT\"];\n", channels_it->ID, ports_it->ID);
				break;
			case BORDER_OUT:
				fprintf(output, "\t\t\tchannel_%d_port_%d [label=\"BORDER_OUT\"];\n", channels_it->ID, ports_it->ID);
				break;
			}
			ports_it = ports_it->next;
		}

		fprintf(output, "\n\n");
		fprintf(output, "\t\t\tsubgraph cluster_%d {\n\t\t\t\tlabel=\"Filters\";\n\n", subgraph++);
		fprintf(output, "\t\t\t\tfilter_%d_%d [label=\"%s\"shape=\"rect\"];\n", channels_it->ID, channels_it->attached_to_DMA->ID, channels_it->attached_to_DMA->access->string);

		while (links_it != NULL) {
			//line 18
			fprintf(output, "\t\t\t\tfilter_%d_%d [label=\"%s\"shape=\"rect\"];\n", channels_it->ID, links_it->to->ID, links_it->to->access->string);
			links_it = links_it -> next;
		}

		fprintf(output, "\n\n");
		links_it = channels_it ->links;
		int count = 0;
		while (links_it != NULL) {
			fprintf(output, "\t\t\t\tfifo_%d [label =\"%d\\n\\n\\n\"shape=\"assembly\"];\n", count, links_it->weight);
			count ++;
			links_it = links_it ->next;
		}
		fprintf(output, "\n\n");

		links_it = channels_it ->links;
		count = 0;
		fprintf(output, "\t\t\t\tfilter_%d_%d -> fifo_%d;\n", channels_it->ID, channels_it->attached_to_DMA->ID, 0);


		while (links_it != NULL) {
			fprintf(output, "\t\t\t\tfifo_%d -> filter_%d_%d;\n", count, channels_it->ID, links_it->to->ID);

			if (links_it->next != NULL) {
				fprintf(output, "\t\t\t\tfilter_%d_%d -> fifo_%d;\n", channels_it->ID, links_it->to->ID, (count + 1 ));

			}

			count ++;
			links_it = links_it ->next;
		}

		fprintf(output, "\t\t\t}\n\n\n");

		ports_it = channels_it->ports;

		while ( ports_it != NULL ) {
			if (ports_it->port_type == DMA_IN) {
				ports_it = ports_it -> next;
				continue;
			}
			fprintf(output, "\t\t\tfilter_%d_%d -> channel_%d_port_%d;\n", channels_it->ID, ports_it -> filter -> ID, channels_it->ID, ports_it->ID );
			ports_it = ports_it -> next;
		}

		fprintf(output, "\t\t}\n\n\n");


	}

	struct soag_channel_port *from = NULL;
	struct soag_kernel_port *to =  NULL;
	struct soag_channel_kernel_link *links_channel_kernel = SST->links_channel_kernel;
	while (links_channel_kernel != NULL) {
		from = links_channel_kernel->from;
		to = links_channel_kernel->to;
		fprintf(output, "\t\tchannel_%d_port_%d -> kernel_%d_port_%d;\n", from->channel->ID, from ->ID, to->kernel->ID, to->ID );
		links_channel_kernel = links_channel_kernel -> next;
	}


	struct soag_kernel *kernels_it = NULL;
	struct soag_kernel_port* kernel_ports_it =  NULL;
	kernels_it = SST->kernels;
	fprintf(output, "\n\n");
	while (kernels_it != NULL) {
		fprintf(output, "\t\tsubgraph cluster_%d {\n\t\t\t\tlabel=\"Kernel_%d\";\n\n", subgraph++, kernels_it->ID );
		fprintf(output, "\t\t\t\tkernel_%d_body [label=\"%s\"shape=\"rect\"];\n", kernels_it->ID, remove_spaces_from_statement ( kernels_it->body));

		kernel_ports_it = kernels_it->ports;
		while (kernel_ports_it != NULL) {
			switch (kernel_ports_it->port_type) {
			case FILTER_IN:
				fprintf(output, "\t\t\t\tkernel_%d_port_%d [label=\"FILTER_IN\"];\n", kernels_it->ID,  kernel_ports_it->ID);
				break;
			case KERNEL_OUT:
				fprintf(output, "\t\t\t\tkernel_%d_port_%d [label=\"KERNEL_OUT\"];\n", kernels_it->ID,  kernel_ports_it->ID);
				break;
			}
			kernel_ports_it = kernel_ports_it->next;
		}
		fprintf(output, "\n\n");
		kernel_ports_it = kernels_it->ports;
		while (kernel_ports_it != NULL) {
			switch (kernel_ports_it->port_type) {
			case FILTER_IN:
				fprintf(output, "\t\t\t\tkernel_%d_port_%d -> kernel_%d_body;\n", kernels_it->ID,  kernel_ports_it->ID, kernels_it->ID);
				break;
			case KERNEL_OUT:
				fprintf(output, "\t\t\t\tkernel_%d_body -> kernel_%d_port_%d;\n", kernels_it->ID, kernels_it->ID,  kernel_ports_it->ID);
				break;
			}
			kernel_ports_it = kernel_ports_it->next;
		}

		fprintf(output, "\n\n\t\t}\n\n");

		kernels_it = kernels_it ->next;
	}

	struct soag_demux* demux = NULL;
	struct soag_demux_port *demux_port = NULL;

	demux = SST->demux;
	fprintf(output, "\t\tsubgraph cluster_%d {\n\t\t\tlabel=\"Demux\";\n\n", subgraph++ );
	fprintf(output, "\t\t\tdemux [label=\"Demux\"shape=\"rect\"];\n\n");

	demux_port = demux->ports;
	while (demux_port != NULL) {
		switch (demux_port->port_type) {
		case KERNEL_IN:
			fprintf(output, "\t\t\tdemux_port_%d [label=\"KERNEL_IN\"];\n", demux_port->ID );
			break;
		case BORDER_IN:
			fprintf(output, "\t\t\tdemux_port_%d [label=\"BORDER_IN\"];\n", demux_port->ID );
			break;
		case DEMUX_OUT:
			fprintf(output, "\t\t\tdemux_port_%d [label=\"DEMUX_OUT\"];\n", demux_port->ID );
			break;
		}
		demux_port = demux_port ->next;
	}

		fprintf(output, "\n\n");

	demux_port = demux->ports;
	while (demux_port != NULL) {
		switch (demux_port->port_type) {
		case KERNEL_IN:
			fprintf(output, "\t\t\tdemux_port_%d->demux;\n", demux_port->ID );
			break;
		case BORDER_IN:
			fprintf(output, "\t\t\tdemux_port_%d->demux;\n", demux_port->ID );
			break;
		case DEMUX_OUT:
			fprintf(output, "\t\t\tdemux->demux_port_%d;\n", demux_port->ID );
			break;
		}
		demux_port = demux_port ->next;
	}

		fprintf(output, "\n");

	struct soag_kernel_demux_link* kernel_demux_links = NULL;
	struct soag_kernel_port *from1 = NULL;
	struct soag_demux_port *to1 = NULL;

	kernel_demux_links = SST->links_kernel_demux;
	while(kernel_demux_links != NULL){
		from1 = kernel_demux_links->from;
		to1 = kernel_demux_links->to;

		fprintf(output, "\t\t\tkernel_%d_port_%d -> demux_port_%d;\n", from1->kernel->ID, from1->ID, to1->ID);
		kernel_demux_links = kernel_demux_links->next;
	}

	struct soag_filter_demux_link *filter_demux_link = NULL;
	//I think that having more than one link between filters and demux is impossible
	// so no while loop
	filter_demux_link = SST->links_channel_demux;
	fprintf(output, "\t\t\tchannel_%d_port_%d -> demux_port_%d;\n", filter_demux_link->from->channel->ID , filter_demux_link->from->ID, filter_demux_link->to->ID);
	
		fprintf(output, "\n\n\t\t}\n\n");


		demux_port = demux->ports;
	while (demux_port != NULL) {
		switch (demux_port->port_type) {
		case DEMUX_OUT:
			fprintf(output, "\t\tdemux_port_%d->SST_port_out;\n", demux_port->ID );
			break;
		}
		demux_port = demux_port ->next;
	}
		fprintf(output, "\n\t}\n\n");
	fprintf(output, "\tSST_port_out->OUTPUT;");
	fprintf(output, "\n}\n\n");
	return ;
}

void create_demux(scop_context *tmp_context) {
	struct soag_SST *sst = tmp_context->SST;
	struct soag_demux *tmp_demux = NULL;
	tmp_demux = (struct soag_demux *)malloc(sizeof(struct soag_demux));

	tmp_demux->SST = sst;
	//TO set the last =1 the last visited element is going to be the
	//one in sst->size_dim

	struct soag_kernel *kernel_it = sst->kernels;
	struct soag_channel *channels_it = sst->channels;

	struct soag_kernel_port *kernel_ports_it = NULL;
	struct soag_channel_port *channel_ports_it = NULL;

	struct soag_demux_port *ports_list = NULL;
	struct soag_demux_port *ports_tmp = NULL;
	struct soag_demux_port *ports_last = NULL;

	struct soag_kernel_demux_link *links_kernel_demux_list = NULL;
	struct soag_kernel_demux_link *links_kernel_demux_tmp = NULL;
	struct soag_kernel_demux_link *links_kernel_demux_last = NULL;

	int demux_port_id = 0;
	while (kernel_it != NULL ) {
		kernel_ports_it = kernel_it->ports;
		while (kernel_ports_it != NULL) {
			if (kernel_ports_it->port_type == KERNEL_OUT) {
				ports_tmp = (struct soag_demux_port *) malloc(sizeof(struct soag_demux_port));


				ports_tmp -> port_type = KERNEL_IN;
				ports_tmp -> ID = demux_port_id;
				ports_tmp -> demux = tmp_demux;
				ports_tmp -> next = NULL;

				//TODO create function to compute the port condition
				//link ports_tmp ->condition to the output

				demux_port_id++;

				if (ports_list == NULL) {
					ports_list = ports_tmp;
				}
				if (ports_last  == NULL) {
					ports_last = ports_tmp;
				} else {
					ports_last->next = ports_tmp;
					ports_last = ports_tmp;
				}


				links_kernel_demux_tmp = (struct soag_kernel_demux_link *) malloc(sizeof(struct soag_kernel_demux_link));
				links_kernel_demux_tmp->from = kernel_ports_it;
				links_kernel_demux_tmp->to = ports_tmp;
				links_kernel_demux_tmp->next = NULL;

				if (links_kernel_demux_list == NULL ) {
					links_kernel_demux_list = links_kernel_demux_tmp;
				}
				if (links_kernel_demux_last == NULL) {
					links_kernel_demux_last = links_kernel_demux_tmp;
				} else {
					links_kernel_demux_last->next = links_kernel_demux_tmp;
					links_kernel_demux_last = links_kernel_demux_tmp;
				}

			}

			kernel_ports_it = kernel_ports_it->next;
		}
		kernel_it = kernel_it->next;
	}
	sst ->links_kernel_demux = links_kernel_demux_list;

	int stancil_array_id = sst->stancil_array_id;
	struct soag_filter_demux_link *tmp_border_link = NULL;
	channels_it = sst->channels;
	while (channels_it != NULL) {
		if (channels_it->ID != stancil_array_id) {
			channels_it = channels_it->next;
			continue;
		}

		channel_ports_it = channels_it->ports;
		while (channel_ports_it != NULL) {

			if (channel_ports_it->port_type == BORDER_OUT) {
				ports_tmp = (struct soag_demux_port *) malloc(sizeof(struct soag_demux_port));
				ports_tmp -> port_type = BORDER_IN;
				ports_tmp -> ID = demux_port_id;
				ports_tmp -> demux = tmp_demux;
				ports_tmp -> next = NULL;

				//TODO create function to compute the port condition
				//link ports_tmp ->condition to the output

				demux_port_id++;

				if (ports_list == NULL) {
					ports_list = ports_tmp;
				}
				if (ports_last  == NULL) {
					ports_last = ports_tmp;
				} else {
					ports_last->next = ports_tmp;
					ports_last = ports_tmp;
				}


				tmp_border_link = (struct soag_filter_demux_link *) malloc(sizeof(struct soag_filter_demux_link));
				tmp_border_link->from = channel_ports_it;
				tmp_border_link->to = ports_tmp;
				tmp_border_link->next = NULL;
				sst->links_channel_demux = tmp_border_link;
				break;
			}
			channel_ports_it = channel_ports_it->next;
		}
		channels_it = channels_it->next;

	}
	ports_tmp = (struct soag_demux_port *) malloc(sizeof(struct soag_demux_port));
	ports_tmp -> port_type = DEMUX_OUT;
	ports_tmp -> ID = demux_port_id;
	ports_tmp -> demux = tmp_demux;
	ports_tmp -> next = NULL;

	//TODO create function to compute the port condition
	//link ports_tmp ->condition to the output

	demux_port_id++;

	if (ports_list == NULL) {
		ports_list = ports_tmp;
	}
	if (ports_last  == NULL) {
		ports_last = ports_tmp;
	} else {
		ports_last->next = ports_tmp;
		ports_last = ports_tmp;
	}

	tmp_demux->ports = ports_list;
	sst->demux = tmp_demux;
	return;
}


void make_channels_kernels_links(struct soag_SST* SST) {
	struct soag_kernel *kernels = SST->kernels;
	struct soag_kernel_port *ports = NULL;
	struct soag_channel_port *channel_port = NULL;

	struct sog_access *tmp_access = NULL;
	struct soag_channel_kernel_link *links_lst = NULL;
	struct soag_channel_kernel_link *links_last = NULL;
	struct soag_channel_kernel_link *links_tmp = NULL;

	while (kernels != NULL) {
		ports = kernels->ports;

		while (ports != NULL) {
			tmp_access = ports->access;
			channel_port = get_channel_port(FILTER_OUT, tmp_access, SST->channels);

			if (channel_port == NULL) {
				ports = ports->next;
				continue;
			}

			links_tmp = (struct soag_channel_kernel_link *) malloc (sizeof(struct soag_channel_kernel_link));
			links_tmp -> from = channel_port;
			links_tmp -> to = ports;
			links_tmp -> next = NULL;

			if (links_lst == NULL) {
				links_lst = links_tmp ;
			}
			if (links_last == NULL) {
				links_last = links_tmp;
			} else {
				links_last->next = links_tmp;
				links_last = links_tmp;
			}

			ports = ports->next;
		}

		kernels = kernels -> next;
	}

	SST -> links_channel_kernel = links_lst;
}

struct soag_filter *get_filter(struct sog_access *access, struct soag_channel *channels) {
	struct soag_channel *channels_it = channels;
	struct soag_filter *filter_it = NULL;

	while (channels_it != NULL) {
		filter_it = channels_it->filters;
		while (filter_it != NULL) {
			if (access == filter_it->access)
				return filter_it;
			filter_it = filter_it->next;
		}
		channels_it = channels_it->next;
	}

	return NULL;
}

struct soag_channel_port *get_channel_port(int port_type, struct sog_access *access, struct soag_channel *channels) {


	struct soag_channel *channels_it = channels;
	struct soag_filter *filter_it = NULL;

	struct soag_channel_port *port_it = NULL;

	while (channels_it != NULL) {
		port_it = channels_it->ports;
		while (port_it != NULL) {
			if (port_it->port_type == port_type
			        && port_it->filter->access == access)
				return port_it;
			port_it = port_it -> next;
		}

		channels_it = channels_it->next;
	}


	return NULL;
}

void create_channel_ports(struct soag_channel *channels) {
	struct soag_channel *channels_it = channels;

	struct soag_channel_port *ports_lst = NULL;
	struct soag_channel_port *ports_last = NULL;
	struct soag_channel_port *ports_tmp = NULL;

	int port_nb = 0;
	while (channels_it != NULL) {
		ports_tmp = (struct soag_channel_port *) malloc (sizeof(struct soag_channel_port));

		ports_tmp -> port_type = DMA_IN;
		ports_tmp -> filter = channels_it -> attached_to_DMA;
		ports_tmp -> channel = channels_it;
		ports_tmp->ID = port_nb++;
		ports_tmp -> next = NULL;
		if (ports_lst == NULL)
			ports_lst = ports_tmp;
		if (ports_last == NULL)
			ports_last = ports_tmp;
		else {
			ports_last->next = ports_tmp;
			ports_last = ports_tmp;
		}

		struct soag_filter *filters_it = channels_it ->filters;
		while (filters_it != NULL) {
			ports_tmp = (struct soag_channel_port *) malloc (sizeof(struct soag_channel_port));
			ports_tmp -> port_type = FILTER_OUT;
			ports_tmp -> filter = filters_it;
			ports_tmp -> channel = channels_it;
			ports_tmp -> next = NULL;
			ports_tmp->ID = port_nb++;
			if (ports_lst == NULL)
				ports_lst = ports_tmp;
			if (ports_last == NULL)
				ports_last = ports_tmp;
			else {
				ports_last->next = ports_tmp;
				ports_last = ports_tmp;
			}

			if (filters_it->center) {
				ports_tmp = (struct soag_channel_port *) malloc (sizeof(struct soag_channel_port));
				ports_tmp -> port_type = BORDER_OUT;
				ports_tmp -> filter = filters_it;
				ports_tmp -> channel = channels_it;
				ports_tmp->ID = port_nb++;
				ports_tmp -> next = NULL;
				if (ports_lst == NULL)
					ports_lst = ports_tmp;
				if (ports_last == NULL)
					ports_last = ports_tmp;
				else {
					ports_last->next = ports_tmp;
					ports_last = ports_tmp;

				}
			}
			filters_it = filters_it->next;

		}
		channels_it->ports = ports_lst;
		channels_it = channels_it->next;
	}
}

//The dimension of the stancil array taken from the scop access
//is coherent with the dimension of statement iteration domain minus 1
//(the extra dimension is the time)
//return 0 means coherency
int check_dimension_coherency(scop_context * tmp_context) {
	int dim_from_access = get_stancil_array_DIM(tmp_context);

	osl_scop_p tmp_scop = tmp_context->scop;
	osl_statement_p statements = tmp_scop->statement;

	//take the last statement
	if (statements == NULL) return -1;

	while (statements->next != NULL)
		statements = statements->next;

	int dim_from_domain = statements->domain->nb_output_dims;
	dim_from_domain--;

	if (dim_from_domain == dim_from_access) return 0;
	else return 1;
}

//The stancil array is the array where the last statement writes data
int get_stancil_array_ID(scop_context * tmp_context) {
	osl_scop_p tmp_scop = tmp_context->scop;
	osl_statement_p statements = tmp_scop->statement;

	//take the last statement
	if (statements == NULL) return -1;

	while (statements->next != NULL)
		statements = statements->next;

	//take the first access of the last statement (the write one)
	osl_relation_list_p access = statements->access;
	osl_relation_p wite_access = access->elt;
	int stancil_array_ID = get_array_id_from_osl_relation(wite_access, tmp_scop);
	return stancil_array_ID;
}

int get_stancil_array_DIM(scop_context * tmp_context) {
	osl_relation_list_p result = NULL;
	result = select_access_to_stancil_array(tmp_context);
	int dim = result->elt->nb_output_dims;
	dim--;
	return  dim;
}

//Takes the scop from the scop context and all the access ( read or write )
//done to the stencil array of the scop context
//and returns a list of all those access ( used later for computing
// the stancil array domain)
osl_relation_list_p select_access_to_stancil_array(scop_context * tmp_context) {
	osl_relation_list_p result = NULL;
	osl_relation_list_p current = NULL;
	osl_relation_list_p new_element = NULL;

	osl_scop_p tmp_scop = tmp_context->scop;
	osl_statement_p statements = tmp_scop->statement;

	int stancil_array_ID = get_stancil_array_ID(tmp_context);

	while (statements != NULL) {
		osl_relation_list_p stmt_acc_list = statements-> access;

		while (stmt_acc_list != NULL) {
			osl_relation_p tmp_acc = stmt_acc_list->elt;
			int array_ID = get_array_id_from_osl_relation(tmp_acc, tmp_scop);
			if (array_ID == stancil_array_ID) {
				new_element = osl_relation_list_malloc();
				new_element->elt = tmp_acc;
				if (result == NULL) {
					result = new_element;
					current = new_element;
				}
				else {
					current->next = new_element;
					current = new_element;
				}
			}
			stmt_acc_list = stmt_acc_list->next;
		}
		statements = statements->next;
	}
	return result;
}

int count_relation_list(osl_relation_list_p list) {
	int i = 0;
	while (list != NULL) {
		i++;
		list = list->next;
	}
	return i;
}

//Parses the output of a lexmax query and returns an
// integer array containing the lexmax for each dimentions
int *compute_stancil_array_dimensions_parse_res(char* iscc_res) {
	int dimensions = 1;
	char *tmp = iscc_res;

	while (*tmp != '\0') {
		if (*tmp == ',') dimensions++;
		tmp++;
	}
	int *res = (int *)malloc (sizeof(int) * dimensions);

	int j = 0;
	for (j = 0; j < dimensions; j++)
		res[j] = 0;

	tmp = iscc_res;
	int temp_res = 0;

	int state = 0;
	int i = 0;
	while (*tmp != '}') {
		switch (state) {
		case 0:
			if (*tmp >= '0' && *tmp <= '9') {
				state = 1;
				temp_res = *tmp - '0';
			}
			break;
		case 1:
			if (*tmp >= '0' && *tmp <= '9') {
				temp_res = (temp_res * 10) + (*tmp - '0');
			}
			else {
				state = 2;
				res[i] = temp_res;
				i++;
			}
			break;
		case 2:
			if (*tmp == ' ') {
				state = 0;
			}
			break;
		}

		tmp++;
	}
	return res;
}

int *compute_filter_lexmin(scop_context * tmp_context, struct sog_access * access) {
	char *domain = NULL;
	char *time_dim_map = NULL;

	char query [2000];
	query[0] = '\0';

	osl_relation_p domain_relation;
	osl_scop_p tmp_scop = tmp_context->scop;
	osl_statement_p statements = tmp_scop->statement;

	while (statements->next != NULL)
		statements = statements->next;

	int dimensions = get_stancil_array_DIM(tmp_context);

	int *res = (int *) malloc (sizeof(int) * dimensions);

	int j = 0;
	for (j = 0; j < dimensions; j++)
		res[j] = 0;

	domain_relation = statements->domain;
	domain = get_domain_barvinok(domain_relation);


	char* tmp_access_str = NULL;
	osl_relation_p tmp_access = access->access_relation;
	int i = 0;



	tmp_access_str = get_access_map_barvinok(domain_relation, tmp_access);

	query[0] = '\0';

	strcat(query, "D1 :=");
	strcat(query, domain);
	strcat(query, ";\n");

	strcat(query, "TMPACCESS :=");
	strcat(query, tmp_access_str);
	strcat(query, ";\n");

	strcat(query, "lexmin TMPACCESS(D1);\n");

	//printf("%s\n", query);
	char *result = NULL;
	//printf("QUERY:\n%s\n", query );
	result = query_barvinok(query);
	//printf("result : %s\n", result);
	res = compute_stancil_array_dimensions_parse_res(result);


	return res;
}

int* compute_stancil_array_dimensions(scop_context * tmp_context) {
	char *domain = NULL;
	char *time_dim_map = NULL;

	char query [2000];
	query[0] = '\0';

	osl_relation_p domain_relation;
	osl_scop_p tmp_scop = tmp_context->scop;
	osl_statement_p statements = tmp_scop->statement;

	while (statements->next != NULL)
		statements = statements->next;

	int dimensions = get_stancil_array_DIM(tmp_context);

	int *res = (int *) malloc (sizeof(int) * dimensions);

	int j = 0;
	for (j = 0; j < dimensions; j++)
		res[j] = 0;

	domain_relation = statements->domain;
	domain = get_domain_barvinok(domain_relation);



	time_dim_map = map_to_project_away_time_dim(domain_relation);

	osl_relation_list_p access = select_access_to_stancil_array(tmp_context);
	char* tmp_access_str = NULL;
	osl_relation_p tmp_access;
	int i = 0;
	int* tmpres;
	while (access != NULL) {
		tmp_access = access->elt;
		tmp_access_str = get_access_map_barvinok(domain_relation, tmp_access);

		query[0] = '\0';

		strcat(query, "D1 :=");
		strcat(query, domain);
		strcat(query, ";\n");

		strcat(query, "TMPACCESS :=");
		strcat(query, tmp_access_str);
		strcat(query, ";\n");

		strcat(query, "lexmax TMPACCESS(D1);\n");

		//printf("%s\n", query);
		char *result = NULL;
		//printf("QUERY:\n%s\n", query );
		result = query_barvinok(query);
		//printf("result : %s\n", result);
		tmpres = compute_stancil_array_dimensions_parse_res(result);

		for (j = 0; j < dimensions; j++)
			if (tmpres[j] > res[j])
				res[j] = tmpres[j];

		access = access->next;
	}
	return res;
}

struct soag_channel *get_channel_with_id(struct soag_channel * channels, int ID) {
	while (channels != NULL) {
		if (channels->ID == ID)
			return channels;
		channels = channels->next;
	}
	return NULL;
}

int has_channel_with_id(struct soag_channel * channels, int ID) {
	if (get_channel_with_id(channels, ID) == NULL)
		return 0;
	else
		return 1;
}

struct soag_kernel *create_kernels(scop_context * tmp_context) {
	struct soag_kernel *kernels_lst = NULL;
	struct soag_kernel *kernels_lst_last = NULL;
	struct soag_kernel *tmp_kernel = NULL;



	struct soag_kernel_port *ports_lst = NULL;
	struct soag_kernel_port *ports_last = NULL;
	struct soag_kernel_port *ports_tmp = NULL;

	sog_graph *sog = tmp_context->graph;
	access_lst *write_acc = get_write_access_from_sog_graph(sog);
	osl_relation_p acc_relation = NULL;
	int tmp_id = 0;

	access_lst *write_acc_it = write_acc;
	int id = 0;
	while (write_acc_it != NULL) {
		tmp_kernel = create_kernels_helper(id, write_acc_it->access, tmp_context);

		if (kernels_lst == NULL) {
			kernels_lst = tmp_kernel;
		}

		if (kernels_lst_last == NULL) {
			kernels_lst_last = tmp_kernel;
		}
		else {
			kernels_lst_last->next = tmp_kernel;
			kernels_lst_last = tmp_kernel;
		}

		ports_lst = NULL;
		ports_last = NULL;
		ports_tmp = NULL;
		int port_id = 0;
		sog_graph_edges_list *links_in = NULL;
		links_in = get_edges_arriving_to_sog_access(sog, write_acc_it->access);

		while (links_in != NULL) {
			ports_tmp = (struct soag_kernel_port *)malloc(sizeof(struct soag_kernel_port ));

			ports_tmp->kernel = tmp_kernel;
			ports_tmp->port_type = FILTER_IN;
			ports_tmp ->access = links_in->edge->from;
			ports_tmp -> ID = port_id++;
			ports_tmp ->next = NULL;

			if (ports_lst == NULL) {
				ports_lst = ports_tmp;
			}
			if (ports_last == NULL) {
				ports_last = ports_tmp;
			} else {
				ports_last->next = ports_tmp;
				ports_last = ports_tmp;
			}

			links_in = links_in->next;
		}
		ports_tmp = (struct soag_kernel_port *)malloc(sizeof(struct soag_kernel_port ));
		ports_tmp->kernel = tmp_kernel;
		ports_tmp->port_type = KERNEL_OUT;
		ports_tmp ->access = write_acc_it->access;
		ports_tmp -> ID = port_id++;
		ports_tmp ->next = NULL;

		if (ports_lst == NULL) {
			ports_lst = ports_tmp;
		}
		if (ports_last == NULL) {
			ports_last = ports_tmp;
		} else {
			ports_last->next = ports_tmp;
			ports_last = ports_tmp;
		}



		tmp_kernel -> ports = ports_lst;
		write_acc_it = write_acc_it ->next;
	}





	tmp_context ->SST -> kernels = kernels_lst;
	return;
}

void create_channels(scop_context * tmp_context) {
	struct soag_channel* channels_lst = NULL;
	struct soag_channel* channels_lst_last = NULL;
	struct soag_channel* tmp_channel = NULL;

	sog_graph *sog = tmp_context->graph;
	access_lst *read_acc = get_read_access_from_sog_graph(sog);
	osl_relation_p acc_relation = NULL;
	int tmp_id = 0;

	access_lst *read_acc_it = read_acc;

	//creates one channel for each array accessed
	while (read_acc_it != NULL) {
		acc_relation = read_acc_it->access->access_relation;
		tmp_id = get_array_id_from_osl_relation(acc_relation, tmp_context->scop);
		if (!has_channel_with_id(channels_lst, tmp_id)) {
			tmp_channel = (struct soag_channel*) malloc (sizeof(struct soag_channel));
			tmp_channel->next = NULL;
			tmp_channel->filters = NULL;
			tmp_channel->ports = NULL;
			tmp_channel->links = NULL;
			tmp_channel->attached_to_DMA = NULL;
			tmp_channel->ID = tmp_id;
			tmp_channel->SST = tmp_context-> SST;

			if (channels_lst == NULL) {
				channels_lst = tmp_channel;
			}

			if (channels_lst_last == NULL) {
				channels_lst_last = tmp_channel;
			} else {
				channels_lst_last->next = tmp_channel;
				channels_lst_last = tmp_channel;
			}

		}
		read_acc_it = read_acc_it->next;
	}


	tmp_context->SST->channels = channels_lst;

	//Populates the channel creating the filters from the read
	//access

	read_acc_it = read_acc;
	struct soag_filter *filter_it = NULL;
	struct soag_filter *tmp_filter = NULL;
	int next_filter_ID = 0;
	while (read_acc_it != NULL) {
		acc_relation = read_acc_it->access->access_relation;
		tmp_id = get_array_id_from_osl_relation(acc_relation, tmp_context->scop);
		tmp_channel = get_channel_with_id(channels_lst, tmp_id);
		filter_it = tmp_channel -> filters;

		tmp_filter = create_filter(next_filter_ID, read_acc_it->access, tmp_context);
		next_filter_ID++;
		tmp_filter->channel = tmp_channel;
		if (filter_it != NULL) {
			while (filter_it->next != NULL)
				filter_it = filter_it->next;
			filter_it->next = tmp_filter;
		}
		else {
			tmp_channel -> filters = tmp_filter;
		}

		read_acc_it = read_acc_it->next;
	}

	return;
}

void create_channel_links(struct soag_channel * channel) {
	struct soag_filter *filters;
	struct soag_filter *filters_it;
	struct soag_filter *filters_max;
	filters = channel->filters;

	int count_links=0;
	//find maximum filter ( ordering using lexmins )
	filters_max = filters;
	filters_it = filters->next;
	int count = 0 ;
	while (filters_it != NULL) {
		if (cmp_GT_filters(filters_it, filters_max))
			filters_max = filters_it;
		filters_it = filters_it->next;
		count ++;
	}

	channel->attached_to_DMA = filters_max;
	struct soag_channel_link *links = NULL;
	struct soag_channel_link *links_lst = NULL;
	struct soag_channel_link *link_tmp = NULL;

	int control_flag = 1;

	struct soag_filter *next_filter = filters_max;

//gdb debugging
// printf "next_filter: %s\nfilters_max: %s\n", next_filter->access->string, filters_max->access->string
//printf "%s -%d-> %s",link_tmp->from->access->string,link_tmp->weight, link_tmp->to->access->string
	while (count_links < count) {
		filters_max = NULL;
		filters_it = filters;
		control_flag = 0;


		// if(filters_max == next_filter || cmp_GT_filters(filters_max, next_filterzx)){
		// 	filters_max=filters->next;
		// }

		while (filters_it != NULL) {
			if ((filters_max == NULL || cmp_GT_filters(filters_it, filters_max))
			        && cmp_GT_filters(next_filter, filters_it)) {
				filters_max = filters_it;
			}
			filters_it = filters_it->next;
		}

		
			control_flag = 1;
			count_links++;
			link_tmp = (struct soag_channel_link *)malloc (sizeof(struct soag_channel_link));
			link_tmp ->from = next_filter;
			link_tmp ->to = filters_max;
			link_tmp ->next = NULL;
			//TODO compute minimum fifo dimension and assign it to weight
			link_tmp-> weight = compute_fifo_size(next_filter, filters_max);

			if (links == NULL) {
				links = link_tmp;
			}

			if (links_lst == NULL) {
				links_lst = link_tmp;
			}
			else {
				links_lst->next = link_tmp;
				links_lst = link_tmp;
			}

			next_filter = filters_max;



		channel -> links = links;

	}

}



int compute_fifo_size(struct soag_filter * higher, struct soag_filter * lower) {
	struct soag_channel *channel = higher->channel;
	struct soag_SST *sst = channel->SST;

	return compute_fifo_size_helper(sst->dim, sst->size_dim, lower->access_lexmin, higher->access_lexmin);
}

int compute_fifo_size_helper(int dims, int *dim, int *it_1, int *it_2) {
	int i, j;
	int sum_acc = 0;
	int prod_acc = 1;

	for ( i = 0 ; i <  dims  ; i++) {
		for (j = i + 1; j < dims; j++)
			prod_acc *= dim[j];

		sum_acc += (it_2[i] - it_1[i]) * prod_acc;


		prod_acc = 1;
	}
	return sum_acc;
}

struct soag_filter *create_filter(int filter_ID, struct sog_access * access, scop_context * tmp_context) {
	struct soag_filter *result = NULL;
	result = (struct soag_filter*) malloc (sizeof(struct soag_filter));
	result->ID = filter_ID;
	result->next = NULL;
	result->access = access;
	result->array_id = get_array_id_from_osl_relation(access->access_relation, tmp_context->scop);
	result->access_lexmin = compute_filter_lexmin(tmp_context, access);
	int *lexmins = result->access_lexmin;
	result->center = 1;
	int i = 0;
	while (i < tmp_context->SST->dim) {
		if (lexmins[i] != 1)
			result->center = 0;
		i++;
	}
	result->channel = NULL;
	return result;

}


struct soag_kernel *create_kernels_helper(int kernel_ID, struct sog_access * access, scop_context * tmp_context) {

	struct soag_kernel *result = NULL;
	result = (struct soag_kernel*) malloc (sizeof(struct soag_kernel));
	result->ID  = kernel_ID;
	result->next = NULL;
	result->SST = tmp_context->SST;
	result->access = access;
	result ->body = access->statement->body;
	result->ports = NULL;

	return result;
}


int cmp_GT_filters(struct soag_filter * A, struct soag_filter * B) {
	int dim1 = A->channel->SST->dim;
	int dim2 = B->channel->SST->dim;

	//if they are not comparable
	//because the number of dimensions is different
	//then return false
	if (dim1 != dim2)
		return 0;

	int i = 0;
	int *A_lexmins = A->access_lexmin;
	int *B_lexmins = B->access_lexmin;

	for (i = 0; i < dim1; i++) {
		if (A_lexmins[i] > B_lexmins[i])
			return 1;
		if (A_lexmins[i] < B_lexmins[i])
			return 0;
	}

	//if they are equal return 0
	if (A_lexmins[dim1 - 1] == B_lexmins[dim1 - 1])
		return 0;


	return 1;
}


void dump_SST (struct soag_SST * SST) {
	printf("%s\n", SST ->ID );
	printf("Stancil Array ID: %d\n", SST->stancil_array_id);
	printf("Dimentions: %d\n", SST->dim );
	int i = 0;
	int *tmp_dim = NULL;
	tmp_dim = SST->size_dim;
	for (i = 0 ; i < SST->dim; i++ )
		printf("size dimension %d : %d\n", i, tmp_dim[i]);
}