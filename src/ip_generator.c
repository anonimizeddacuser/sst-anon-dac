/*
 *   This file is part of PolyFPGA.
 *
 *   PolyFPGA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * 	 Copyright (C) 2015 Giulio Stramondo
 *
 */
 
#include "ip_generator.h"


char *get_board_part_name(int BOARD_ID) {
	switch (BOARD_ID) {
	case ZYBO: return "xc7z010clg400-1";
	case VIRTEX7: return "xc7vx485tffg1761-2";
	}
}

char *get_hls_command(int BOARD_ID) {
	switch (BOARD_ID) {
	case ZYBO: return "/opt/clantool/src/generate_ip.sh hls_sources xc7z010clg400-1 10 > ip_synth.log";
	case VIRTEX7: return "/opt/clantool/src/generate_ip.sh hls_sources xc7vx485tffg1761-2 5 > ip_synth.log";
	}
}

int get_closest_FIFO_size(int desidered_size){
	int available_fifo_size[]={16,32,64,128,256,512,1024,2048,4096,8192,16384,32768};
	int i;
	for (i = 0; i<12 ; i++){
		if(desidered_size<=available_fifo_size[i])
			return available_fifo_size[i];
	}
	//TODO signal as an error that this would require a too large fifo
	//or handle putting more fifos in chain
	return -1;
}
void generate_vivado_tcl(options* opt, int BOARD_ID) {
	FILE *fp;


	scop_context * tmp_context = NULL;

	tmp_context = opt->contexts;

	struct soag_SST* tmp_SST = NULL;
	struct soag_channel *channels_it = NULL;
	struct soag_filter *filters_it = NULL;
	struct soag_channel_link *links_it = NULL;
	struct soag_kernel *kernels_it = NULL;
	struct soag_channel_kernel_link *links_channel_kernel = NULL;
	struct soag_kernel_demux_link *links_kernel_demux = NULL;
	int register_slice_ID = 0;
	int fifo_ID = 0;
	int i = 0;
	while (tmp_context != NULL) {

		if (encountered_error(tmp_context)) {
			tmp_context = tmp_context->next;
			continue;
		}

		chdir(opt->root_directory);
		chdir(tmp_context->path);
		tmp_SST = tmp_context->SST;

		fp = fopen ("vivado_sst_ip.tcl", "w");
		fprintf(fp, "create_project SST_%d_IP SST_%d_IP -part %s\n", tmp_context->id, tmp_context->id, get_board_part_name(BOARD_ID));
		fprintf(fp, "create_bd_design \"SST_%d_design\"\n", tmp_context->id);
		fprintf(fp, "set_property ip_repo_paths IP [current_project]\n");
		fprintf(fp, "update_ip_catalog\n");
		fprintf(fp, "startgroup\n");

		channels_it = tmp_SST->channels;
		while (channels_it != NULL) {
			channels_it-> filters;
			links_it = channels_it ->links;
			fprintf(fp, "create_bd_cell -type ip -vlnv xilinx.com:hls:filter_%d_%d_%d:1.0 filter_%d_%d_%d\n", tmp_SST->num_ID,channels_it->ID, channels_it->attached_to_DMA->ID, tmp_SST->num_ID,channels_it->ID, channels_it->attached_to_DMA->ID);

			while (links_it != NULL) {
				fprintf(fp, "create_bd_cell -type ip -vlnv xilinx.com:hls:filter_%d_%d_%d:1.0 filter_%d_%d_%d\n", tmp_SST->num_ID,channels_it->ID, links_it->to->ID, tmp_SST->num_ID,channels_it->ID, links_it->to->ID);
				links_it = links_it -> next;
			}

			channels_it = channels_it->next;
		}

		kernels_it = tmp_SST ->kernels;
		while (kernels_it != NULL) {
			fprintf(fp, "create_bd_cell -type ip -vlnv xilinx.com:hls:kernel_%d_%d:1.0 kernel_%d_%d\n", tmp_SST->num_ID, kernels_it->ID, tmp_SST->num_ID,kernels_it->ID);
			kernels_it = kernels_it->next;
		}

		fprintf(fp, "create_bd_cell -type ip -vlnv xilinx.com:hls:demux_%d:1.0 demux_%d\n",tmp_SST->num_ID,tmp_SST->num_ID);
		fprintf(fp, "endgroup\nstartgroup\n");

		channels_it = tmp_SST->channels;
		fprintf(fp, "create_bd_port -dir I -type clk ap_clk\n");
		fprintf(fp, "create_bd_port -dir I -type rst ap_rst_n\n");

		while (channels_it != NULL) {
			channels_it-> filters;

			fprintf(fp, "create_bd_intf_port -mode Slave -vlnv xilinx.com:interface:axis_rtl:1.0 s_axis_dma_%d_in\n", channels_it->ID);
			fprintf(fp, "set_property -dict [list CONFIG.TDATA_NUM_BYTES [get_property CONFIG.TDATA_NUM_BYTES [get_bd_intf_pins filter_%d_%d_%d/s_axis_dma_%d_in]] CONFIG.HAS_TLAST [get_property CONFIG.HAS_TLAST [get_bd_intf_pins filter_%d_%d_%d/s_axis_dma_%d_in]] CONFIG.LAYERED_METADATA [get_property CONFIG.LAYERED_METADATA [get_bd_intf_pins filter_%d_%d_%d/s_axis_dma_%d_in]]] [get_bd_intf_ports s_axis_dma_%d_in]\n", tmp_SST->num_ID, channels_it->ID, channels_it->attached_to_DMA->ID,channels_it->ID, tmp_SST->num_ID,channels_it->ID, channels_it->attached_to_DMA->ID, channels_it->ID, tmp_SST->num_ID, channels_it->ID, channels_it->attached_to_DMA->ID, channels_it->ID, channels_it->ID);
			fprintf(fp, "connect_bd_intf_net [get_bd_intf_pins filter_%d_%d_%d/s_axis_dma_%d_in] [get_bd_intf_ports s_axis_dma_%d_in]\n",tmp_SST->num_ID, channels_it->ID, channels_it->attached_to_DMA->ID, channels_it->ID, channels_it->ID);
			fprintf(fp, "connect_bd_net [get_bd_pins /filter_%d_%d_%d/ap_clk] [get_bd_ports ap_clk]\n", tmp_SST->num_ID, channels_it->ID, channels_it->attached_to_DMA->ID);
			fprintf(fp, "connect_bd_net [get_bd_pins /filter_%d_%d_%d/ap_rst_n] [get_bd_ports ap_rst_n]\n", tmp_SST->num_ID, channels_it->ID, channels_it->attached_to_DMA->ID);
			channels_it = channels_it->next;
		}
		fprintf(fp, "endgroup\n");

		channels_it = tmp_SST->channels;
		while (channels_it != NULL) {
			channels_it-> filters;
			links_it = channels_it ->links;
			while (links_it != NULL) {
				if (links_it->weight == 1) {
					fprintf(fp, "create_bd_cell -type ip -vlnv xilinx.com:ip:axis_register_slice:1.1 axis_register_slice_%d\n", register_slice_ID);
					fprintf(fp, "set_property -dict [list CONFIG.TDATA_NUM_BYTES.VALUE_SRC USER CONFIG.HAS_TLAST.VALUE_SRC USER] [get_bd_cells axis_register_slice_%d]\n", register_slice_ID);
					fprintf(fp, "set_property -dict [list CONFIG.TDATA_NUM_BYTES {4} CONFIG.HAS_TLAST {1}] [get_bd_cells axis_register_slice_%d]\n", register_slice_ID);
					fprintf(fp, "connect_bd_net -net [get_bd_nets ap_clk_1] [get_bd_ports ap_clk] [get_bd_pins axis_register_slice_%d/aclk]\nconnect_bd_net -net [get_bd_nets ap_rst_n_1] [get_bd_ports ap_rst_n] [get_bd_pins axis_register_slice_%d/aresetn]\n", register_slice_ID, register_slice_ID);
					fprintf(fp, "connect_bd_intf_net [get_bd_intf_pins filter_%d_%d_%d/fifo_%d_%d] [get_bd_intf_pins axis_register_slice_%d/S_AXIS]\n", tmp_SST->num_ID,channels_it->ID, links_it->from->ID, channels_it->ID, links_it->from->ID, register_slice_ID);
					fprintf(fp, "connect_bd_intf_net [get_bd_intf_pins axis_register_slice_%d/M_AXIS] [get_bd_intf_pins filter_%d_%d_%d/fifo_%d_%d]\n", register_slice_ID, tmp_SST->num_ID, channels_it->ID, links_it->to->ID, channels_it->ID, links_it->from->ID);
					fprintf(fp, "connect_bd_net -net [get_bd_nets ap_clk_1] [get_bd_ports ap_clk] [get_bd_pins filter_%d_%d_%d/ap_clk]\nconnect_bd_net -net [get_bd_nets ap_rst_n_1] [get_bd_ports ap_rst_n] [get_bd_pins filter_%d_%d_%d/ap_rst_n]\n", tmp_SST->num_ID,channels_it->ID, links_it->to->ID, tmp_SST->num_ID, channels_it->ID, links_it->to->ID);
					register_slice_ID++;
				}else if(links_it->weight > 1){
					int fifo_size = get_closest_FIFO_size(links_it->weight);
					if (fifo_size == -1){
						set_error(tmp_context,TOO_LARGE_FIFO,THIS_MOD);
						return;
					}
					fprintf(fp, "create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:1.1 axis_data_fifo_%d\n", fifo_ID);
					fprintf(fp, "set_property -dict [list CONFIG.TDATA_NUM_BYTES.VALUE_SRC USER CONFIG.HAS_TLAST.VALUE_SRC USER] [get_bd_cells axis_data_fifo_%d]\n", fifo_ID);
					fprintf(fp, "set_property -dict [list CONFIG.TDATA_NUM_BYTES {4} CONFIG.FIFO_DEPTH {%d} CONFIG.HAS_TLAST {1}] [get_bd_cells axis_data_fifo_%d]\n",fifo_size, fifo_ID);
					fprintf(fp, "connect_bd_net -net [get_bd_nets ap_rst_n_1] [get_bd_ports ap_rst_n] [get_bd_pins axis_data_fifo_%d/s_axis_aresetn]\n", fifo_ID);
					fprintf(fp, "connect_bd_net -net [get_bd_nets ap_clk_1] [get_bd_ports ap_clk] [get_bd_pins axis_data_fifo_%d/s_axis_aclk]\n", fifo_ID);

					fprintf(fp, "connect_bd_intf_net [get_bd_intf_pins filter_%d_%d_%d/fifo_%d_%d] [get_bd_intf_pins axis_data_fifo_%d/S_AXIS]\n", tmp_SST->num_ID,channels_it->ID, links_it->from->ID, channels_it->ID, links_it->from->ID,fifo_ID);
				
					fprintf(fp, "connect_bd_intf_net [get_bd_intf_pins filter_%d_%d_%d/fifo_%d_%d] [get_bd_intf_pins axis_data_fifo_%d/S_AXIS]\n", tmp_SST->num_ID,channels_it->ID, links_it->from->ID, channels_it->ID, links_it->from->ID,fifo_ID);

					fprintf(fp, "connect_bd_intf_net [get_bd_intf_pins axis_data_fifo_%d/M_AXIS] [get_bd_intf_pins filter_%d_%d_%d/fifo_%d_%d]\n", fifo_ID, tmp_SST->num_ID,channels_it->ID, links_it->to->ID, channels_it->ID, links_it->from->ID);

				fprintf(fp, "connect_bd_net -net [get_bd_nets ap_clk_1] [get_bd_ports ap_clk] [get_bd_pins filter_%d_%d_%d/ap_clk]\nconnect_bd_net -net [get_bd_nets ap_rst_n_1] [get_bd_ports ap_rst_n] [get_bd_pins filter_%d_%d_%d/ap_rst_n]\n", tmp_SST->num_ID, channels_it->ID, links_it->to->ID, tmp_SST->num_ID,channels_it->ID, links_it->to->ID);

					fifo_ID++;

				}
				links_it = links_it -> next;
			}

			channels_it = channels_it->next;
		}

		links_channel_kernel = tmp_SST->links_channel_kernel;
		while (links_channel_kernel != NULL) {
			fprintf(fp, "create_bd_cell -type ip -vlnv xilinx.com:ip:axis_register_slice:1.1 axis_register_slice_%d\n", register_slice_ID);
			fprintf(fp, "set_property -dict [list CONFIG.TDATA_NUM_BYTES.VALUE_SRC USER CONFIG.HAS_TLAST.VALUE_SRC USER] [get_bd_cells axis_register_slice_%d]\n", register_slice_ID);
			fprintf(fp, "set_property -dict [list CONFIG.TDATA_NUM_BYTES {4} CONFIG.HAS_TLAST {1}] [get_bd_cells axis_register_slice_%d]\n", register_slice_ID);
			fprintf(fp, "connect_bd_net -net [get_bd_nets ap_clk_1] [get_bd_ports ap_clk] [get_bd_pins axis_register_slice_%d/aclk]\nconnect_bd_net -net [get_bd_nets ap_rst_n_1] [get_bd_ports ap_rst_n] [get_bd_pins axis_register_slice_%d/aresetn]\n", register_slice_ID, register_slice_ID);


			fprintf(fp, "connect_bd_intf_net [get_bd_intf_pins filter_%d_%d_%d/filter_%d] [get_bd_intf_pins axis_register_slice_%d/S_AXIS]\n", tmp_SST->num_ID,links_channel_kernel->from->channel->ID, links_channel_kernel->from->filter->ID, links_channel_kernel->from->filter->ID, register_slice_ID);
			fprintf(fp, "connect_bd_intf_net [get_bd_intf_pins axis_register_slice_%d/M_AXIS] [get_bd_intf_pins kernel_%d_%d/port_%d]\n", register_slice_ID, tmp_SST->num_ID,links_channel_kernel->to->kernel->ID, links_channel_kernel->to->ID);


			register_slice_ID++;
			links_channel_kernel = links_channel_kernel->next;
		}

		kernels_it = tmp_SST->kernels;
		while (kernels_it != NULL) {
			fprintf(fp, "connect_bd_net -net [get_bd_nets ap_clk_1] [get_bd_ports ap_clk] [get_bd_pins kernel_%d_%d/ap_clk]\nconnect_bd_net -net [get_bd_nets ap_rst_n_1] [get_bd_ports ap_rst_n] [get_bd_pins kernel_%d_%d/ap_rst_n]\n", tmp_SST->num_ID,kernels_it->ID, tmp_SST->num_ID, kernels_it->ID);
			kernels_it = kernels_it->next;
		}

		links_kernel_demux = tmp_SST->links_kernel_demux;
		while (links_kernel_demux != NULL) {
			fprintf(fp, "create_bd_cell -type ip -vlnv xilinx.com:ip:axis_register_slice:1.1 axis_register_slice_%d\n", register_slice_ID);
			fprintf(fp, "set_property -dict [list CONFIG.TDATA_NUM_BYTES.VALUE_SRC USER CONFIG.HAS_TLAST.VALUE_SRC USER] [get_bd_cells axis_register_slice_%d]\n", register_slice_ID);
			fprintf(fp, "set_property -dict [list CONFIG.TDATA_NUM_BYTES {4} CONFIG.HAS_TLAST {1}] [get_bd_cells axis_register_slice_%d]\n", register_slice_ID);
			fprintf(fp, "connect_bd_net -net [get_bd_nets ap_clk_1] [get_bd_ports ap_clk] [get_bd_pins axis_register_slice_%d/aclk]\nconnect_bd_net -net [get_bd_nets ap_rst_n_1] [get_bd_ports ap_rst_n] [get_bd_pins axis_register_slice_%d/aresetn]\n", register_slice_ID, register_slice_ID);

			fprintf(fp, "connect_bd_intf_net [get_bd_intf_pins kernel_%d_%d/demux] [get_bd_intf_pins axis_register_slice_%d/S_AXIS]\n", tmp_SST->num_ID,links_kernel_demux->from->kernel->ID, register_slice_ID);
			fprintf(fp, "connect_bd_intf_net [get_bd_intf_pins axis_register_slice_%d/M_AXIS] [get_bd_intf_pins demux_%d/port_%d]\n", register_slice_ID, tmp_SST->num_ID,links_kernel_demux->to->ID);

			register_slice_ID++;
			links_kernel_demux = links_kernel_demux->next;
		}

		struct soag_filter_demux_link *links_channel_demux = tmp_SST->links_channel_demux;
		fprintf(fp, "create_bd_cell -type ip -vlnv xilinx.com:ip:axis_register_slice:1.1 axis_register_slice_%d\n", register_slice_ID);
		fprintf(fp, "set_property -dict [list CONFIG.TDATA_NUM_BYTES.VALUE_SRC USER CONFIG.HAS_TLAST.VALUE_SRC USER] [get_bd_cells axis_register_slice_%d]\n", register_slice_ID);
		fprintf(fp, "set_property -dict [list CONFIG.TDATA_NUM_BYTES {4} CONFIG.HAS_TLAST {1}] [get_bd_cells axis_register_slice_%d]\n", register_slice_ID);
		fprintf(fp, "connect_bd_net -net [get_bd_nets ap_clk_1] [get_bd_ports ap_clk] [get_bd_pins axis_register_slice_%d/aclk]\nconnect_bd_net -net [get_bd_nets ap_rst_n_1] [get_bd_ports ap_rst_n] [get_bd_pins axis_register_slice_%d/aresetn]\n", register_slice_ID, register_slice_ID);

		fprintf(fp, "connect_bd_intf_net [get_bd_intf_pins filter_%d_%d_%d/border] [get_bd_intf_pins axis_register_slice_%d/S_AXIS]\n", tmp_SST->num_ID,links_channel_demux->from->channel->ID, links_channel_demux->from->filter->ID , register_slice_ID);
		fprintf(fp, "connect_bd_intf_net [get_bd_intf_pins axis_register_slice_%d/M_AXIS] [get_bd_intf_pins demux_%d/port_%d]\n", register_slice_ID, tmp_SST->num_ID, links_channel_demux->to->ID);
		fprintf(fp, "connect_bd_net -net [get_bd_nets ap_clk_1] [get_bd_ports ap_clk] [get_bd_pins demux_%d/ap_clk]\nconnect_bd_net -net [get_bd_nets ap_rst_n_1] [get_bd_ports ap_rst_n] [get_bd_pins demux_%d/ap_rst_n]\n",tmp_SST->num_ID,tmp_SST->num_ID);

		fprintf(fp, "startgroup\n");

		struct soag_demux_port *ports_it = tmp_SST->demux->ports;
		while (ports_it != NULL) {
			if (ports_it->port_type == DEMUX_OUT)
				break;
			ports_it = ports_it->next;
		}
		fprintf(fp, "create_bd_intf_port -mode Master -vlnv xilinx.com:interface:axis_rtl:1.0 m_axis_port_%d\n", ports_it->ID);
		fprintf(fp, "connect_bd_intf_net [get_bd_intf_pins demux_%d/m_axis_port_%d] [get_bd_intf_ports m_axis_port_%d]\n", tmp_SST->num_ID,ports_it->ID, ports_it->ID);

		fprintf(fp, "endgroup\n");

		fprintf(fp, "validate_bd_design\n");
		fprintf(fp, "make_wrapper -files [get_files SST_%d_IP/SST_%d_IP.srcs/sources_1/bd/SST_%d_design/SST_%d_design.bd] -top\n", tmp_context->id, tmp_context->id, tmp_context->id, tmp_context->id);

		fprintf(fp, "add_files -norecurse SST_%d_IP/SST_%d_IP.srcs/sources_1/bd/SST_%d_design/hdl/SST_%d_design_wrapper.v\n", tmp_context->id, tmp_context->id, tmp_context->id, tmp_context->id);

		fprintf(fp, "update_compile_order -fileset sources_1\n");
		fprintf(fp, "update_compile_order -fileset sources_1\n");
		fprintf(fp, "update_compile_order -fileset sim_1\n");

		fprintf(fp, "ipx::package_project -root_dir SST_%d_IP/SST_%d_IP.srcs/sources_1/bd/SST_%d_design\n", tmp_context->id, tmp_context->id, tmp_context->id);

		fprintf(fp, "set_property vendor Blablabla [ipx::current_core]\n");
		fprintf(fp, "set_property taxonomy /UserIP [ipx::current_core]\n");

		fprintf(fp, "ipx::add_bus_parameter FREQ_HZ [ipx::get_bus_interfaces m_axis_port_%d -of_objects [ipx::current_core]]\n", ports_it->ID);

		fprintf(fp, "set_property description {Clock frequency (Hertz)} [ipx::get_bus_parameters FREQ_HZ -of_objects [ipx::get_bus_interfaces m_axis_port_%d -of_objects [ipx::current_core]]]\n", ports_it->ID);

		fprintf(fp, "set_property value 100000000 [ipx::get_bus_parameters FREQ_HZ -of_objects [ipx::get_bus_interfaces m_axis_port_%d -of_objects [ipx::current_core]]]\n", ports_it->ID);


		channels_it = tmp_SST->channels;
		while (channels_it != NULL) {

			fprintf(fp, "ipx::add_bus_parameter FREQ_HZ [ipx::get_bus_interfaces s_axis_dma_%d_in -of_objects [ipx::current_core]]\n", channels_it->ID);

			fprintf(fp, "set_property description {Clock frequency (Hertz)} [ipx::get_bus_parameters FREQ_HZ -of_objects [ipx::get_bus_interfaces s_axis_dma_%d_in -of_objects [ipx::current_core]]]\n",channels_it->ID);

			fprintf(fp, "set_property value 100000000 [ipx::get_bus_parameters FREQ_HZ -of_objects [ipx::get_bus_interfaces s_axis_dma_%d_in -of_objects [ipx::current_core]]]\n",channels_it->ID);
			channels_it = channels_it->next;
		}

		fprintf(fp, "ipx::add_bus_parameter ASSOCIATED_BUSIF [ipx::get_bus_interfaces ap_signal_clock -of_objects [ipx::current_core]]\n");

		fprintf(fp, "set_property value m_axis_port_%d",ports_it->ID);

				channels_it = tmp_SST->channels;
		while (channels_it != NULL) {
			fprintf(fp, ":s_axis_dma_%d_in", channels_it->ID);
			channels_it = channels_it->next;
		}

		fprintf(fp, " [ipx::get_bus_parameters ASSOCIATED_BUSIF -of_objects [ipx::get_bus_interfaces ap_signal_clock -of_objects [ipx::current_core]]]\n");


		fprintf(fp, "open_bd_design {SST_%d_IP/SST_%d_IP.srcs/sources_1/bd/SST_%d_design/SST_%d_design.bd}\n", tmp_context->id, tmp_context->id, tmp_context->id, tmp_context->id);

		fprintf(fp, "set_property core_revision 2 [ipx::current_core]\n");
		fprintf(fp, "ipx::create_xgui_files [ipx::current_core]\n");
		fprintf(fp, "ipx::update_checksums [ipx::current_core]\n");
		fprintf(fp, "ipx::save_core [ipx::current_core]\n");
		fprintf(fp, "exit\n");

		fclose(fp);
		chdir("..");
		tmp_context = tmp_context->next;
		i++;
	}
}

void generate_IPs(options* opt, int BOARD_ID) {


	scop_context * tmp_context = NULL;

	tmp_context = opt->contexts;

	struct soag_SST* tmp_SST = NULL;

	int i = 0;
	while (tmp_context != NULL) {

		if (encountered_error(tmp_context)) {
			tmp_context = tmp_context->next;
			continue;
		}

		chdir(opt->root_directory);
		chdir(tmp_context->path);

		system(get_hls_command(BOARD_ID));
		generate_vivado_tcl(opt,BOARD_ID);

		chdir("..");
		tmp_context = tmp_context->next;
		i++;
	}

}

void package_SST_IP(options* opt){
	scop_context * tmp_context = NULL;

	tmp_context = opt->contexts;

	struct soag_SST* tmp_SST = NULL;

	int i = 0;
	while (tmp_context != NULL) {

		if (encountered_error(tmp_context)) {
			tmp_context = tmp_context->next;
			continue;
		}

		chdir(opt->root_directory);
		chdir(tmp_context->path);

	//	if(access("vivado_sst_ip.tcl",F_OK)!=-1)
	//	{
	//		set_error(tmp_context, FILE_NOT_FOUND_VIVADO_TCL, THIS_MOD);
	//		tmp_context = tmp_context->next;
	//		continue;
	//	}

		system("/opt/clantool/src/pack_SST.sh > pack_SST_out 2> pack_SST_err");

		chdir("..");
		tmp_context = tmp_context->next;
		i++;
	}
}