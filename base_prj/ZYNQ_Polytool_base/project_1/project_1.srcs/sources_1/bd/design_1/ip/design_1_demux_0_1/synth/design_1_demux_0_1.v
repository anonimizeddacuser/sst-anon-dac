// (c) Copyright 1995-2015 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.


// IP VLNV: xilinx.com:hls:demux:3.0
// IP Revision: 1507140852

(* X_CORE_INFO = "demux,Vivado 2014.4" *)
(* CHECK_LICENSE_TYPE = "design_1_demux_0_1,demux,{}" *)
(* CORE_GENERATION_INFO = "design_1_demux_0_1,demux,{x_ipProduct=Vivado 2014.4,x_ipVendor=xilinx.com,x_ipLibrary=hls,x_ipName=demux,x_ipVersion=3.0,x_ipCoreRevision=1507140852,x_ipLanguage=VERILOG,x_ipSimLanguage=MIXED}" *)
(* DowngradeIPIdentifiedWarnings = "yes" *)
module design_1_demux_0_1 (
  ap_clk,
  ap_rst_n,
  class_1_TVALID,
  class_1_TREADY,
  class_1_TDATA,
  class_1_TLAST,
  class_2_TVALID,
  class_2_TREADY,
  class_2_TDATA,
  class_2_TLAST,
  dma_out_TVALID,
  dma_out_TREADY,
  dma_out_TDATA,
  dma_out_TLAST
);

(* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 ap_clk CLK" *)
input wire ap_clk;
(* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 ap_rst_n RST" *)
input wire ap_rst_n;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 class_1 TVALID" *)
input wire class_1_TVALID;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 class_1 TREADY" *)
output wire class_1_TREADY;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 class_1 TDATA" *)
input wire [31 : 0] class_1_TDATA;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 class_1 TLAST" *)
input wire [0 : 0] class_1_TLAST;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 class_2 TVALID" *)
input wire class_2_TVALID;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 class_2 TREADY" *)
output wire class_2_TREADY;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 class_2 TDATA" *)
input wire [31 : 0] class_2_TDATA;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 class_2 TLAST" *)
input wire [0 : 0] class_2_TLAST;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 dma_out TVALID" *)
output wire dma_out_TVALID;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 dma_out TREADY" *)
input wire dma_out_TREADY;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 dma_out TDATA" *)
output wire [31 : 0] dma_out_TDATA;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 dma_out TLAST" *)
output wire [0 : 0] dma_out_TLAST;

  demux inst (
    .ap_clk(ap_clk),
    .ap_rst_n(ap_rst_n),
    .class_1_TVALID(class_1_TVALID),
    .class_1_TREADY(class_1_TREADY),
    .class_1_TDATA(class_1_TDATA),
    .class_1_TLAST(class_1_TLAST),
    .class_2_TVALID(class_2_TVALID),
    .class_2_TREADY(class_2_TREADY),
    .class_2_TDATA(class_2_TDATA),
    .class_2_TLAST(class_2_TLAST),
    .dma_out_TVALID(dma_out_TVALID),
    .dma_out_TREADY(dma_out_TREADY),
    .dma_out_TDATA(dma_out_TDATA),
    .dma_out_TLAST(dma_out_TLAST)
  );
endmodule
